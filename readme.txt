cad4tcl
=======================================

cad4tcl is an extract of the rattleCAD project and deals with tk::canvas also
as with tkpath while tkpath support antialiased rendering. Both canvases are 
controlled via the same interface.


cad4tcl provides:
    zoom
    pan
    drag
    read and write svg-files 
    different DIN-Formats (A0, A1, ..)
    events
    ...

    
... please see the scripts in the test directory


cad4tcl requires the following packages:
    Tk                      ... part of tcl 8.6
    TclOO                   ... part of tcl 8.6
    math            1.2.5   ... part of tcllib
    math::geometry          ... part of tcllib
    tdom                    ... part of most tcl-distributions
    svgDOM                  ... svg-parcer based on tdom and TclOO
    
    
if some packages are not available in your environment, you can download them
from here:
    https://sourceforge.net/projects/tcllib/
    https://sourceforge.net/projects/svgdom/

    
cad4tcl is used in 
    rattleCAD 
        ... https://rattlecad.com
    
