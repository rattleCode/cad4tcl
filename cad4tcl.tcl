 ########################################################################
 #
 #  This software is copyrighted by Manfred Rosenberger and other parties.  
 #  The following terms apply to all files associated with the software unless
 #  explicitly disclaimed in individual files.
 #  
 #  The authors hereby grant permission to use, copy, modify, distribute,
 #  and license this software and its documentation for any purpose, provided
 #  that existing copyright notices are retained in all copies and that this
 #  notice is included verbatim in any distributions. No written agreement,
 #  license, or royalty fee is required for any of the authorized uses.
 #  Modifications to this software may be copyrighted by their authors
 #  and need not follow the licensing terms described here, provided that
 #  the new terms are clearly indicated on the first page of each file where
 #  they apply.
 #  
 #  IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 #  FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 #  ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
 #  DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
 #  POSSIBILITY OF SUCH DAMAGE.
 #  
 #  THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
 #  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 #  FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
 #  IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
 #  NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 #  MODIFICATIONS.
 #  
 #  GOVERNMENT USE: If you are acquiring this software on behalf of the
 #  U.S. government, the Government shall have only "Restricted Rights"
 #  in the software and related documentation as defined in the Federal 
 #  Acquisition Regulations (FARs) in Clause 52.227.19 (c) (2).  If you
 #  are acquiring the software on behalf of the Department of Defense, the
 #  software shall be classified as "Commercial Computer Software" and the
 #  Government shall have only "Restricted Rights" as defined in Clause
 #  252.227-7013 (c) (1) of DFARs.  Notwithstanding the foregoing, the
 #  authors grant the U.S. Government and others acting in its behalf
 #  permission to use and distribute the software in accordance with the
 #  terms specified in this license.
 # 
 # ----------------------------------------------------------------------
 #
 #  Copyright (c) Manfred ROSENBERGER, 2017
 #
 # ----------------------------------------------------------------------
 #  http://www.magicsplat.com/articles/oo.html
 # ----------------------------------------------------------------------
 #
 #  rattleCAD 3.4.05:
 #          http://rattlecad.sourceforge.net/
 #          https://sourceforge.net/projects/rattlecad/
 #  
 #  vectorfont:
 #          Copyright (c) Gerhard Reithofer, 2006/12/27
 #  
 #  simplifySVG:
 #          is a library developed in the rattleCAD - project 
 #  
 #  other sources:
 #  
 #      rotate_item:
 #          kvetter@DELETETHIS.alltel.net
 #          http://wiki.tcl.tk/8595
 #      zoom:
 #          masse-navette.glfs@wanadoo.fr
 #          http://wiki.tcl.tk/4844
 #      vector algorythms:
 #          kvetter@DELETETHIS.alltel.net
 #          http://wiki.tcl.tk/8447
 #  
 #       http://www.magicsplat.com/articles/oo.html
 #
 # ----------------------------------------------------------------------
 # 
 #  2017/11/26
 #      ... extracted from the rattleCAD-project (Version 3.4.05) by
 #          the author of rattleCAD
 #
 # ----------------------------------------------------------------------
 #  namespace:  cad4tcl
 # ----------------------------------------------------------------------
 #
 #  0.11 - 2018/12/09
 #      ... replace vectorfont by xmlfont, cleanup in next version
 #  0.12 - 2018/12/11
 #      ... cleanup variable definitions
 #  0.13 - 2018/12/11
 #      ... remove vectorfont
 #  0.14 - 2018/12/12
 #      ... cleanup
 #  0.15 - 2018/12/14
 #      ... reorganize lib structure
 #  0.16 - 2018/12/20
 #      ... 
 #  0.18 - 2019/04/14
 #      ... check for 
 #          ... CheckClickObject
 #          ... CheckDragObject
 #  0.19 - 2019/08/11
 #      ... update Tk before creating the object    
 #  0.20 - 2019/10/21
 #      ... update cad4tcl::ItemInterface_PathCanvas
 #          ... path -> Arc
 #       - 2019/11/17
 #      ... method move -> Move -> movebare
 #  0.22.2 - 2020/04/25
 #      ... reference to unicode xmlfont
 #  0.22.3 - 2020/11/07
 #      ... rename parameter Defaultprecision -> DefaultPrecision
 #  0.22.4 - 2020/11/20
 #      ... mouse binding: move, zoom
 #  0.22.5 - 2021/01/11
 #      ... cad4tcl::ItemInterface... -> createSVG
 #  0.22.6 - 2021/03/20
 #      ... ::tkpath::antialias -> ::tkp::antialias
 #
 #  0.22.9 - 2021/07/24
 #      ... add -highlightthickness 0 at create canvas
 #  0.22.12 - 2021/09/07
 #      ... cad4tcl::ItemInterface_PathCanvas -> createCircle ... use -activefill
 #  0.22.14 - 2023/03/16
 #      ... add hiddenLine
 #  0.22.15 - 2023/03/16
 #  0.22.16 - 2023/03/20
 #      ... fix strokedasharray for PathCanvas
 #  0.22.17 - 2023/07/24
 #      ... _getBBoxInfo: handle infinity values
 #      ... centerContent: handle empty tags
 #      ... scale: scale factor must not be null
 #  0.22.17 - 2023/07/24
 #      ... _getBBoxInfo: handle infinity values
 #  0.22.18 - 2023/07/31
 #      ... debug: raise
 #  0.22.19 - 2023/08/21
 #      ... debug: add Temporary_CanvasScale to prevent canvas to change line widths
 #  0.22.20 - 2023/11/15
 #      ... debug: remove logging in -> setTemporaryScale
 #  0.22.21 - 2024/01/23
 #      ... feature: enable path behaviour in app/simplifySVG
 #  0.22.22 - 2024/02/02
 #      ... debug: app/simplifySVG -> enable to be source of center_00
 #  0.22.23 - 2024/02/15
 #      ... debug: app/simplifySVG -> ensure unique ids
 #  0.22.24 - 2024/02/16
 #      ... debug: bbox2 -> handle path definitiona (M, L, Z)
 #
 
 # ... if needed simplifySVG
 
    #
package provide cad4tcl 0.22.24
    #
package require Tk
package require TclOO
package require tdom
package require math 1.2.5
package require math::geometry
    #
catch {package require tkpath}
    #

    # -----------------------------------------------------------------------------------
    #
    #: Functions : namespace      c a d 4 t c l
    #
namespace eval cad4tcl {
        #
    variable packageHomeDir [file dirname [file normalize [info script]]]
    lappend auto_path [file join $packageHomeDir lib]
        #
}
    #
