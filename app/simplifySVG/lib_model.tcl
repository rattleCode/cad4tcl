 ########################################################################
 #
 # simplifySVG: lib_model.tcl
 #
 # Copyright (c) Manfred ROSENBERGER, 2017/11/26
 #
 # The author  hereby grant permission to use,  copy, modify, distribute,
 # and  license this  software  and its  documentation  for any  purpose,
 # provided that  existing copyright notices  are retained in  all copies
 # and that  this notice  is included verbatim  in any  distributions. No
 # written agreement, license, or royalty  fee is required for any of the
 # authorized uses.  Modifications to this software may be copyrighted by
 # their authors and need not  follow the licensing terms described here,
 # provided that the new terms are clearly indicated on the first page of
 # each file where they apply.
 #
 # IN NO  EVENT SHALL THE AUTHOR  OR DISTRIBUTORS BE LIABLE  TO ANY PARTY
 # FOR  DIRECT, INDIRECT, SPECIAL,  INCIDENTAL, OR  CONSEQUENTIAL DAMAGES
 # ARISING OUT  OF THE  USE OF THIS  SOFTWARE, ITS DOCUMENTATION,  OR ANY
 # DERIVATIVES  THEREOF, EVEN  IF THE  AUTHOR  HAVE BEEN  ADVISED OF  THE
 # POSSIBILITY OF SUCH DAMAGE.
 #
 # THE  AUTHOR  AND DISTRIBUTORS  SPECIFICALLY  DISCLAIM ANY  WARRANTIES,
 # INCLUDING,   BUT   NOT  LIMITED   TO,   THE   IMPLIED  WARRANTIES   OF
 # MERCHANTABILITY,    FITNESS   FOR    A    PARTICULAR   PURPOSE,    AND
 # NON-INFRINGEMENT.  THIS  SOFTWARE IS PROVIDED  ON AN "AS  IS" BASIS,
 # AND  THE  AUTHOR  AND  DISTRIBUTORS  HAVE  NO  OBLIGATION  TO  PROVIDE
 # MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 #
  
namespace eval cad4tcl::app::simplifySVG::model {
      
        # --------------------------------------------
        # Export as global command
    variable packageHomeDir [file normalize [file join [pwd] [file dirname [info script]]] ]
    
        # --------------------------------------------
        #  create base config 
        #       -> registryDOM

        #
    variable CONST_PI           [expr 4*atan(1)]        
        #
    variable pathType
        #
    variable svgTextDeep
    variable svgDoc
    variable svgRoot

    variable exportFileName    
        #
}

proc cad4tcl::app::simplifySVG::model::openSVGFile {_fileName} {
        # variable cvObject
    variable fileName
        #
    variable svgTextDeep
    variable svgDoc
    variable svgRoot
        #
        #
        # variable sourceTree
        # variable sourceText
        # variable workTree
        # variable targetText 
        # variable svgRoot
        # variable svgTextDeep
        # variable svgRootTarget 
        # variable currentVersion
        #
    variable inputDir
        #
    if {$_fileName != {}} {
        set fileName $_fileName
    } else {
        return
    }
        #
        # variable exportFileName $_exportFileName
        #
    puts "\n"
    puts "  =============================================="
    puts "   -- openSVGFile:   $fileName"
    puts "  =============================================="
    puts "\n"
        #
        # --- open File ------------
    set inputDir [file dirname $fileName]
        #
    set fp [open $fileName]
        # 
    set svgXML [read $fp]
    close   $fp
        #
        # --- compute results ------            
    set svgDoc [dom parse  $svgXML]
        # $doc documentElement root
        #
    unifyIDs
        #
        # puts "[$svgDoc asXML]"
        #
    set svgRoot [$svgDoc documentElement]
        #
    set svgTextDeep [$svgRoot asXML]
        #
    return
        #
}

proc cad4tcl::app::simplifySVG::model::unifyIDs {} {
        #
    variable svgDoc
        #
    set myRoot      [$svgDoc documentElement]
    set nodeList    [$svgDoc getElementsByTagName *]
        # puts "\n\n           size: [llength $nodeList]\n\n"
        #
    set index       0
    set listUsed    {}
    foreach node $nodeList {
        if {[$node nodeType] eq {ELEMENT_NODE}} {
            set nodeName [$node nodeName]
                # puts "         -> \$nodeName $nodeName"
            if {$nodeName in {circle ellipse g image line path polygon polyline rect text}} {
                if {[catch {set nodeID [$node getAttribute id]} eID]} {
                    set nodeID {}
                } else {
                    if {$nodeID in $listUsed} {
                        set nodeID  {}
                    } else {
                        lappend listUsed $nodeID
                    }
                }
                if {$nodeID eq {}} {
                    incr index 
                    set nodeID  [format {_my_%s} $index]
                    lappend listUsed    $nodeID
                    $node setAttribute id $nodeID
                }
            }
        }
    }
        #
}

proc cad4tcl::app::simplifySVG::model::getSVGDoc {} {
    variable svgDoc
    return  $svgDoc
}

proc cad4tcl::app::simplifySVG::model::getSVGText {} {
    variable svgDoc
    return  [$svgDoc asXML]
}

proc cad4tcl::app::simplifySVG::model::getSVGRoot {} {
    variable svgDoc
    return  [$svgDoc documentElement]
}

proc cad4tcl::app::simplifySVG::model::writeFile {fileName fileContent} {
    set fileId [open $fileName "w"]
    puts [encoding names]
    puts -nonewline $fileId $fileContent
    close $fileId
    return 1
}

