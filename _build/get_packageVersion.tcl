#!/bin/sh
# the next line restarts using wish \
exec wish "$0" "$@"

#glob ../trunk *
#puts "[file normalize .]"
#puts "[file dirname [file normalize .]]"



puts "\n"
puts "  $argv0"
puts " ----------------------------------------"
puts "     ... $argv"
puts "\n\n"


set packageName [lindex $argv 0]
set packageRoot [file normalize [lindex $argv 1]]
set versionFile [file normalize [lindex $argv 2]]

set inputFile   [file join "$packageRoot" pkgIndex.tcl]

# ----------------------------------
    #    
puts "\n ========================\n"
puts "         get Version Info\n"
puts "                       $argv"    
puts "             ... ${packageRoot}\n"    
puts "             ... ${packageName}\n"    
puts "             ... ${inputFile}\n"    
puts "             ... ${versionFile}\n"    
    #
    #
if {[file exists ${inputFile}]} {
        #
    set fp [open ${inputFile}]
    set searchList  [list package/ifneeded/$packageName]
    while {[gets $fp line] >= 0} {
        if [regexp -- {package} $line] {
            # puts $line
            set line [string map {\" {}} $line]
            puts [join [lrange $line 0 2] /]
            if {[join [lrange $line 0 2] /] eq $searchList} {
                set appVersion [lindex $line 3]
            }
        }
    }    
    close $fp
        #
} else {
        #
    set appVersion {_unknown_}
        #
}

puts "\n"
puts "    -> \$appVersion  .....  $appVersion"
puts "\n"
    #
  
    #
set fp [open ${versionFile} w]
puts $fp $appVersion
close $fp
    #

