 ########################################################################
 #
 #  Copyright (c) Manfred ROSENBERGER, 2017
 #
 #      package: cadCanvas 	->	classExtension_SVG__Super.tcl
 #
 # ----------------------------------------------------------------------
 #  namespace:  cadCanvas
 # ----------------------------------------------------------------------
 #
 #  2017/11/26
 #      extracted from the rattleCAD-project (Version 3.4.05)
 #          http://rattlecad.sourceforge.net/
 #          https://sourceforge.net/projects/rattlecad/
 #
 #
 
oo::define cad4tcl::Extension_SVG__Super {
        #
        #
    variable packageHomeDir
    variable cvObject
    variable ItemInterface
        #
    variable Style
        #
        #
    constructor {cvObj itemIF args} { 
            #
        puts "              -> class Extension_SVG__Super"
            #
            #
        set cvObject       $cvObj    
            #
        set ItemInterface  $itemIF    
            #
        set packageHomeDir  $::cad4tcl::packageHomeDir
            #
            #
        array set Style {    
            Linewidth           "0.35" 
            Linecolour          "black" 
            Fontstyle           "vector" 
            Fontsize            "3.5" 
            Fontdist            "1" 
            Font                "Arial" 
            Fontcolour          "black" 
            Precision           "2" 
            DefaultPrecision    "2"
        }
            #
    }
        #
    destructor { 
            #
        puts "            -> [self] ... destroy Extension_SVG__Super"
            #
    }
        #
    method unknown {target_method args} {
                     puts "<E> ... cad4tcl::Extension_SVG__Super $target_method $args  ... unknown"
    }
        #
        #
    method readNode {svgRoot canvasPosition angle customTag {style {}}} {}
        #
    method createElement {svgNode canvasPosition svgCenter angle svgTag {style {}}} {}
        #
    method exportFile {{svgFile {}}} {
        # http://tclbitprint.sourceforge.net/tkpath/README.txt		
        #     circle
		#     ellipse
		#     group
		#     path
		#     pimage
		#     pline
		#     polyline
		#     ppolygon
		#     prect
		#     ptext
        #    
    } 
        #
    method transformObject {valueList matrix} {
            #
        set valueList_Return {}
            # puts "    transformObject: $matrix"
        lassign $matrix  a b c d tx ty 
            # foreach {a b c d tx ty} $matrix break
            # puts "          $a $b $tx  /  $c $d $ty " 
        foreach {x y} $valueList {
                # puts "       -> $x $y"
            set xt [ expr {$a*$x - $b*$y + $tx}]
            set yt [ expr {$c*$x - $d*$y - $ty}]
            set valueList_Return [lappend valueList_Return $xt [expr {-1*$yt}] ]
                # puts "             function   x:  $a*$x - $b*$y + $tx    $xt"
                # puts "             function   y:  $c*$x - $d*$y - $ty    $yt"
        }
            #
        return $valueList_Return
            #
    }
        #
    method formatXColor {rgb} {
        if {$rgb == ""} {return none}
        lassign [winfo rgb . $rgb]  r g b
            # foreach {r g b} [winfo rgb . $rgb] break
        return [format "#%02x%02x%02x" [expr {$r/256}] [expr {$g/256}] [expr {$b/256}] ]
    }
        #
    method FormatSVGAttribute {cvItem attrDictNew attrDictMap} {
            #
        set cv           [$cvObject getCanvas]
        set cvType       [$cvObject configure   Canvas  Type]      
            #
        set attrString {}
            #
        dict for {svgKey value} $attrDictNew {
            append attrString "$svgKey=\"$value\" "
        }
            #
            #appUtil::pdict $attrDictMap
            #
        dict for {svgKey value} $attrDictMap {
                #
            lassign $value  cvKey itemDefault
                # foreach {cvKey itemDefault} $value break
                #
            set itemValue [$cv itemcget $cvItem $cvKey]
                #
                #puts "      1 -> $cvItem -> $svgKey  -> $cvKey => $itemDefault <- $itemValue"
                #
            switch -exact -- $svgKey {
                fill -
                stroke {
                    if {$itemValue eq {}} {
                        set itemValue {none}
                    } else {
                        set itemValue [my formatXColor $itemValue]
                    }
                }
                default {}
            }
                #
            if {$itemValue eq {}} {
                continue
            }
                #
                #puts "      3 -> $cvItem -> $svgKey  -> $cvKey => $itemDefault <- $itemValue"
                #
            append attrString "$svgKey=\"$itemValue\" "
                #
        }
            #
        return $attrString
            #
    }
        #
    method ___formatItemAttribute {name value {default {}}} {
            #
        set cv           [$cvObject getCanvas]
        set cvType       [$cvObject configure   Canvas  Type]      
            #
            # puts "     -> $cvType"
            #
        switch -exact $cvType {
            PathCanvas {
                puts "     -> PathCanvas"
            }
            default {
                if {$value != $default} {
                    return " $name=\"$value\""
                }
                return " $name=\"$default\""
            }
        }
    }
        #
}

