 ########################################################################
 #
 #  Copyright (c) Manfred ROSENBERGER, 2017
 #
 #      package: cad4tcl 	->	classExtension_SVG_PathCanvas.tcl
 #
 # ----------------------------------------------------------------------
 #  namespace:  cad4tcl
 # ----------------------------------------------------------------------
 #
 #  2017/11/26
 #      extracted from the rattleCAD-project (Version 3.4.05)
 #          http://rattlecad.sourceforge.net/
 #          https://sourceforge.net/projects/rattlecad/
 #
 #
 
oo::define cad4tcl::Extension_SVG_PathCanvas {
        #
    superclass cad4tcl::Extension_SVG__Super    
        #
        #
    variable cvObject
    variable ItemInterface        
        #
        #
    constructor {cvObj itemIF args} { 
            #
        puts "              -> class Extension_SVG_PathCanvas"
            #
        next $cvObj $itemIF $args
            #
    }
        #
    destructor { 
            #
        puts "            -> [self] ... destroy Extension_SVG_PathCanvas"
            #
    }
        #
    method unknown {target_method args} {
                     puts "<E> ... cad4tcl::Extension_SVG_PathCanvas $target_method $args  ... unknown"
    }
        #
        #    
    method readNode {svgRoot canvasPosition angle customTag {style {}}} {
            #
            # puts "\n"
            # puts "    [self] readNode"
            # puts "    -> \$cvObject   $cvObject"
            #
        set root $svgRoot
            #
        set center_Node [$svgRoot find id center_00]
        if { $center_Node != {} } {
            set svgPosition(x)  [$center_Node getAttribute cx]
            set svgPosition(y)  [$center_Node getAttribute cy]
        } else {
            # puts "     ... no id=\"center_00\""
            set svgPosition(x)  0
            set svgPosition(y)  0
        }
        set svgCenter [list $svgPosition(x) $svgPosition(y)]
            #
                
            #
            # -- define a unique id for svgContent
            #
        set w [$cvObject getCanvas]
            #        
        if {$customTag eq {}} { 
            set svgTag      [format "group_svg_%s" [llength [$w find withtag all]] ]
            set $svgTag     {}
        } else {
            set svgTag     $customTag
        }
            #
            # -- get graphic content nodes
            #
        set svgNode [$svgRoot firstChild]
        while {$svgNode ne {}} {
            # puts "  -> [$svgNode asXML]"
            set newNode [my createElement $svgNode $canvasPosition $svgCenter $angle $svgTag]
            $w addtag $svgTag withtag $newNode
            set svgNode [$svgNode nextSibling]
        }
            #
        return $svgTag
            #
    }
        #
    method createElement {svgNode canvasPosition svgCenter angle svgTag {style {}}} {
            #
            # -- canvasPosition
            #
        lassign $canvasPosition  pos_x pos_y
            # foreach {pos_x pos_y} $canvasPosition break
            #
            # -- get center SVG
            #    
        set svgPosition(x)  [lindex $svgCenter 0]
        set svgPosition(y)  [lindex $svgCenter 1]
        set svgPosition(xy) [list $svgPosition(x) $svgPosition(y)]
            
            #
            # -- define item container
            #
        set w       [$cvObject getCanvas]        
            #
            # -- handle exceptions
        if {[$svgNode nodeType] != {ELEMENT_NODE}} {
            return
        }
        if {[$svgNode hasAttribute id]} {
            switch -exact [$svgNode getAttribute id] {
                center_00 -
                orient_00 -
                orient_01 {
                    return
                }
                default {}
            }
        }
            #
            # -- get svg Object attributs
            #
            # puts "   write_svgNode -> $svgNode [$svgNode nodeName]"
            #
            # -- set defaults
            #
        set objectPoints    {}
            #
        # set attrDict        [my GetNodeRendering $svgNode]
            #
            # -- get transform attribute
        set transform {_no_transformation_}
        catch {set transform    [$svgNode getAttribute transform]}
            #
        set cvItem  {}
            #
        switch -exact [$svgNode nodeName] {
            g {      
                set childNode [$svgNode firstChild]
                while {$childNode ne {}} {
                    # puts "    -> $childNode  [$childNode nodeName]"
                    my createElement $childNode $canvasPosition $svgCenter $angle $svgTag
                    set childNode [$childNode nextSibling]
                }
            }
            rect {
                    #
                set x  [expr { [$svgNode getAttribute x] - $svgPosition(x)}]
                set y  [expr {-[$svgNode getAttribute y] + $svgPosition(y)}]
                set width   [$svgNode getAttribute  width ]
                set height  [$svgNode getAttribute  height]
                set x2 [expr {$x + $width }]
                set y2 [expr {$y - $height}]
                    #
                set objectPoints [list $x $y $x $y2 $x2 $y2 $x2 $y]
                    #
                if {$angle != 0} { 
                    set objectPoints    [cad4tcl::math::rotateCoordList {0 0} $objectPoints $angle] 
                }
                    #
                set objectPoints        [cad4tcl::math::addVectorCoordList [list $pos_x $pos_y]    $objectPoints]
                    #
                set attrDict            [my GetNodeRendering $svgNode   [list  -fill white  -stroke black]]
                    #
                set cvItem              [$ItemInterface create  polygon \
                                            $objectPoints \
                                            $attrDict]
                    #
            }
            polygon {
                    #
                set valueList [ $svgNode getAttribute points ]
                set objectPoints [string map {, { }} $valueList]
                if {$transform != {_no_transformation_}} {
                        set matrix          [split [string map {matrix( {} ) {}} $transform] ,]
                        set objectPoints    [my transformObject $objectPoints $matrix ]
                            # puts "      polygon  -> SVGObject $objectPoints " 
                            # puts "      polygon  -> matrix    $matrix" 
                }
                    #
                if {[llength $objectPoints] < 4} { return }
                    #
                set tmpList {}
                foreach {x y} $objectPoints {
                    set tmpList [lappend tmpList [expr {$x - $svgPosition(x)}] [expr {$svgPosition(y) - $y}]]  
                }
                set objectPoints $tmpList
                if {$angle != 0} { 
                    set objectPoints    [cad4tcl::math::rotateCoordList {0 0} $objectPoints $angle] 
                }
                    #
                set objectPoints        [cad4tcl::math::addVectorCoordList [list $pos_x $pos_y]    $objectPoints]
                    #
                set attrDict            [my GetNodeRendering $svgNode   [list  -fill white  -stroke black]]
                    #
                set cvItem              [$ItemInterface create  polygon \
                                            $objectPoints \
                                            $attrDict]
                    #
            }
            polyline { # polyline class="fil0 str0" points="44.9197,137.492 47.3404,135.703 48.7804,133.101 ...
                set valueList [ $svgNode getAttribute points ]
                set objectPoints [string map {, { }} $valueList]
                if {$transform != {_no_transformation_}} {
                        set matrix [split [string map {matrix( {} ) {}} $transform] ,]
                        set objectPoints    [my transformObject $objectPoints $matrix ]
                            # puts "      polygon  -> SVGObject $objectPoints " 
                            # puts "      polygon  -> matrix    $matrix" 
                }
                    #
                if {[llength $objectPoints] < 4} { return }
                    #
                set tmpList {}
                foreach {x y} $objectPoints {
                    set tmpList [lappend tmpList [expr  {$x - $svgPosition(x)}] [expr {$svgPosition(y) - $y}]]  
                }
                set objectPoints $tmpList
                    #
                if {$angle != 0} { 
                    set objectPoints    [cad4tcl::math::rotateCoordList {0 0} $objectPoints $angle] 
                }
                    #
                set objectPoints        [cad4tcl::math::addVectorCoordList [list $pos_x $pos_y]    $objectPoints]
                    #
                set attrDict            [my GetNodeRendering $svgNode   [list  -fill none  -stroke black]]
                     #
                set cvItem              [$ItemInterface create  line \
                                            $objectPoints \
                                            $attrDict]
                    #
            }
            line { # line class="fil0 str0" x1="89.7519" y1="133.41" x2="86.9997" y2= "119.789"
                set objectPoints [list  [expr {[$svgNode getAttribute x1] - $svgPosition(x)}] [expr {-([$svgNode getAttribute y1] - $svgPosition(y))}] \
                                        [expr {[$svgNode getAttribute x2] - $svgPosition(x)}] [expr {-([$svgNode getAttribute y2] - $svgPosition(y))}] ]
                if {$angle != 0} { 
                    set objectPoints    [cad4tcl::math::rotateCoordList {0 0} $objectPoints $angle] 
                }
                if {[llength $objectPoints] < 4} { return }
                    #
                set objectPoints        [cad4tcl::math::addVectorCoordList [list $pos_x $pos_y]    $objectPoints]
                    #
                set attrDict            [my GetNodeRendering $svgNode   [list  -fill none -stroke black]]
                    #
                set cvItem              [$ItemInterface create  line \
                                            $objectPoints \
                                            $attrDict]
                    #
            }
            circle { # circle class="fil0 str2" cx="58.4116" cy="120.791" r="5.04665"
                    # --- dont display the center_object with id="center_00"
                if {![$svgNode hasAttribute cx]} return
                set cx [expr {  [$svgNode getAttribute cx] - $svgPosition(x) }]
                set cy [expr {-([$svgNode getAttribute cy] - $svgPosition(y))}]
                if {$angle != 0} { 
                    set c_xy [cad4tcl::math::rotateCoordList {0 0} [list $cx $cy] $angle] 
                    lassign $c_xy   cx cy
                        # foreach {cx cy} $c_xy break
                }
                set objectRadius [$svgNode getAttribute  r]
                set objectPoints [list  $cx $cy]
                    # set x1 [expr $cx - $r]
                    # set y1 [expr $cy - $r]
                    # set x2 [expr $cx + $r]
                    # set y2 [expr $cy + $r]
                    # set objectPoints [list $x1 $y1 $x2 $y2]
                    #
                set pos_objectPoints    [cad4tcl::math::addVectorCoordList [list $pos_x $pos_y]    $objectPoints]
                    #
                set attrDict            [my GetNodeRendering $svgNode   [list  -fill white  -stroke black]]
                    #
                dict set attrDict       -r $objectRadius
                    #
                set cvItem              [$ItemInterface create  circle \
                                            $pos_objectPoints \
                                            $attrDict]
                    #
            }
            path { # the complex all inclusive object in svg
                    #
                set pathDef     [$svgNode getAttribute d]
                    #
                    # puts "   -> $pathDef"
                    #
                set pathDef    [regsub -all {[a-zA-Z]|.^$\\]} $pathDef { & }]
                set pathDef    [string map {, { }}  $pathDef]
                set pathDef    [string map {- { -}} $pathDef]
                    #
                    # pathName itempdf tagOrId ?extgsProc objProc gradProc?                
                    # puts "---> itempdf"
                    # puts "[$w itempdf $cvItem]"
                    #    
                if 1 {
                    set cvItem          [$w create  path $pathDef]
                        # 
                    set objectPoints    [$w itempdf $cvItem]
                        #
                    $w delete $cvItem
                        #
                        # - reposition
                    set e_0 {}
                    set e_1 {}
                    set e_2 {}
                    set pathPoints {}
                    foreach element $objectPoints {
                            # puts "    -> $element"
                        set e_0 $e_1
                        set e_1 $e_2
                        set e_2 $element
                        if {$e_2 eq {l} || $e_2 eq {m}} {
                            lappend pathPoints [string toupper $element]
                            set x [expr {$e_0 - $svgPosition(x)}]
                            set y [expr {$e_1 - $svgPosition(y)}]
                            if {$angle != 0} { 
                                set c_xy [cad4tcl::math::rotateCoordList {0 0} [list $x $y] [expr {-1 * $angle}]] 
                                lassign $c_xy   x y
                            }
                            set x [expr {$x + $pos_x}]
                            set y [expr {$y - $pos_y}]
                            lappend pathPoints $x $y
                        }
                    }
                        # puts "    -> \$pathPoints: $pathPoints"
                        #
                    if {[regexp -nocase -start 0 -inline {z} $pathDef] != {}} {
                        lappend pathPoints Z
                    }
                        #
                    set pathDef             $pathPoints
                        #
                }
                set attrDict            [my GetNodeRendering $svgNode   [list  -fill white  -stroke black]]
                    #
                set cvItem              [$ItemInterface create  path \
                                            $pathDef \
                                            $attrDict]
                    #
            }              
            default {}
        }
        
            #
        if {$cvItem ne {}} {
            $w addtag $svgTag withtag   $cvItem
        }
            #

            #
            # -- add each to unique $svgTag
            #    
        return $svgTag
            #
    }
        #
    method GetNodeRendering {svgNode {attrDict {}}} {
            #
        set attr_Fill           {}
        set attr_Stroke         {}
        set attr_StrokeWidth    {}
            #
        if 0 {
            if [$svgNode hasAttribute style] {
                set nodeStyle   [$svgNode getAttribute style]
                puts "   -> $nodeStyle"
                # exit
            }
        }
            #
        foreach attrName [$svgNode attributes] {
            switch -exact $attrName {
                fill {
                    set attr_Fill           [$svgNode getAttribute fill]
                }
                stroke {
                    set attr_Stroke         [$svgNode getAttribute stroke]
                }
                default {}
            }
        }
            #
        dict for {key value} $attrDict {
            switch -exact $key {
                -fill {
                    if {$attr_Fill != {none}} {
                        set attr_Fill       [dict get $attrDict -fill]
                    }
                }
                -stroke {
                    set attr_Stroke         [dict get $attrDict -stroke]
                }
                default {}
            }
        }
            #
        set attrList    {}
            #
        if {$attr_Fill eq {}} {
            dict set attrList -fill {}
        } else {
            dict set attrList -fill $attr_Fill   ; # white
        }
        if {$attr_Stroke eq {}} {   
            dict set attrList -stroke {}
        } else {
            dict set attrList -stroke black
        }
            #
        return $attrList
            #
            #
        if 0 {    
            if {$attr_StrokeWidth eq {}} {
                ; #lappend attrList -stroke-width 1   
            }  
             stroke-width {
                set attr_StrokeWidth    [$svgNode getAttribute stroke-width]
            }
            -stroke-width {
                set attr_StrokeWidth    [dict get $attrDict -stroke-width]
            }
        }
            #
    }    
        
        #
        #
    method exportFile {{svgFile {}}} {
            #
            # based on http://wiki.tcl.tk/4534
            #
        set cv           [$cvObject getCanvas]
        set wType        [$cvObject configure   Canvas  Type]      
        set wScale       [$cvObject configure   Canvas  Scale]      
        set stageScale   [$cvObject configure   Stage   Scale]      
        set stageUnit    [$cvObject configure   Stage   Unit]      
        set font         [$cvObject configure   Style   Font]
        set unitScale    [$cvObject configure   Stage   UnitScale] 
            #
        set stageFormat  [$cvObject configure   Stage   Format]   
        set stageWidth   [$cvObject configure   Stage   Width]   
        set stageHeight  [$cvObject configure   Stage   Height]   
            #
            # set cv           [cad4tcl::getNodeAttribute   $canvasDOMNode  Canvas   path ]
            # set wScale       [cad4tcl::getNodeAttribute   $canvasDOMNode  Canvas   scale ]      
            # set stageScale   [cad4tcl::getNodeAttribute   $canvasDOMNode  Stage  scale ]      
            # set stageUnit    [cad4tcl::getNodeAttribute   $canvasDOMNode  Stage  unit  ]      
            # set font         [cad4tcl::getNodeAttribute   $canvasDOMNode  Style    font  ]
            # set unitScale    [cad4tcl::_getUnitRefScale   $stageUnit]
            # 
            # set stageFormat  [cad4tcl::getNodeAttribute   $canvasDOMNode  Stage  format ]   
            # set stageWidth   [cad4tcl::getNodeAttribute   $canvasDOMNode  Stage  width  ]   
            # set stageHeight  [cad4tcl::getNodeAttribute   $canvasDOMNode  Stage  height ]   
        
            # set scalePixel   [ cad4tcl::getNodeAttributeRoot /root/_package_/UnitScale p ]  -> 20170909
            # set scaleInch    [ cad4tcl::getNodeAttributeRoot /root/_package_/UnitScale i ]  -> 20170909
            # set scaleMetric  [ cad4tcl::getNodeAttributeRoot /root/_package_/UnitScale m ]  -> 20170909
        

            # -------------------------
            #  get SVG-Units and scale 
        case $stageUnit {
            m  {   set svgUnit  "mm"; }
            c  {   set svgUnit  "cm"}
            i  {   set svgUnit  "in"}
            p  {   set svgUnit  "px"}
        }

            # -------------------------
            #  get canvs scaling and reposition
        set cv_ViewBox      [$cv coords __Stage__]
        set cv_View_x0      [lindex $cv_ViewBox 0]
        set cv_View_y0      [lindex $cv_ViewBox 1]
        set cv_View_x1      [lindex $cv_ViewBox 2]
        set cv_View_y1      [lindex $cv_ViewBox 3]
        set cv_ViewWidth    [expr {$cv_View_x1 - $cv_View_x0}]
        set cv_ViewHeight   [expr {$cv_View_y1 - $cv_View_y0}]
            #
        set svgScale        [expr {$wScale / $unitScale}]
            
            # -------------------------
            #  debug info
        puts "        --------------------------------------------"
        puts "           \$stageFormat $stageFormat  "
        puts "                   \$stageUnit      $stageUnit"
        puts "                   \$svgUnit        $svgUnit  "
        puts "                   \$unitScale      [format "%.5f"  $unitScale]"
        puts "                   \$stageWidth     $stageWidth  "
        puts "                   \$stageHeight    $stageHeight "
        puts "        --------------------------------------------"
        puts "               \$wScale         [ format "%.5f  %.5f"  $wScale        [expr {1.0/$wScale}]]"
        puts "               \$stageScale     [ format "%.5f  %.5f"  $stageScale    [expr {1.0/$stageScale}]]"
        puts "        --------------------------------------------"
        puts "               \$cv_ViewBox      $cv_ViewBox"
        puts "                   \$cv_View_x0      $cv_View_x0"
        puts "                   \$cv_View_y0      $cv_View_y0"
        puts "                   \$cv_View_x1      $cv_View_x1"
        puts "                   \$cv_View_y1      $cv_View_y1"
        puts "                   \$cv_ViewWidth    $cv_ViewWidth"
        puts "                   \$cv_ViewHeight   $cv_ViewHeight"
        puts "        --------------------------------------------"
        puts "               \$svgScale       ( $wScale / $unitScale )"
        puts "               \$svgScale       [ format "%.5f "  $svgScale ]"
        puts "        --------------------------------------------"
        puts "               \$svgFile        $svgFile"
        puts "        --------------------------------------------"
        
            
            # -------------------------
            #  create bounding boxes
            # $cv create prect          [$cv coords __Stage__]   \
                        -tags           {__SheetFormat__ __Content__ __temporary__}  \
                        -stroke         black    \
                        -strokewidth    0.01
                                        
            # -------------------------
            #  get svgViewBox
        set svgViewBox    [list  $cv_View_x0 $cv_View_y0 [expr {$svgScale * $stageWidth}]  [expr {$svgScale * $stageHeight}]]
            
            # -------------------------
            #  create bounding boxes
        set      svgContent  "<svg xmlns=\"http://www.w3.org/2000/svg\" \n"
        append   svgContent  "         width=\"$stageWidth$svgUnit\" \n"
        append   svgContent  "         height=\"$stageHeight$svgUnit\"\n"
        append   svgContent  "         viewBox=\"$svgViewBox\"\n"
        append   svgContent  "     >\n"
        
        append   svgContent  "<g  id=\"__root__\">\n\n"
            
            
        # ========================================================================
            # -------------------------
            #  for each item
            #
            #
        foreach cvItem   [$cv find withtag {__Content__}] {
        
            set cvType      [$cv type $cvItem]
            set svgCoords   {}
            set svgAtts     {}
                #
                # puts "    -> $cvType"
                #
                # set defaults
            if 0 {
                set lineDash    {}
                
                    # --- get attributes
                switch -exact -- $cvType {
                    line {
                        catch {set lineColour [my formatXColor  [$cv itemcget $cvItem -fill]]}              {set lineColour   gray50}
                        catch {set lineWidth                    [$cv itemcget $cvItem -width]}              {set lineWidth     0.1}
                        catch {set lineDash                     [$cv itemcget $cvItem -dash]}               {set lineDash     {none}}
                        catch {set itemFill                     {}}                                         {set itemFill     {}}
                    }
                    arc -
                    oval -
                    polygon -
                    rectangle -
                    text {
                        catch {set lineColour [my formatXColor  [$cv itemcget $cvItem -outline]]}           {set lineColour   gray50}
                        catch {set lineWidth                    [$cv itemcget $cvItem -width]}              {set lineWidth     0.1}
                        catch {set lineDash                     [$cv itemcget $cvItem -dash]}               {set lineDash     {none}}
                        catch {set itemFill   [my formatXColor  [$cv itemcget $cvItem -fill]]}              {set itemFill     gray50}
                    }
                    default {
                        catch {set lineColour [my formatXColor  [$cv itemcget $cvItem -stroke]]}            {set lineColour   gray50}
                        catch {set lineWidth                    [$cv itemcget $cvItem -strokewidth]}        {set lineWidth     0.1}
                        catch {set lineDash                     [$cv itemcget $cvItem -strokedasharray]}    {set lineDash     {none}}
                        catch {set itemFill   [my formatXColor  [$cv itemcget $cvItem -fill]]}              {set itemFill     gray50}
                    }
                }
                
                    # --- preformat attribues
                if {$lineDash eq ""} {
                    set lineDash    {none}
                } else {
                    set lineDash    [string map {{ } {,}} $lineDash]
                }
                
                puts "        -> [$cv itemconfigure $cvItem] "
                puts "        -> lineColour $lineColour "
                puts "        -> lineWidth  $lineWidth  "
                puts "        -> lineDash   $lineDash   "
                puts "        -> itemFill   $itemFill   "
            }
                #
                # --- get coords
            lassign [string map {".0 " " "} "[$cv coords $cvItem] "]  x0 y0 x1 y1
            
                # --- get coords
            set cvPoints {}
            foreach {x y} [$cv coords $cvItem] {
                lappend cvPoints [list $x $y]
            }
            
                # --- continue if object has no coords / geometry
            if {[llength $cvPoints] == 0} {
                puts "\n                ... <W> $cvItem -> $cvType  ... has no coords"
                continue
            }
        
            # -------------------------
            #  handle types      
            switch -exact $cvType {
            
                ________Canvas_like_Objects________ {}
                
                arc {
                    lassign [my CreateSVG_arc $cv $cvItem]          svgXML svgType svgAtts svgCoords
                }
                line {
                    lassign [my CreateSVG_line $cv $cvItem]         svgXML svgType svgAtts svgCoords 
                }
                oval {
                    lassign [my CreateSVG_oval $cv $cvItem]         svgXML svgType svgAtts svgCoords
                }
                polygon {
                    lassign [my CreateSVG_polygon $cv $cvItem]      svgXML svgType svgAtts svgCoords
                }
                rectangle {
                    lassign [my CreateSVG_rectangle $cv $cvItem]    svgXML svgType svgAtts svgCoords
                }
                text {
                    lassign [my CreateSVG_text $cv $cvItem]         svgXML svgType svgAtts svgCoords
                }
                
                ________PathCanvas_Objects_________ {}
                
                circle {
                    lassign [my CreateSVG_circle $cv $cvItem]       svgXML svgType svgAtts svgCoords
                }
                ellipse {
                    lassign [my CreateSVG_ellipse $cv $cvItem]      svgXML svgType svgAtts svgCoords
                }
                path {
                    lassign [my CreateSVG_path $cv $cvItem]         svgXML svgType svgAtts svgCoords
                }
                pline {
                    lassign [my CreateSVG_pline $cv $cvItem]        svgXML svgType svgAtts svgCoords
                }
                polyline {
                    lassign [my CreateSVG_polyline $cv $cvItem]     svgXML svgType svgAtts svgCoords
                }
                ppolygon {
                    lassign [my CreateSVG_polygon $cv $cvItem]      svgXML svgType svgAtts svgCoords
                }
                prect {
                    lassign [my CreateSVG_prect $cv $cvItem]        svgXML svgType svgAtts svgCoords
                }
                ptext {
                    set svgXML {}
                }               

                ________Exceptions_________________ {}
                
                pimage -
                group -
                default {
                    # error "type $type not(yet) dumpable to SVG"
                    puts "type $cvType not (yet) dumpable to SVG"
                    set svgXML {}
                }
            }
                
                # -------------------------
                #  report
                # puts "     -> $cvType"
                # puts "               $svgXML"
                # puts "               $svgType"
                # puts "               $svgAtts"
                # puts "               $svgCoords"
            
                # -------------------------
                #  canvas item attributes
            append svgContent   "    <!-- $cvType:-->\n"
            append svgContent   "        <!--    cvPoints:    $cvPoints   -->\n"
            append svgContent   "        <!--    svgCoords:   $svgCoords   -->\n"
                # 
            append svgContent   "$svgXML"
                #
        }
            #
        # ========================================================================
            # -------------------------
            #  center_00
            #
        lassign [$cv coords __Stage__] a b c d
            #
        set svgXML  "  <circle  id=\"center_00\"  cx=\"$a\"  cy=\"$d\"  r=\"0.0005\"  fill=\"none\"  stroke=\"none\"  />\n"
            # puts "         -> \$svgXML $svgXML"
            #
        append svgContent   "$svgXML"
            #
        
            # -- close svg-Tag
            #
        append svgContent "</g>\n"
        append svgContent "</svg>"


            #
            # -- fill export svgFile
            #
        if {$svgFile ne {}} {
            set fp      [open $svgFile w]          
            fconfigure  $fp -encoding utf-8
            puts        $fp $svgContent
            close       $fp
        }
        
            #
            # -- fill export svgFile
            #
        if {[file exists $svgFile]} {
            return $svgFile
        } else {
            return {_noFile_}
        }
        
    } 
        #
    method CreateSVG_line {cv cvItem} {
            #
            # puts "\n  --- Extension_SVG__Super - CreateSVG_polyline --- "    
            #
        set cvPoints {}
        foreach {x y}       [$cv coords $cvItem] {
            lappend cvPoints [list $x $y]
        }
            #
            #
        set svgType         polyline
            #
        set svgCoords       "points=\"[join $cvPoints ", "]\""
            #
            #
        set attrDictNew     [list   fill    "none"]
            #
        set attrDictMap     [list   stroke              {-fill      "none"}     \
                                    stroke-width        {-width     "0.1"}  \
                                    stroke-dasharray    {-dash      "12,1,1,1"}]
            #        
        set svgAtts         [my FormatSVGAttribute $cvItem $attrDictNew $attrDictMap]
            #
        set svgXML          "  <$svgType \n      $svgAtts \n      $svgCoords/>\n"
            #
            #
        if {[llength $cvPoints] == 0} {
                #
            puts "\n                ... <W> $cvItem -> [$cv type $cvItem]  ... has no coords"
                #
            return [list {}      $svgType $svgAtts $svgCoords]
                #
        } else {
                #
            return [list $svgXML $svgType $svgAtts $svgCoords]
                #
        }
    }
        #
    method CreateSVG_circle {cv cvItem} {
            #
        if 0 {
            puts "---> CreateSVG_circle -- [$cv coords $cvItem]"
            foreach attrList [$cv itemconfigure $cvItem] {
                set key     [lindex $attrList 0]
                set value   [lrange $attrList 1 end]
                puts "   -> $key - $value"
                puts "   -> $key - [$cv itemcget $cvItem $key]"
            }
        }
            #
        lassign [string map {".0 " " "} "[$cv coords $cvItem] "]  x y
            # foreach {x y} [string map {".0 " " "} "[$cv coords $cvItem] "] break
            #
        set cx              $x
        set cy              $y
        set r               [$cv itemcget $cvItem -r]
            #
            #
        set svgType         circle
            #
            #
        set attrDictNew     [list   cx      "$cx" \
                                    cy      "$cy" \
                                    r       "$r"  ]
            
        set attrDictMap     [list   fill                {-fill              "none"} \
                                    stroke              {-stroke            "none"} \
                                    stroke-width        {-strokewidth       "0.1"}  \
                                    stroke-dasharray    {-strokedasharray   "12,1,1,1"}]
            #        
        set svgAtts         [my FormatSVGAttribute $cvItem $attrDictNew $attrDictMap]
            #
        set svgXML          "  <$svgType \n      $svgAtts/>\n"
            #
            #
        return [list $svgXML $svgType $svgAtts {}]
            #
    }
        #
    method CreateSVG_ellipse {cv cvItem} {
        #
        lassign [string map {".0 " " "} "[$cv coords $cvItem] "]  x y
            # foreach {x y} [string map {".0 " " "} "[$cv coords $cvItem] "] break
            #
        set cx              $x
        set cy              $y
        set rx              [$cv itemcget $cvItem -rx]
        set ry              [$cv itemcget $cvItem -ry]
            #
            #
        set svgType         ellipse
            #
            #
        set attrDictNew     [list   cx      "$cx" \
                                    cy      "$cy" \
                                    rx      "$rx" \
                                    ry      "$ry" ]
            
        set attrDictMap     [list   fill                {-fill              "none"} \
                                    stroke              {-stroke            "none"} \
                                    stroke-width        {-strokewidth       "0.1"}  \
                                    stroke-dasharray    {-strokedasharray   "12,1,1,1"}]
            #        
        set svgAtts         [my FormatSVGAttribute $cvItem $attrDictNew $attrDictMap]
            #
        set svgXML          "  <$svgType \n      $svgAtts/>\n"
            #
            #
        return [list $svgXML $svgType $svgAtts {}]
            #
    }
        #
    method CreateSVG_path {cv cvItem} {
            #
        if 0 {    
            puts "---> CreateSVG_path -- [$cv coords $cvItem]"
            foreach attrList [$cv itemconfigure $cvItem] {
                set key     [lindex $attrList 0]
                set value   [lrange $attrList 1 end]
                puts "   -> $key - $value"
                puts "   -> $key - [$cv itemcget $cvItem $key]"
            }
        }
            #
            #
        set pathDefinition  [$cv coords $cvItem]   
            #
            #
        set svgType         path
            #
            #
        set attrDictNew     [list   d       "$pathDefinition"]
            
        set attrDictMap     [list   fill                {-fill              "none"} \
                                    fill-rule           {-fillrule          "none"} \
                                    stroke              {-stroke            "none"} \
                                    stroke-width        {-strokewidth       "0.1"}  \
                                    stroke-dasharray    {-strokedasharray   "12, 1, 1, 1"}]
            #        
        set svgAtts         [my FormatSVGAttribute $cvItem $attrDictNew $attrDictMap]
            #
        set svgXML          "  <$svgType \n      $svgAtts/>\n"
            #
            #
        return [list $svgXML $svgType $svgAtts {}]
            #
    }
        #
    method CreateSVG_pline {cv cvItem} {
            #
        puts "\n  --- Extension_SVG_PathCanvas - CreateSVG_pline --- "    
            #
        set cvPoints {}
        foreach {x y}       [$cv coords $cvItem] {
            lappend cvPoints [list $x $y]
        }
            #
            #
        set svgType         polyline
            #
        set svgCoords       "points=\"[join $cvPoints ", "]\""
            #
            #
        set attrDictNew     [list   fill    "none"]
            #
        set attrDictMap     [list   stroke              {-stroke            "none"} \
                                    stroke-width        {-strokewidth       "0.1"}  \
                                    stroke-dasharray    {-strokedasharray   "12,1,1,1"}]
            #        
        set svgAtts         [my FormatSVGAttribute $cvItem $attrDictNew $attrDictMap]
            #
        set svgXML          "  <$svgType \n      $svgAtts \n      $svgCoords/>\n"
            #
        if {[llength $cvPoints] == 0} {
                #
            puts "\n                ... <W> $cvItem -> [$cv type $cvItem]  ... has no coords"
                #
            return [list {}      $svgType $svgAtts $svgCoords]
                #
        } else {
                #
            return [list $svgXML $svgType $svgAtts $svgCoords]
                #
        }
    }       
        #
    method CreateSVG_polyline {cv cvItem} {
            #
            # puts "\n  --- Extension_SVG__Super - CreateSVG_polyline --- "    
            #
        set cvPoints {}
        foreach {x y}       [$cv coords $cvItem] {
            lappend cvPoints [list $x $y]
        }
            #
            #
        set svgType         polyline
            #
        set svgCoords       "points=\"[join $cvPoints ", "]\""
            #
            #
        set attrDictNew     [list   fill    "none"]
            #
        set attrDictMap     [list   stroke              {-stroke            "none"} \
                                    stroke-width        {-strokewidth       "0.1"} \
                                    stroke-dasharray    {-strokedasharray   "12,1,1,1"}]
            #        
        set svgAtts         [my FormatSVGAttribute $cvItem $attrDictNew $attrDictMap]
            #
        set svgXML          "  <$svgType \n      $svgAtts \n      $svgCoords/>\n"
            #
            #
        if {[llength $cvPoints] == 0} {
                #
            puts "\n                ... <W> $cvItem -> [$cv type $cvItem]  ... has no coords"
                #
            return [list {}      $svgType $svgAtts $svgCoords]
                #
        } else {
                #
            return [list $svgXML $svgType $svgAtts $svgCoords]
                #
        }
    }
        #
    method CreateSVG_polygon {cv cvItem} {
            #
        set cvPoints {}
        foreach {x y}       [$cv coords $cvItem] {
            lappend cvPoints [list $x $y]
        }
            #
            #
        set svgType         polygon
            #
        set svgCoords       "points=\"[join $cvPoints ", "]\""
            #
            #
        set attrDictNew     {}
            #
        set attrDictMap     [list   fill                {-fill              "none"} \
                                    stroke              {-stroke            "none"} \
                                    stroke-width        {-strokewidth       "0.1"}  \
                                    stroke-dasharray    {-strokedasharray   "12,1,1,1"}]
            #        
        set svgAtts         [my FormatSVGAttribute $cvItem $attrDictNew $attrDictMap]
            #
        set svgXML          "  <$svgType \n      $svgAtts \n      $svgCoords/>\n"
            #
            #
        if {[llength $cvPoints] == 0} {
                #
            puts "\n                ... <W> $cvItem -> [$cv type $cvItem]  ... has no coords"
                #
            return [list {}      $svgType $svgAtts $svgCoords]
                #
        } else {
                #
            return [list $svgXML $svgType $svgAtts $svgCoords]
                #
        }
    }
        #
    method CreateSVG_prect {cv cvItem} {
            #
        if 0 {
            puts "---> CreateSVG_prect -- [$cv coords $cvItem]"
            foreach attrList [$cv itemconfigure $cvItem] {
                set key     [lindex $attrList 0]
                set value   [lrange $attrList 1 end]
                puts "   -> $key - $value"
                puts "   -> $key - [$cv itemcget $cvItem $key]"
            }
        }
            #
        lassign [string map {".0 " " "} "[$cv coords $cvItem] "]  x y x1 y1
            # foreach {x y x1 y1} [string map {".0 " " "} "[$cv coords $cvItem] "] break
            #
            #
        set svgType         rect
            #
            #
        set attrDictNew     [list   x       "$x" \
                                    y       "$y" \
                                    width  [expr {$x1-$x}] \
                                    height [expr {$y1-$y}]]
            #
        set attrDictMap     [list   fill                {-fill              "none"} \
                                    stroke              {-stroke            "none"} \
                                    stroke-width        {-strokewidth       "0.1"} \
                                    stroke-dasharray    {-strokedasharray   "12,1,1,1"}]
            #        
        set svgAtts         [my FormatSVGAttribute $cvItem $attrDictNew $attrDictMap]
            #
            #
        set svgXML          "  <$svgType \n      $svgAtts/>\n"
            #
        return [list $svgXML $svgType $svgAtts {}]
            #
    }
        #
        #
    method CreateSVG_oval {cv cvItem} {
            #
            # puts "                  -> CreateSVG_oval -> CreateSVG_oval_template"
            #
        lassign [string map {".0 " " "} "[$cv coords $cvItem] "]  x y x1 y1
            # foreach {x y x1 y1} [string map {".0 " " "} "[$cv coords $cvItem] "] break
            #
        set cx              [expr {($x+$x1)/2}]
        set cy              [expr {($y+$y1)/2}]
        set rx              [expr {($x1-$x)/2}]
        set ry              [expr {($y1-$y)/2}]
            #
            #
        set svgType         ellipse
            #
            #
        set attrDictNew     [list   cx      "$cx" \
                                    cy      "$cy" \
                                    rx      "$rx" \
                                    ry      "$ry"]
            #
        set attrDictMap     [list   fill                {-fill      "none"}     \
                                    stroke              {-outline   "none"}     \
                                    stroke-width        {-width     "0.1"}  \
                                    stroke-dasharray    {-dash      "12,1,1,1"}]
            #        
        set svgAtts         [my FormatSVGAttribute $cvItem $attrDictNew $attrDictMap]
            #
        set svgXML          "  <$svgType \n      $svgAtts/>\n"
            #
            #
        return [list $svgXML $svgType $svgAtts {}]
            #
    }
        #
        #
}

