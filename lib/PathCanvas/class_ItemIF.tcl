 ########################################################################
 #
 #  Copyright (c) Manfred ROSENBERGER, 2017
 #
 #      package: cad4tcl 	->	classItemIF_PathCanvas.tcl
 #
 # ----------------------------------------------------------------------
 #  namespace:  cad4tcl
 # ----------------------------------------------------------------------
 #
 #  2017/11/26
 #      extracted from the rattleCAD-project (Version 3.4.05)
 #          http://rattlecad.sourceforge.net/
 #          https://sourceforge.net/projects/rattlecad/
 #
 #

oo::define cad4tcl::ItemInterface_PathCanvas {
        #
    # superclass cad4tcl::ItemInterface__Super    
        #
        #
    variable canvasObject
    variable canvasPath
        #
    variable DimensionFactory
    variable DXF
    variable SVG
        #
    variable canvasScale
        #
    variable Temporary_CanvasScale
    variable Temporary_StageScale 
    variable Temporary_StageFormat
    variable Temporary_StageWidth 
    variable Temporary_StageHeight
        #
    variable stageFormat
    variable stageScale
    variable stageUnit
    variable stageUnitScale
    variable stageWidth
    variable stageHeight
        # <Style  linewidth="0.35" linecolour="black" fontstyle="vector" fontsize="3.5" fontdist="1" font="Arial" fontcolour="black" precision="2" DefaultPrecision="2"/>
    variable styleLinewidth
    variable styleLinecolour
    variable styleFontstyle
    variable styleFontsize
    variable styleFontdist
    variable styleFont
    variable styleFontcolour
    variable stylePrecision
    variable styleDefaultPrecision
        #
    variable unitScale_p
        #
    variable DraftFrame
    variable DraftRaster
        #
    variable tagCount
        #
        #
    constructor {cvObj} {
            #
        puts "              -> class ItemInterface_PathCanvas"
        puts "                  -> $cvObj"
            #
        set canvasObject            $cvObj
            #
        set canvasPath              [$canvasObject getCanvas]
            #
        set packageHomeDir          $::cad4tcl::packageHomeDir
            #
            # ------- Create DimensionFactory ------------------------------
        set DimensionFactory        [cad4tcl::DimensionFactory          new $canvasObject [self]]     
        set DXF                     [cad4tcl::Extension_DXF_PathCanvas  new $canvasObject ]     
        set SVG                     [cad4tcl::Extension_SVG_PathCanvas  new $canvasObject [self]]
            #
            # ------- Create Format Extensions -----------------------------
            #
        set stageFormat             "A4"
            #
        set styleFontsize           2.5
            #
        set styleLinewidth          [expr {0.1 * $styleFontsize}]
        set styleLinecolour         "black" 
        set styleFontstyle          "vector" 
        set styleFontdist           [expr {0.1 * $styleFontsize}]
        set styleFont               "Arial" 
        set styleFontcolour         "black" 
        set stylePrecision          2 
        set styleDefaultPrecision   2
            #
        array set DraftFrame {
            label   {}    
            title   {}    
            date    {}    
            descr   {}
        }
        array set DraftRaster {
            minorDistance      5    
            minorColor      lightgray    
            majorDistance     10    
            majorColor      lightgray
        }
            #
        set tagCount        0
            #
        my unsetTemporaryScale      ;# set Temporary_... values
            #
        return
            #
    }
        #
    destructor {
        puts "            -> [self] ... destroy ItemInterface_PathCanvas"
    }
        #
    method unknown {target_method args} {
        puts "<E> ... cad4tcl::ItemInterface_PathCanvas $target_method $args  ... unknown"
    }
        #
        #
    method updateConfig {} {
            #
        # my configure  canvasScale             [$canvasObject configure Canvas Scale          ]
            #
        # my configure  stageFormat             [$canvasObject configure Stage Format          ]
        # my configure  stagePointScale         [$canvasObject configure Stage PointScale      ]
        # my configure  stageUnit               [$canvasObject configure Stage Unit            ]
        # my configure  stageUnitScale          [$canvasObject configure Stage UnitScale       ]
        # my configure  stageScale              [$canvasObject configure Stage Scale           ]
        # my configure  stageWidth              [$canvasObject configure Stage Width           ]
        # my configure  stageHeight             [$canvasObject configure Stage Height          ]
            #
        # my configure  styleLinewidth          [$canvasObject configure Style Linewidth       ]
        # my configure  styleLinecolour         [$canvasObject configure Style Linecolour      ]
        # my configure  styleFontstyle          [$canvasObject configure Style Fontstyle       ]
        # my configure  styleFontsize           [$canvasObject configure Style Fontsize        ]
        # my configure  styleFontdist           [$canvasObject configure Style Fontdist        ]
        # my configure  styleFont               [$canvasObject configure Style Font            ]
        # my configure  styleFontcolour         [$canvasObject configure Style Fontcolour      ]
        # my configure  stylePrecision          [$canvasObject configure Style Precision       ]
        # my configure  styleDefaultPrecision   [$canvasObject configure Style DefaultPrecision] 
            #
    }   
        #
    method create {type args} {
            #
            # routes args to the default implementation of canvas create ...
            #
            # puts "    -> cad4tcl::ItemInterface  create $type $args"
            #
        set moveOrigin  [$canvasObject getOrigin]
            #
        set coordList   [lindex $args 0]
        set argList     [lindex $args 1]
            #
            # https://github.com/pd-l2ork/pd/tree/master/pd/tkpath/doc
            # 	New items:
            #       circle
            #       ellipse
            #       group
            #       path
            #       pimage
            #       pline
            #       polyline
            #       ppolygon
            #       prect
            #       ptext
            #
            # ------ check for linewidth ----
        if ![dict exists $argList -width] {
            switch -exact -- $type {
                arc -
                circle -
                line -
                oval -
                ovalarc -
                ovalarc2 -
                path -
                polygon -
                rectangle {
                    lappend argList -width $styleLinewidth
                }
            }
        }
            #
        switch -exact -- $type {
            line -
            polyline {          set myItem [my createLine       $coordList  $argList]}
            centerLine {        set myItem [my createCenterLine $coordList  $argList]}
            hiddenLine {        set myItem [my createHiddenLine $coordList  $argList]}
            polygon {           set myItem [my createPolygon    $coordList  $argList]}
            rectangle {         set myItem [my createRectangle  $coordList  $argList]}
            oval {              set myItem [my createOval       $coordList  $argList]}       
            ovalarc {           set myItem [my createOvalArc    $coordList  $argList]}       
            ovalarc2 {          set myItem [my createOvalArc2   $coordList  $argList]}       
            arc {               set myItem [my createArc        $coordList  $argList]}
            circle {            set myItem [my createCircle     $coordList  $argList]}
            path {              set myItem [my createPath       $coordList  $argList]}
            text {              set myItem [my createText       $coordList  $argList]}
            vectorText {        set myItem [my createVectorText $coordList  $argList]}
            
            dimensionAngle {    set myItem [$DimensionFactory create  angle     $coordList $argList]}
            dimensionLength {   set myItem [$DimensionFactory create  length    $coordList $argList]}
            dimensionRadius {   set myItem [$DimensionFactory create  radius    $coordList $argList]}            
            
            draftFrame {        set myItem [my createDraftFrame             $argList]}
            draftLabel {        set myItem [my createDraftLabel $coordList  $argList]}
            draftLine {         set myItem [my createDraftLine  $coordList  $argList]}
            draftText {         set myItem [my createDraftText  $coordList  $argList]}
            
            draftRaster {       set myItem [my createDraftRaster            $argList]}
            
            svg {               set myItem [my createSVG        $coordList  $argList]}
            
            default {
                                puts "  <E> ... cad4tcl::ItemInterface_PathCanvas -> $type ... not defined"
                                return {}
            }
        }
            #
        if {![info exists myItem]} {
            puts "   <w> ... \$myItem empty"
            return
        }    
            #
        set w           $canvasPath            
            #
            # puts "          $type"
            #
        switch -exact -- $type {
            draftFrame -
            draftLabel -
            draftLine -
            draftRaster -
            svg -
            __exception__ {
                # ... no action required
            }
            __debug__ {
                # $canvasObject scale     $myItem  0 0  [expr {1.0/$stageUnitScale}] [expr {1.0/$stageUnitScale}]
                # $canvasObject bareMove  $myItem [lindex $moveOrigin 0] [lindex $moveOrigin 1]
                # $canvasObject addtag    {__Content__} withtag $myItem
            }
            default {
                $canvasObject scale     $myItem  0 0  [expr {1.0/$stageUnitScale}] [expr {1.0/$stageUnitScale}]
                $canvasObject bareMove  $myItem [lindex $moveOrigin 0] [lindex $moveOrigin 1]
                $canvasObject addtag    {__Content__} withtag $myItem
            }
        }    
            #
            # $w addtag {__StageItem__} withtag $myItem
            #
            #
            # $canvasObject raise {__Stage__ScaleReference__}    all    
            #
            # puts "     -> create \$myItem $myItem"
            #
        return $myItem
            #
    }
        #
    method itemconfigure {itemTag argList} {
            #
            # puts "-- itemconfigure -- $itemTag -----"
            #
        set itemType    [$canvasPath type $itemTag]
        if {$itemType eq {group}} {
            foreach childTag [$canvasPath children $itemTag] {
                my itemconfigure $childTag $argList
            }
            return
        }
        set argList     [my UpdateItemAttributes $itemType $argList noSize]
            # puts "             -> \$args $args"    
            # puts "      $canvasPath itemconfigure $itemTag [join $args]"
            # puts "-- itemconfigure -- $itemTag -- [$canvasObject classInfo]:$itemType "
        set attrList    [$canvasPath itemconfigure $itemTag]
            # puts " ----> $attrList"
        foreach key {-fill -stroke} {
                # puts "   ----> $key"
            set attrOrig [lindex [$canvasPath itemconfigure $itemTag $key] 4]
                # puts "     ----> $attrOrig"
            if {$attrOrig eq {}} {
                dict unset argList $key
            }
        }
        return [$canvasPath itemconfigure $itemTag {*}$argList]
            #
    }
        #
    method itemcget {itemTag option} {
            #
            # puts "\n      [self] itemconfigure $itemTag $argList"
        set type    [$canvasPath type $itemTag ]
            # puts "             -> \$type $type"    
            # puts "             -> \$argList $argList"    
            #
            # puts "             -> ItemInterface_PathCanvas"    
            # puts "             -> \$type    $type"    
            # puts "             -> \$option $option"    
        set argList [my UpdateItemAttributes $type [list $option {1}] noSize]
            # puts "             -> \$option $argList"
        set option  [lindex $argList 0]            
            # puts "             -> \$option $option"    
            # puts "      $canvasPath itemconfigure $itemTag [join $args]"
        return [eval $canvasPath itemcget $itemTag $option]
            #
    }
        #
    method getLineWidth {lineWidth} {
            #
        set newWidth [expr {$lineWidth * ($canvasScale / $stageUnitScale)}]
            #
        return $newWidth
            #
    }
        #
        #
    method setTemporaryScale {} {
            #
            # puts "    <I> ... setTemporaryScale"
            #
        set Temporary_CanvasScale   $canvasScale
        set Temporary_StageScale    $stageScale
        set Temporary_StageFormat   $stageFormat
        set Temporary_StageWidth    $stageWidth
        set Temporary_StageHeight   $stageHeight
            #
    }
        #
    method unsetTemporaryScale {} {
            #
        set Temporary_CanvasScale   {}
        set Temporary_StageScale    {}
        set Temporary_StageFormat   {}
        set Temporary_StageWidth    {}
        set Temporary_StageHeight   {}
            #
    }
        #
        #
    method export {fileFormat fileName} {
        switch -exact -- $fileFormat {
            dxf -
            DXF {
                return [my exportDXF $fileName]
            }
            svg -
            SVG {
                return [my exportSVG $fileName]
            }
            default {
                puts "   -> export \$fileFormat $fileFormat ... not defined"
                return {}
            }
        }
    }
        #
    method delete {item} {
            #
        foreach _item $item {
            switch -exact -- $_item {
                __Stage__ {}
                __StageShadow__ {}
                default {
                    $canvasPath delete $item
                }
            }
        }
            #
    }    
        #
    method configure {key {value __noValue__}} {
            #
        switch -exact -- $key {
            canvasScale -
            stageFormat -
            stagePointScale -
            stageUnit -
            stageUnitScale -
            stageScale -
            stageWidth -
            stageHeight -
            styleLinewidth -
            styleLinecolour -
            styleFontstyle -
            styleFontsize -
            styleFontdist -
            styleFont -
            styleFontcolour -
            stylePrecision -
            styleDefaultPrecision -
            unitScale_p {
                if {$value ne {__noValue__}} {
                    # puts "   -> $key $value"
                    set $key $value
                    return [set $key]
                } else {
                    # puts "   -> $key [set $key]"
                    return [set $key]
                }
            }
            canvasPath {
                    # puts "   -> $key [set $key]"
                    return [set $key]
            }
            default {
            }
        }
    }
        #
    method UpdateItemAttributes {type argDict formatSize} {
            #
            # -- <A0  width="1189"   height="841"  unit="m"	f1="10.0"  f2="7.5"  f3="5.0" />
            #    formatSize: [f1, f2, f3]   ... belongs to fontSize
            #                     lineWidth ... $fontSize / 10
            #
        # set formatSize {noSize}
            #
            #
            # puts "   UpdateItemAttributes -> \$type $type"
            # puts "             -> \$argDict $argDict"
            #
        set typePolyline    polyline
            #
        switch -exact -- $type {
            circle {
                set mapDict {-radius -r -outline -stroke -width -strokewidth}
            }
            ellipse -
            path -
            ppolygon -
            prect {
                set mapDict {-outline -stroke -width -strokewidth}
            }
            pline -
            centerline -
            hiddenline -
            polyline {
                set typePolyline    $type
                set type            polyline
                set mapDict {-outline -stroke -width -strokewidth}
            }
            group -
            ptext -
            pimage -
            default {
                    # puts "   <E> $type"
                set mapDict {}
            }
        }
            #
            # puts "   -> \$mapDict $mapDict"    
            #
            # --- map argumentNames
            #
        if {$mapDict ne {}} {
            dict for {key newKey} $mapDict {
                    # puts "         ... $key -> $newKey"
                if [dict exist $argDict $key] {
                    set value [dict get $argDict $key]
                    dict set argDict $newKey $value
                    dict unset argDict $key
                } else {
                    # puts "             -> $key ... not existing"
                }
            }
        }
            #
            # puts " -> \$argDict ($type):   $argDict  <-"    
            #
            # --- cleanup dict
            #
        dict for {key value} $argDict {
                # puts "         ... $key -> $value"
            switch -exact -- $key {
                -fill -
                -stroke {
                    if {$value eq {}} {
                        # dict set argDict $key "none"
                        # puts $argDict
                    }
                }
            }
        }
            #
            # -- exception for rectangle
            #
        switch -exact -- $type {
            path -
            polygon -
            prect -
            circle -
            default {
                dict for {key value} $argDict {
                    switch -exact -- $key {
                        -fill -
                        -stroke {
                            if {$value eq {none}} {
                                dict set argDict $key {}
                            }
                        }
                        default {}
                    }
                }
            }
        }
            #
            # --- handle lineWidth, dashArray, ...
            #
        if {$Temporary_CanvasScale == {}} {
            set wth_Scale   [expr {1.0 * $canvasScale / $stageUnitScale}]
        } else {
            set wth_Scale   [expr {1.0 * $Temporary_CanvasScale / $stageUnitScale}]
        }
            #
        # set wth_Scale   [expr {1.0 * $canvasScale / $stageUnitScale}]
            #
            # puts "    --> ... \$canvasScale      $canvasScale"
            # puts "    --> ... \$stageScale       $stageScale"
            # puts "    --> ... \$stageUnitScale   $stageUnitScale"
            # puts " -----> ... \$wth_Scale        $wth_Scale"
            #
            # puts "    \$canvasScale \$stageScale \$stageUnitScale \$wth_Scale"
            # puts "    $canvasScale $stageScale $stageUnitScale $wth_Scale"
            #
        switch -exact -- $formatSize {
            {noSize} {
                    #
                    # -- strokewidth
                    #
                if {[dict exist $argDict -strokewidth]} {
                        #
                    set inputWidth  [dict get $argDict -strokewidth]
                        #
                } else {
                        #
                    set formatSize  f1
                    set stageFormat A4
                    set fontSize    [cad4tcl::_getFontSize $stageFormat $formatSize]
                    set inputWidth  [expr {0.1 * $fontSize}]
                        #
                }
                    #
                set lineWidth   [expr {$inputWidth * $wth_Scale}]
                dict set argDict -strokewidth $lineWidth
                    #
                    # -- strokedasharray
                    #       
                if {$inputWidth > 0} {
                    if {$lineWidth > 1.0} {
                        set lng_Scale   [expr {1.0 / $inputWidth}]
                            # puts " =========> $lineWidth - $inputWidth -> \$lng_Scale $lng_Scale "
                    } else {
                            # ... it seems, that the relation between linewidth and strokedasharray changes on $lineWidth < 1.0!
                        set lng_Scale   [expr {1.0 * $lineWidth / $inputWidth}]
                            # puts " =========> $lineWidth - $inputWidth -> \$lng_Scale $lng_Scale ... ( $lineWidth < 1.0 )"
                    }
                } else {
                    set lng_Scale   1.0
                }
                if {[dict exist $argDict -strokedasharray]} {
                        #
                    set dashArray   [dict get $argDict -strokedasharray]
                    set newArray    {}
                    foreach dashLength $dashArray {
                        lappend newArray [expr {1.0 * $dashLength * $lng_Scale}]
                            #   newArray [expr {1.0 * $dashLength / $stageUnitScale}]
                    }
                    dict set argDict -strokedasharray $newArray
                        #
                }
                    #
            }
            default {
                    #
                    # -- handle exception
                    #
                if {$formatSize eq {}} {
                    set formatSize f1
                }
                if {$stageFormat eq {passive}} {
                    set stageFormat A4
                }
                    #
                    # -- lineWidth
                    #
                set fontSize    [cad4tcl::_getFontSize $stageFormat $formatSize]
                set inputWidth  [expr {0.1 * $fontSize}]
                    #
                    # puts "    ----> \$fontSize $fontSize <- $inputWidth"
                    #
                    # -- strokewidth
                    #
                if {[dict exist $argDict -strokewidth]} {
                        #
                    set inputWidth   [dict get $argDict -strokewidth]
                        # puts "    ----> \$inputWidth $inputWidth <- strokewidth"
                        #
                }
                    #
                set lineWidth   [expr {$inputWidth * $wth_Scale}]
                    # puts "    ----> \$lineWidth $lineWidth"
                    #
                dict set argDict -strokewidth $lineWidth
                    #
                    #
                    #
                    # -- strokedasharray
                    #
                if {$inputWidth > 0} {
                    if {$lineWidth > 1.0} {
                        set lng_Scale   [expr {1.0 / $inputWidth}]
                            # puts " =========> $lineWidth - $inputWidth -> \$lng_Scale $lng_Scale "
                    } else {
                            # ... it seems, that the relation between linewidth and strokedasharray changes on $lineWidth < 1.0!
                        set lng_Scale   [expr {1.0 * $lineWidth / $inputWidth}]
                            # puts " =========> $lineWidth - $inputWidth -> \$lng_Scale $lng_Scale ... ( $lineWidth < 1.0 )"
                    }
                } else {
                    set lng_Scale   1.0
                }
                    #
                if {[dict exist $argDict -strokedasharray]} {
                        #
                    set dashArray   [dict get $argDict -strokedasharray]
                        # puts "         001 -strokedasharray -> $dashArray <- $typePolyline"
                        #
                    set newArray    {}
                    foreach dashLength $dashArray {
                            # lappend newArray [expr {$dashLength * 1}]
                        lappend newArray [expr {$dashLength * $lng_Scale}]
                            #   newArray [expr {$dashLength * $canvasScale / ($lineWidth * $unitScale_p)}]
                    }
                        # puts "         001 -strokedasharray -> $newArray"
                    dict set argDict -strokedasharray $newArray
                        #
                } else {
                        #
                        # --- exception for centerline & hiddenline
                        #
                    switch -exact -- $typePolyline {
                        centerline {
                            set lngShort    [expr { 1.0 * $lng_Scale}]
                            set lngLong     [expr {12.0 * $lng_Scale}]
                                # puts "         centerline: $wth_Scale -> [list $lngLong $lngShort $lngShort $lngShort]"
                            dict set argDict -strokedasharray [list $lngLong $lngShort $lngShort $lngShort]
                        }
                        hiddenline {
                            set lngShort    [expr { 1.0 * $lng_Scale}]
                            set lngLong     [expr { 5.0 * $lng_Scale}]
                                # puts "         hiddenline: $wth_Scale -> [list $lngLong $lngShort]"
                            dict set argDict -strokedasharray [list $lngLong $lngShort]
                        }
                        default {
                            # puts "      -----> ... default: $typePolyline"
                        }
                    }
                
                }
                    #            
            }
        }
            #
            # set lineWidth   [dict get $argDict -strokewidth]
            # puts "    \$lineWidth $lineWidth"
            # puts "     \$argDict:  $argDict"
            # puts " -----"
            #
        return $argDict
            #
            #
    }
        #
        #
    method createArc {coordList argList} {
            #
        set w           $canvasPath
            #
        set posCenter   $coordList
            #
        set argList     [my UpdateItemAttributes  path       $argList  f1]
        lappend argList -strokelinecap round  -strokelinejoin round
            #
        set radius      [dict get $argList -radius]
        set startAngle  [dict get $argList -start]
        set extendAngle [dict get $argList -extent]
            #
        if [dict exists $argList -style] {
            set style   [dict get $argList -style]
        } else {
            set style   pieslice
        }
            #
        dict unset argList -radius
        dict unset argList -start
        dict unset argList -extent
        dict unset argList -style
            #
        set posStart    [cad4tcl::math::rotateLine  $posCenter $radius   [expr {1.0 * $startAngle}]]
        set posEnd      [cad4tcl::math::rotatePoint $posCenter $posStart $extendAngle]
            #
            #
        set radius      [expr {1.0 * $radius * $canvasScale * $stageScale}]
            #
        if {[expr {abs($extendAngle)}] > 180} {
            set largeArcFlag 1
        } else {
            set largeArcFlag 0
        }
            #
        if {$extendAngle > 0} {
            set sweepFlag 0
        } else {
            set sweepFlag 1
        }
            #
        lassign [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $posCenter]]  centx centy
        lassign [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $posStart]]   startx starty
        lassign [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $posEnd]]     endx endy   
            # foreach {centx centy}   [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $posCenter]]  break
            # foreach {startx starty} [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $posStart]]   break
            # foreach {endx endy}     [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $posEnd]]     break
            # puts "    \$args $args"
            # puts "        ... \$radius          $radius"
            # puts "        ... \$posStart        $posStart"
            # puts "        ... \$posEnd          $posEnd"
            # puts "        ... \$largeArcFlag    $largeArcFlag"
            # puts "        ... \$sweepFlag       $sweepFlag"
            #
        switch -exact -- $style {
            arc {
                set pathString  [format {M %s %s A %s %s 0 %i %i %s %s}             $startx $starty $radius $radius $largeArcFlag $sweepFlag $endx $endy]
                dict unset argList -fill
            }
            chord {
                set pathString  [format {M %s %s A %s %s 0 %i %i %s %s z}           $startx $starty $radius $radius $largeArcFlag $sweepFlag $endx $endy]
            }
            pieslice -
            default {
                set pathString  [format {M %s %s A %s %s 0 %i %i %s %s L %s %s z}   $startx $starty $radius $radius $largeArcFlag $sweepFlag $endx $endy $centx $centy]
            }
        }
            # puts "        ... \$pathString $pathString"
            #
            # set argList     [my UpdateArgDict_LineWidth $argList]
            #
            # puts "\n --- path  \"$pathString\" $argList"
        set myItem      [eval $w create path    \"$pathString\"  $argList]
            #
        return $myItem
            #
    }
    method createCenterLine {coordList argList} {
            #
        set w           $canvasPath
            #
        set coordList   [join [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $coordList]]]
            #
            # puts " -------------------------------------------------------"
            # puts " --- createCenterLine - polyline $coordList $argList"
        set argList     [my UpdateItemAttributes  centerline  $argList  f3]
            #
            # puts "  -> $argList"
            # lappend argList -strokelinecap round  -strokelinejoin round
            #
            # set lineWidth   [dict get $argList -strokewidth]
            # set strokeDash  [dict get $argList -strokedasharray]
            # puts " --- createCenterLine - lineWidth  $lineWidth"
            # puts " --- createCenterLine - strokeDash $strokeDash"
            # puts " -------------------------------------------------------"
            # puts " --- createCenterLine - polyline $coordList $argList\n"
            #
        set myItem      [eval $w create polyline    $coordList  $argList]
            #
        return          $myItem
            #
    }
    method createHiddenLine {coordList argList} {
            #
        set w           $canvasPath
            #
        set coordList   [join [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $coordList]]]
            #
            # puts "  -> $argList"
            #
            #
        set argList     [my UpdateItemAttributes  hiddenline    $argList  f3]
            # lappend argList -strokelinecap round  -strokelinejoin round
            #
            # puts "\n --- createHiddenLine - polyline $coordList $argList"
            #
            # set lineWidth   [dict get $argList -strokewidth]
            # set strokeDash  [dict get $argList -strokedasharray]
            # puts " --- createHiddenLine - lineWidth  $lineWidth"
            # puts " --- createHiddenLine - strokeDash $strokeDash"
            #
        set myItem      [eval $w create polyline    $coordList  $argList]
            #
        return          $myItem
            #
    }
    method createCircle {coordList argList} {
            #
        set w           $canvasPath
            #
        set coordList   [join [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $coordList]]]
            #
        lassign $coordList  cx cy
            # foreach {cx cy} $coordList break
            #
            # puts "  createCircle -> $argList"
            # puts "---000---"
        set argList     [my UpdateItemAttributes  circle     $argList  f1]
            #
            # puts "---001---"
        set radius      [dict get $argList -r]
        dict set argList    -r [expr {$radius * $canvasScale * $stageScale}]
            # puts "---002---"
            #
        if [dict exist $argList -activefill] {
                #
                # ... this is a tkpath circle
                #
            set radius      [dict get $argList -r]
            dict unset argList -r    
                #
            set x1 [expr {$cx - $radius}]
            set x2 [expr {$cx + $radius}]
            set y1 [expr {$cy - $radius}]
            set y2 [expr {$cy + $radius}]    
            set coords      [list   $x1 $y1 $x2 $y2]    
                #
                # puts $argList
                #
            if [dict exists $argList -stroke] {
                dict set argList -outline [dict get $argList -stroke]
                dict unset argList -stroke 
            }
            if [dict exists $argList -strokewidth] {
                dict set argList -width   [dict get $argList -strokewidth]
                dict unset argList -strokewidth 
            }
                #
            set myItem      [eval $w create oval $coords $argList]    
                #
            return          $myItem    
                #
                #
        }
            #
            # puts "\n --- createCircle - circle $cx $cy $args"
        set myItem      [eval $w create circle $cx $cy $argList]
            #
        return          $myItem
            #
    }
    method createLine {coordList argList} {
            #
        set w           $canvasPath
            #
        set coordList   [join [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $coordList]]]
            #
        set argList     [my UpdateItemAttributes  polyline   $argList  f1]
        lappend argList -strokelinecap round  -strokelinejoin round
            #
            # puts "\n --- createLine - polyline $coordList $argList"
            # puts "\n --- createLine - polyline"
            # puts "          $coordList "
            # puts "          $argList"
        dict set argList -fill {}
            # puts "          $argList"
        set myItem      [$w create polyline    {*}$coordList  {*}$argList]
            #
        return          $myItem
            #
    }
    method createOval {coordList argList} {
            #
        set w           $canvasPath
            #
        set coordList   [join [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $coordList]]]
            #
        lassign [cad4tcl::_getBBoxInfo  $coordList   center]  cx cy 
        lassign [cad4tcl::_getBBoxInfo  $coordList   size]    dx dy 
            # foreach {cx cy} [cad4tcl::_getBBoxInfo  $coordList   center] break
            # foreach {dx dy} [cad4tcl::_getBBoxInfo  $coordList   size]   break
        set rx          [expr {abs(0.5 * $dx)}]
        set ry          [expr {abs(0.5 * $dy)}]
            #
        set argList     [my UpdateItemAttributes  ellipse    $argList  f1]
            #
            # puts "\n --- createOval - ellipse $cx $cy -rx $rx -ry $ry $args]"
        set myItem      [eval $w create ellipse  $cx $cy  -rx $rx  -ry $ry  $argList]
            #
        return          $myItem
            #
    }
    method createOvalArc {coordList argList} {
            #
        set w           $canvasPath
            #
        set wScale      $canvasScale            
        set stageScale  $stageScale            
            # 
        set argList     [my UpdateItemAttributes  path       $argList  f1]
        lappend argList -strokelinecap round  -strokelinejoin round
            #
        lassign $coordList  x0 y0 x1 y1
            # foreach {x0 y0 x1 y1} $coordList break
        set radius1     [expr 0.5 * abs($x1 - $x0)]
        set radius2     [expr 0.5 * abs($y1 - $y0)]
            #
        set posCenter   [cad4tcl::_getBBoxInfo $coordList center]    
            #
        set startAngle  [dict get $argList -start]
        set extendAngle [dict get $argList -extent]
            #
        if {[dict exists $argList -style]} {
            set style   [dict get $argList -style]
        } else {
            set style   pieslice
        }
            #
        dict unset argList -start
        dict unset argList -extent
        dict unset argList -style
            #
        set posStart    [my getPointOnEllipse $radius1 $radius2 $startAngle]
        set posEnd      [my getPointOnEllipse $radius1 $radius2 [expr $startAngle + $extendAngle]]
            #
        set posStart    [cad4tcl::math::addVector $posCenter $posStart]
        set posEnd      [cad4tcl::math::addVector $posCenter $posEnd]
            # posStart  [::math::geometry::+ $posCenter $posStart]
            # posEnd    [::math::geometry::+ $posCenter $posEnd]
            #
        if {[expr abs($extendAngle)] > 180} {
            set largeArcFlag 1
        } else {
            set largeArcFlag 0
        }
            #
        if {$extendAngle > 0} {
            set sweepFlag 0
        } else {
            set sweepFlag 1
        }
            #
        lassign [cad4tcl::_convertBottomLeft [expr $canvasScale * $stageScale] [join $posCenter]]  centx centy   
        lassign [cad4tcl::_convertBottomLeft [expr $canvasScale * $stageScale] [join $posStart]]   startx starty 
        lassign [cad4tcl::_convertBottomLeft [expr $canvasScale * $stageScale] [join $posEnd]]     endx endy     
            # foreach {centx centy}   [cad4tcl::_convertBottomLeft [expr $canvasScale * $stageScale] [join $posCenter]]  break
            # foreach {startx starty} [cad4tcl::_convertBottomLeft [expr $canvasScale * $stageScale] [join $posStart]]   break
            # foreach {endx endy}     [cad4tcl::_convertBottomLeft [expr $canvasScale * $stageScale] [join $posEnd]]     break
            #
        set radius1     [expr 1.0 * $radius1 * $canvasScale * $stageScale]
        set radius2     [expr 1.0 * $radius2 * $canvasScale * $stageScale]
            #
        switch -exact -- $style {
            arc {
                set pathString  [format {M %s %s A %s %s 0 %i %i %s %s}             $startx $starty $radius1 $radius2 $largeArcFlag $sweepFlag $endx $endy]
                dict unset argList -fill
            }
            chord {
                set pathString  [format {M %s %s A %s %s 0 %i %i %s %s z}           $startx $starty $radius1 $radius2 $largeArcFlag $sweepFlag $endx $endy]
            }
            pieslice -
            default {
                set pathString  [format {M %s %s A %s %s 0 %i %i %s %s L %s %s z}   $startx $starty $radius1 $radius2 $largeArcFlag $sweepFlag $endx $endy $centx $centy]
            }
        }
            #
        set myItem      [eval $w create path    \"$pathString\"  $argList]
            #
        return $myItem
            #
    }
    method createOvalArc2 {coordList argList} {
            #
        set w           $canvasPath
            #
        set wScale      $canvasScale            
        set stageScale  $stageScale            
            # 
        set argList     [my UpdateItemAttributes  path       $argList  f1]
        lappend argList -strokelinecap round  -strokelinejoin round
            #
        lassign $coordList  x0 y0 x1 y1
            # foreach {x0 y0 x1 y1} $coordList break
        set radius1     [expr {0.5 * abs($x1 - $x0)}]
        set radius2     [expr {0.5 * abs($y1 - $y0)}]
            #
        set posCenter   [cad4tcl::_getBBoxInfo $coordList center]    
            #
            # my create circle [list $posCenter] [list -radius 25 -outline darkred]
            #
        set startAngle  [dict get $argList -start]
        set extendAngle [dict get $argList -extent]
            #
        if {[dict exists $argList -style]} {
            set style   [dict get $argList -style]
        } else {
            set style   pieslice
        }
            #
        dict unset argList -start
        dict unset argList -extent
        dict unset argList -style
            #
        set posStart    [my getPointOnEllipse2 $radius1 $radius2 $startAngle]
        set posEnd      [my getPointOnEllipse2 $radius1 $radius2 [expr {$startAngle + $extendAngle}]]
            #
        set  posStart   [cad4tcl::math::addVector $posCenter $posStart]
        set  posEnd     [cad4tcl::math::addVector $posCenter $posEnd]
            # posStart    [::math::geometry::+ $posCenter $posStart]
            # posEnd      [::math::geometry::+ $posCenter $posEnd]
            #
            # my create circle [list $posStart] [list -radius 2 -outline red]
            # my create circle [list $posEnd]   [list -radius 2 -outline blue]
            #
        if {[expr {abs($extendAngle)}] > 180} {
            set largeArcFlag 1
        } else {
            set largeArcFlag 0
        }
            #
        if {$extendAngle > 0} {
            set sweepFlag 0
        } else {
            set sweepFlag 1
        }
            #
        lassign [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $posCenter]]  centx centy 
        lassign [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $posStart]]   startx starty
        lassign [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $posEnd]]     endx endy     
            # foreach {centx centy}   [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $posCenter]]  break
            # foreach {startx starty} [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $posStart]]   break
            # foreach {endx endy}     [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $posEnd]]     break
            #
            # puts "    \$args $args"
            # puts "        ... \$radius1         $radius1"
            # puts "        ... \$radius2         $radius2"
            # puts "        ... \$posStart        $posStart"
            # puts "        ... \$posEnd          $posEnd"
            # puts "        ... \$largeArcFlag    $largeArcFlag"
            # puts "        ... \$sweepFlag       $sweepFlag"
        set radius1     [expr {1.0 * $radius1 * $canvasScale * $stageScale}]
        set radius2     [expr {1.0 * $radius2 * $canvasScale * $stageScale}]
        #
        switch -exact -- $style {
            arc {
                set pathString  [format {M %s %s A %s %s 0 %i %i %s %s}             $startx $starty $radius1 $radius2 $largeArcFlag $sweepFlag $endx $endy]
                dict unset argList -fill
            }
            chord {
                set pathString  [format {M %s %s A %s %s 0 %i %i %s %s z}           $startx $starty $radius1 $radius2 $largeArcFlag $sweepFlag $endx $endy]
            }
            pieslice -
            default {
                set pathString  [format {M %s %s A %s %s 0 %i %i %s %s L %s %s z}   $startx $starty $radius1 $radius2 $largeArcFlag $sweepFlag $endx $endy $centx $centy]
            }
        }
            # puts "        ... \$pathString $pathString"
            #
            # puts "\n --- path  \"$pathString\"  [join $args]"
        set myItem      [eval $w create path    \"$pathString\"  $argList]
            #
        return $myItem
            #
    }
    method createPath {pathDef argList} {
            #
        set w           $canvasPath
            #
        set matrix_Base {1 0 0 1 0 0}
            #
        set scale   [expr {$canvasScale * $stageScale}]    
            #
            # puts "------ createPath -------------------------------------------"
            # puts "   -> \$pathDef       $pathDef"
            # puts "   -> \$argList       $argList"
            #       
       if 0 {
            if {[dict exists $argList -matrixList]} {
                    #
                set matrixList  [dict get $argList -matrixList]
                    #
                    # puts "------ 001 -------------------------------------------"
                    # puts "   -> \$pathDef       $pathDef"
                    # puts "   -> \$argList       $argList"
                    # puts "   -> \$matrixList    $matrixList"
                    #
                set matrixScale [cad4tcl::math::matrixScale $matrix_Base $scale]
                    #
                set matrixList [linsert $matrixList 0 $matrixScale]
                    # puts "   -> \$matrixList $matrixList"
                    #
                set matrix  [cad4tcl::math::matrixMultiplyList $matrixList]    
                    #
            }
        }
            #
            # puts "   -> \$pathDef $pathDef "
            #
        set pathDef [my ConvertPath2Absolute $pathDef $scale]
            #
            # puts "   -> \$pathDef       $pathDef"
            # puts "------ 002 -------------------------------------------"
            # puts ""
            #
        set argList     [my UpdateItemAttributes  path  $argList  f1]
        lappend argList -fillrule evenodd  -strokelinecap round  -strokelinejoin round
            #
        set myItem      [$w create  path  $pathDef {*}$argList]
            #
        return          $myItem
            #
    }
    method createPolygon {coordList argList} {
            #
        set w           $canvasPath
            #
        set coordList   [join [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $coordList]]]
            #
        set argList     [my UpdateItemAttributes  ppolygon   $argList  f1]
        lappend argList -strokelinecap round  -strokelinejoin round
            #
            # puts "\n --- createPolygon - ppolygon $coordList  $args"
        set myItem      [eval $w create ppolygon    $coordList  $argList]
            #
        return          $myItem
            #
    }
    method createRectangle {coordList argList} {
            #
        set w           $canvasPath
            #
        set coordList   [cad4tcl::_convertBottomLeft [expr {$canvasScale * $stageScale}] [join $coordList]]
            #
        set argList     [my UpdateItemAttributes  prect      $argList  f1]
        lappend argList -strokelinecap round  -strokelinejoin round
            #
        set myItem      [eval $w create prect   $coordList  $argList]
            # set myItem      [eval $w create rectangle   $coordList  [cad4tcl::_flattenNestedList $args]]
            #
        return          $myItem
            #
    }
    method createText {coordList argList} {
            #
        set w           $canvasPath            
            #
        set moveOrigin  [$canvasObject getOrigin]
            #
        set font        Helvetica
        set fontSize    8
        set myText      {}
        set anchor      "se"
            #
        set orgList $argList    
            #
        foreach key {-font -size -text -anchor} {
            if [dict exists $argList $key] {
                switch -exact $key {
                    -font { 
                        set font        [dict get $argList $key]
                    }
                    -size { 
                        set fontSize    [dict get $argList $key]
                        set fontSize    [expr {round($fontSize)}]
                    }
                    -text { 
                        set myText      [dict get $argList $key]
                    }
                    -anchor { 
                        set anchor      [dict get $argList $key]
                    }
                    default {}
                }
                set argList [dict remove $argList $key]
            }
        }
            #
        set myFont      [font create]
        font configure  $myFont  -family Helvetica  -size $fontSize
        set fontDescent [font metrics $myFont -descent]
            #
        set lineRatio   [expr {(1.0 * $fontSize + $fontDescent) / $fontSize}]
        set formatSize  [expr {round(1.0 * ($lineRatio * $fontSize * $canvasScale * $stageScale * ($unitScale_p / $stageUnitScale)))}]
            #
        font configure  $myFont  -size $formatSize
            #
        dict set argList -font      $myFont
        dict set argList -anchor    $anchor
        dict set argList -text      $myText
            #
        set coordList   [cad4tcl::_convertBottomLeft [expr $canvasScale * $stageScale] [join $coordList]]
            #
        switch -exact -- $anchor {
            sw -
            s -
            se {
                set fontDescent [font metrics $myFont -descent]
                set dy [expr {$fontDescent * $stageUnitScale}]
                    # puts "   -> $formatSize + $fontDescent <- $dy"
                set coordList [cad4tcl::math::addVector $coordList [list 0 $dy]]
                    # coordList [::math::geometry::+  $coordList [list 0 $dy]]
                    # 
            }
            default {}
        }
            #
        set myItem      [eval $canvasPath create text  $coordList  $argList]
            #
        return          $myItem
            #
    }
        #
    method createVectorText {coordList argList} {
            #
            # puts "   -> createVectorText_New"    
            # puts "       -> \$coordList $coordList"    
            # puts "       -> \$argList $argList"    
            #
        set fontSize    3.5
        set argDict     {-angle 0.0 -outline black -width 0.35}     
        set tagList     {}
            #
        foreach key {-text -anchor -angle -outline -width -size -tags} {
            if [dict exist $argList $key] {
                switch -exact $key {
                    -text { 
                        set myText      [dict get $argList $key]
                    }
                    -size { 
                        set fontSize    [dict get $argList $key]
                    }
                    default { 
                        set value       [dict get $argList $key]
                        dict set argDict $key   $value
                    }
                }
             }
        }
            #
        dict set argDict  -strokelinecap  round 
        dict set argDict  -strokelinejoin round
            #
        incr tagCount
            #
        set txtObject   [cad4tcl::VectorText_PathCanvas new $canvasObject [self]  $::cad4tcl::vectorFontRoot $tagCount $myText $coordList $fontSize $argDict]    
            #
        set myItem      [$txtObject getTag]
        $txtObject      destroy             ; # the object itself is not required anymore 
            #
        return          $myItem
            #
    }
    
    
        #
    method createDraftFrame {argList} {
            #
            # set stageFormat $Stage(Format)
            # set stageWidth  $Stage(Width)
            # set stageHeight $Stage(Height)
            # set stageScale  $Stage(Scale)
            #
            #
        set itemTag __DraftFrame__    
            #
        my delete $itemTag
            #
        set label   {}    
        set title   {}    
        set date    {}    
        set descr   {}    
        foreach key [dict keys $argList] {
            switch -exact $key {
                -label {
                    set label [dict get $argList -label]
                }    
                -title {
                    set title   [dict get $argList -title]
                }    
                -date {
                    set date    [dict get $argList -date]
                }    
                -description -
                -descr {
                    set descr   [dict get $argList -descr]
                } 
            }
        }
            #
        set DraftFrame(label)        $label
        set DraftFrame(title)        $title
        set DraftFrame(date)         $date 
        set DraftFrame(descr)        $descr
            #
        puts ""
        puts "   -------------------------------"
        puts "    [self] createDraftFrame"
        puts "       stageFormat:   $stageFormat"
        puts "           label:     $label"
        puts "           title:     $title"
        puts "            date:     $date"
            #
            #

            # --- drafting frame
        set df_Border          5
        set df_Width        [expr {$stageWidth  - 2 * $df_Border}]
        set df_Height       [expr {$stageHeight - 2 * $df_Border}]
        set tb_Width         170
        set tb_Height         20
        set tb_BottomLeft   [expr {$stageWidth  - $df_Border  - $tb_Width}]
            # ----- outer frame
        set x_00            $df_Border
        set x_01            [expr {$df_Border + $df_Width}]
        set y_00            $df_Border
        set y_01            [expr {$df_Border + $df_Height}]
            # ----- title frame
        set x_10            [expr {$x_00 + $tb_BottomLeft}]
        set x_11            [expr {$x_10 + 18}]
        set x_12            [expr {$x_01 - 45}]
        set x_13            [expr {$x_01}]
        set y_10            $y_00
        set y_11            [expr {$y_00 + 11}]
        set y_12            [expr {$y_00 + $tb_Height}]
        
        
        set border_Coords   [list   $x_00 $y_00     $x_00 $y_01     $x_01 $y_01     $x_01 $y_00     $x_00 $y_00 ]
        my create draftLine $border_Coords  [list -fill black  -width 0.5  -tags $itemTag]
            
            #
            # --- title block
            #
        set border_Coords   [list   $x_10 $y_10        $x_10 $y_12        $x_01 $y_12        $x_01 $y_10        $x_10 $y_10    ]
        my create draftLine $border_Coords  [list -fill black  -width 0.5  -tags $itemTag]        ;# title block - border

        set line_Coords     [list   $x_10 $y_11        $x_01 $y_11]
        my create draftLine $line_Coords    [list -fill black  -width 0.5  -tags $itemTag]        ;# title block - horizontal line separator

        set line_Coords     [list   $x_11 $y_10        $x_11 $y_12]
        my create draftLine $line_Coords    [list -fill black  -width 0.5  -tags $itemTag]        ;# title block - first left column separator

        set line_Coords     [list   $x_12 $y_11        $x_12 $y_12]
        my create draftLine $line_Coords    [list -fill black  -width 0.5  -tags $itemTag]        ;# title block - second left column separator
            
            #
            # --- create Text
            #
        set scaleFactor        [expr {1.0 / $stageScale}]
            #
        if {[expr {round($scaleFactor)}] == $scaleFactor} {
            set formatScaleFactor        [expr {round($scaleFactor)}]
        } else {
            set formatScaleFactor        [format "%.1f" $scaleFactor ]
        }
        set scaleText "1:$formatScaleFactor"


            # --- create Text:
        set textSize        5

            # --- create Text: DIN Format
        set myPos       [list [expr {$x_10 + 5.0}] [expr {$y_11 + 2.0}]]
        set myText      $stageFormat
        set textFormat  [my create draftText $myPos  [list -text $myText  -size $textSize  -tags $itemTag]]
            
            # --- create Text: Scale
        set myPos       [list [expr {$x_10 + 5.0}] [expr {$y_10 + 3.0}]]
        set myText      $scaleText
        set textScale   [my create draftText $myPos  [list -text $myText  -size $textSize  -tags $itemTag]]
            
            # --- create Text: Project-File
        set myPos       [list [expr {$x_13 - 2.0}] [expr {$y_10 + 3.0}]]
        set myText      $title
        set textProject [my create draftText $myPos  [list -text $myText  -size $textSize  -tags $itemTag  -anchor se]]

            # --- create Text: Date
        set myPos       [list [expr {$x_13 - 2.0}] [expr {$y_11 + 2.5}]]
        set myText      $date
        set textDate    [my create draftText $myPos  [list -text $myText  -size 3.5        -tags $itemTag  -anchor se]]
            #
            
            # --- create Text: label
        if {[file exists $label]} {
                # ... is a SVG-file
                #
            set svgFile     $label    
                #
            set fp          [open $svgFile]
                #
            fconfigure      $fp -encoding utf-8
            set xml         [read $fp]
            close           $fp
                #
            set doc         [dom parse  $xml]
            set svgNode     [$doc documentElement]
                #
            set refx_00     [expr {$x_11 + 2}]
            set refx_01     [expr {$x_12 - 2}]
            set refy_00     [expr {$y_11 + 2}]
            set refy_01     [expr {$y_12 - 2}]
                #
            set myCoords    [list $refx_00 $refy_00 $refx_01 $refy_01]
                #
            set svgLabel    [my create draftLabel $myCoords [list  -svgNode $svgNode  -tags $itemTag  -anchor w]]
                #
        } else {
                # ... is text
            set myPos       [list [expr {$x_11 + 2.0}] [expr {$y_11 + 2.0}]]
            set myText      $label
            set textCompany [my create draftText $myPos  [list  -text $myText  -size $textSize  -tags $itemTag  -anchor sw]]
                #
        }
            
            # --- create Text: Description
        if {$descr ne {}} {    
            set myPos       [list [expr {$x_12 - 2.0}] [expr {$y_11 + 2.0}]]
            set myText      $descr
            set textCompany [my create draftText $myPos  [list  -text $myText  -size 3.5        -tags $itemTag  -anchor se]]
        }
            #
            #
        $canvasObject lower $itemTag all
            #
        return [list pageScale $textScale pageFormat $textFormat]    
            #
    }
    method createDraftLabel {coordList argList} {
            #
        set svgNode {}    
        set anchor  w    
        set angle   0    
        set tags    {}    
            #
        foreach key [dict keys $argList] {
            switch -exact $key {
                -svgNode {
                    set svgNode [dict get $argList -svgNode]
                }    
                -anchor {
                    set anchor  [dict get $argList -anchor]
                }    
                -angle {
                    set angle   [dict get $argList -angle]
                }    
                -tags {
                    set tags    [dict get $argList -tags]
                }
            }
        }
            #
        if {$svgNode eq {}} {
            puts "\n           <E> ... key: -svgNode ... not defined"
            return
        }
            #
            #
            # puts ""
            # puts "---- createDraftLabel ----"
            # puts "        -> \$svgNode $svgNode"    
            # puts "        -> \$anchor  $anchor "    
            # puts "        -> \$angle   $angle  "    
            # puts "        -> \$tags    $tags   "    
            #
        #set myItem      [$canvasObject readSVGNode $svgNode]
        set myItem      [my create svg {0 0} [list -svgNode $svgNode]]
        set bboxItem    [$canvasObject bbox2 $myItem]
        set centerItem  [cad4tcl::_getBBoxInfo $bboxItem center]
        set heightItem  [cad4tcl::_getBBoxInfo $bboxItem height]
            #
        set lineRef     [$canvasObject create draftLine $coordList [list -fill red -width 0.5]]
        set bboxRef     [$canvasObject coords $lineRef]
        set centerRef   [cad4tcl::_getBBoxInfo $bboxRef center]
        set heightRef   [cad4tcl::_getBBoxInfo $bboxRef height]
            #
        set scaleItem   [expr {abs(double($heightRef / $heightItem))}]
            #"
        lassign $centerItem  x y
            # foreach {x y} $centerItem break
        $canvasObject scale $myItem  $x $y $scaleItem $scaleItem
            #
        set bboxItem    [$canvasObject bbox2 $myItem]
        lassign $bboxItem  item_x0 item_y0 item_x1 item_y1
        lassign $bboxRef   ref_x0 ref_y0 ref_x1 ref_y1
            # foreach {item_x0 item_y0 item_x1 item_y1} $bboxItem break
            # foreach {ref_x0 ref_y0 ref_x1 ref_y1} $bboxRef break
            #
        switch -exact $anchor {
            e {
                set posRef  [list $ref_x1 $ref_y0]
                set posItem [list $item_x1 $item_y1]
            }
            c {
                set posRef  [list [expr {0.5 * ($ref_x0 + $ref_x1)}]   [expr {0.5 * ($ref_y0 + $ref_y1)}]]
                set posItem [list [expr {0.5 * ($item_x0 + $item_x1)}] [expr {0.5 * ($item_y0 + $item_y1)}]]
            }
            w -
            default {
                set posRef  [list $ref_x0 $ref_y0]
                set posItem [list $item_x0 $item_y1]
            }
        }
            #
        set vctMove   [cad4tcl::math::subVector $posRef $posItem]
            # vctMove [::math::geometry::- $posRef $posItem]
            # 
            #
        $canvasPath   move $myItem [lindex $vctMove 0] [lindex $vctMove 1]
            #
        $canvasObject delete $lineRef
            #
        foreach tag $tags {
            $canvasObject addtag $tag withtag $myItem
        }
        $canvasObject addtag {__Content__} withtag $myItem
            #
        return $myItem
            #
    }        
    method createDraftLine {coordList argList} {
            #
            # puts "------> createDraftLine $coordList $argList"
            # puts "     -> \$stageScale  $stageScale"
            #
        set scaleFactor [expr {double(1/$stageScale)}]
        set coordList   [cad4tcl::math::scaleCoordList {0 0} $coordList    $scaleFactor]        
            #
        set myItem      [my create line     $coordList  $argList]    
            #
        $canvasObject addtag {__Content__} withtag $myItem
            #
            # puts "   -> $myItem"
            #
        return $myItem    
            #
    }
    method createDraftText {coordList argList} {
            #
            # puts "------> createDraftText $coordList $argList"
            # puts "      -> \$canvasScale    $canvasScale"
            # puts "      -> \$stageScale     $stageScale"
            # puts "      -> \$stageUnitScale $stageUnitScale"
            # puts "    -> \$refScale    $refScale"
            #
        if [dict exist $argList -tags] {
            set value       [dict get $argList -tags]
            lappend value   ___DraftText___
            dict set argList -tags $value
        }    
            #
        set scaleSize   [expr {double(1/$stageScale)}]
            #
        if [dict exist $argList -size] {
            set value       [dict get $argList -size]
            dict set argList -size [expr {$value / $stageScale}]
        } else {
            dict set argList -size [expr {5 / $stageScale}]
        }
            #
        set textSize    [dict get $argList -size]
        set lineWidth   [expr {0.1 * $textSize * $stageScale}]    
            #
        dict set argList -width $lineWidth
        lappend argList -strokelinecap round  -strokelinejoin round
            #
        set coordList   [cad4tcl::math::scaleCoordList {0 0} $coordList    $scaleSize]
            #
        set myItem      [my createVectorText $coordList $argList]
            # 
        return $myItem
            #
    }
        #
    method updateDraftFrame {} {
            #
        puts "   -> updateDraftFrame: $stageScale"    
            #
        my create draftFrame {} [list \
                                    -label $DraftFrame(label) \
                                    -title $DraftFrame(title) \
                                    -date  $DraftFrame(date)  \
                                    -descr $DraftFrame(descr) ]
    }
        #
        #
    method createDraftRaster {argList} {
            #
            # creates a raster on stage defined by $defRaster
            # this raster does not belong to the $stageScale
            # the distance of the lines are in the unit oft the stage without $stageScale
            #
            # defRaster {5 lightgray 10 lightgray}
            # {defRaster {5 gray50 25 gray20}}
            #
            #
            # puts "           -> \$argList:  $argList"
            #
        if {$argList != {}} {
            set arguments [dict get $argList -defRaster]
            if {$arguments != {}} {
                lassign $arguments _minDist _minCol _majDist _majCol
                catch {set DraftRaster(minorDistance)   $_minDist}
                catch {set DraftRaster(minorColor)      $_minCol}
                catch {set DraftRaster(majorDistance)   $_majDist}
                catch {set DraftRaster(majorColor)      $_majCol}
            }
        }
            #
        set minorDistance   $DraftRaster(minorDistance)
        set minorColor      $DraftRaster(minorColor)
        set majorDistance   $DraftRaster(majorDistance)
        set majorColor      $DraftRaster(majorColor)    
            #
            # puts "   createDraftRaster: \$minorDistance $minorDistance"
            # puts "   createDraftRaster: \$minorColor    $minorColor"
            # puts "   createDraftRaster: \$majorDistance $majorDistance"
            # puts "   createDraftRaster: \$majorColor    $majorColor"
            #
        set itemTag         __DraftRaster__
            #
        my delete $itemTag
            #
        foreach {distance color} [list $minorDistance $minorColor $majorDistance $majorColor] {
                #
            set x 0
            set y 0
                #
            while {$x < [expr {$stageWidth - $distance}]} {
                set x           [expr {$x + $distance}]
                set coordList   [list $x 0 $x $stageHeight]
                my create draftLine $coordList  [list  -strokewidth 0.001  -outline $color  -tags $itemTag]
            }
                #
            set x 0
            set y 0
                #
            while {$y < [expr {$stageHeight - $distance}]} {
                set y           [expr {$y + $distance}]
                set coordList   [list 0 $y $stageWidth $y]
                my create draftLine $coordList  [list  -strokewidth 0.001  -outline $color  -tags $itemTag]
            }             
                #
        }
            #
        $canvasObject lower $itemTag all
            #
        return
            #
    }
        #
    method createSVG {coordList argList {style {cad}}} {
            #
            # - -svgNode $svgNode -angle $angle -tags $customTag
            # puts "  -> createSVG "
            # puts "           \$coordList $coordList"
            # puts "           \$argList $argList"
            # puts "           \$style $style"
            #
        set canvPos $coordList
            #
        set svgNode {}
        set svgFile {}
        set angle   0
        set tags    {}
        foreach key [dict keys $argList] {
            switch -exact $key {
                -svgNode {
                    set svgNode [dict get $argList -svgNode]
                }
                -svgFile {
                    set svgFile [dict get $argList -svgFile]
                }
                -angle {
                    set angle   [dict get $argList -angle]
                }    
                -tags {
                    set tags    [dict get $argList -tags]
                } 
            }
        }
            #
        if {$svgNode eq {} && $svgFile eq {}} {
            puts "\n           <E> ... key: -svgNode ... not defined"
            puts "\n           <E> ... key: -svgFile ... not defined"
            return
        }
            #
        if {$svgFile != {}} {
                #
            set fp      [open $svgFile]
                #
            fconfigure  $fp -encoding utf-8
            set xml     [read $fp]
            close       $fp
                #
            set doc     [dom parse  $xml]
            set svgNode [$doc documentElement]
                #
        }
            #
            # puts "    -> \$svgNode  $svgNode"    
            # puts "    -> \$canvPos  $canvPos"    
            # puts "    -> \$angle    $angle"    
            # puts "    -> \$tags     $tags"    
            #
            # puts [$svgNode asXML]    
            #
        set svgTag  [$SVG readNode $svgNode $canvPos $angle $tags $style]
            #
            # puts "     -> create \$svgTag $svgTag"
        return $svgTag
            #
    }
        #
        #
    method ConvertPath2Absolute {pathDef scale} {
            #
            # puts "------ FormatPathDef -------------------------------------------"
            # puts "   -> \$pathDef       $pathDef"
            # puts "   -> \$scale         $scale"
            #
            #
        # set pathDef     [regsub -all {[a-zA-Z]|.^$\\]} $pathDef { & }]   ;# exception for e: -2.2737367544323206e-13
        # set pathDef     [string map {, { }}  $pathDef]
        # set pathDef     [string map {- { -}} $pathDef]
            #
            # set pathDef     [join $pathDef " "]
            #
            # puts "   -> \$pathDef       $pathDef"
            #
        set _pathList   {}
        set key         {}
        set value       {}
            #
        foreach element $pathDef {
                #
            if [string is double $element] {
                lappend value $element
            } else {
                lappend _pathList $key $value
                set key $element
                set value {}
            }
                #
        }
            #
            # puts "    002 -> \$_pathList $_pathList"
            #
            #
            #
            #       M x y   Put the pen on the paper at specified coordinate.
            #               Must be the first atom but can appear any time later.
            #               The pen doesn't draw anything when moved to this point.
            #       L x y   Draw a straight line to the given coordinate.
            #       H x     Draw a horizontal line to the given x coordinate.
            #       V y     Draw a vertical line to the given y coordinate.
            #       A rx ry phi largeArc sweep x y
            #               Draw an elliptical arc from the current point to (x, y). 
            #               The points are on an ellipse with x-radius rx and y-radius ry.
            #               The ellipse is rotated by phi degrees. If the arc is less than 
            #               180 degrees, largeArc is zero, else it is one. If the arc is to be
            #               drawn in cw direction, sweep is one, and zero for the ccw
            #               direction.
            #               NB: the start and end points may not coincide else the result
            #               is undefined. If you want to make a circle just do two
            #           180 degree arcs.
            #       Q x1 y1 x y
            #               Draw a qadratic Bezier curve from the current point to (x, y)
            #               using control point (x1, y1).
            #       T x y   Draw a qadratic Bezier curve from the current point to (x, y)
            #               The control point will be the reflection of the previous Q atoms
            #               control point. This makes smooth paths.
            #       C x1 y1 x2 y2 x y
            #               Draw a cubic Bezier curve from the current point to (x, y)
            #               using control points (x1, y1) and (x2, y2).
            #       S x2 y2 x y
            #               Draw a cubic Bezier curve from the current point to (x, y), using
            #               (x2, y2) as the control point for this new endpoint. The first
            #               control point will be the reflection of the previous C atoms
            #               ending control point. This makes smooth paths.
            #       Z       Close path by drawing from the current point to the preceeding M 
            #               point.
            #
        set xy_init     {0 0}
        set xy_last     {0 0}
        set __pathDef   {}
        foreach {key value} $_pathList {
                # puts "        ---- $key $value"
            switch -exact -- $key {
                M -
                m {
                    lappend __pathDef [string toupper $key]
                        #
                    lassign $value x y
                    set x [expr {$scale * $x}]
                    set y [expr {$scale * $y}]
                        #
                    if {$key eq [string tolower $key]} {
                        set xy_init [cad4tcl::math::addVector $xy_init [list $x $y]]
                        set xy_last $xy_init
                    } else {
                        set xy_init [list $x $y]
                        set xy_last $xy_init
                    }
                        #
                    lappend __pathDef {*}$xy_last 
                        #
                }
                L -
                l -
                S -
                s {
                    lappend __pathDef [string toupper $key]
                        #
                    lassign $value x y
                    set x [expr {$scale * $x}]
                    set y [expr {$scale * $y}]
                        #
                    if {$key eq [string tolower $key]} {
                        set xy_last [cad4tcl::math::addVector $xy_last [list $x $y]]
                    } else {
                        set xy_last [list $x $y]
                    }
                        #
                    lappend __pathDef {*}$xy_last 
                        #
                }
                H -
                h {
                    lappend __pathDef [string toupper $key]
                        #
                    set x $value
                    set y 0
                    set x [expr {$scale * $x}]
                        #
                    if {$key eq [string tolower $key]} {
                        set xy_last [cad4tcl::math::addVector $xy_last [list $x $y]]
                    } else {
                        set xy_last [list $x $y]
                    }
                        #
                    lappend __pathDef {*}$xy_last  
                        #
                }
                V -
                v {
                    lappend __pathDef [string toupper $key]
                        #
                    set x 0
                    set y $value
                    set y [expr {$scale * $y}]
                        #
                    if {$key eq [string tolower $key]} {
                        set xy_last [cad4tcl::math::addVector $xy_last [list $x $y]]
                    } else {
                        set xy_last [list $x $y]
                    }
                        #
                    lappend __pathDef {*}$xy_last  
                        #
                }
                A - 
                a {
                    lappend __pathDef [string toupper $key]
                        #
                    lassign $value rx ry angle large_arc_flag sweep_flag x2 y2
                    set x2 [expr {$scale * $x2}]
                    set y2 [expr {$scale * $y2}]
                    set rx [expr {$scale * $rx}]
                    set ry [expr {$scale * $ry}]
                        #
                    if {$key eq [string tolower $key]} {
                        set xy_last [cad4tcl::math::addVector $xy_last [list $x2 $y2]]
                    } else {
                        set xy_last [list $x2 $y2]
                    }
                        #
                    lappend __pathDef $rx $ry $angle $large_arc_flag $sweep_flag {*}$xy_last
                        #
                }
                Q -
                q -
                T -
                t -
                C -
                c {
                    puts "    -> $key -> $value" 
                    puts "        .... not implemented"
                }
                Z -
                z {
                    # puts "  -> z: \$xy_init $xy_init"
                    # puts "  -> z: \$xy_last $xy_last"
                    # set xy_last $xy_init
                    lappend __pathDef [string toupper $key]
                }
                default {}
            }
        }
            #
            # set pathDef     [list $__pathDef]            
            # puts "   -> $pathDef       {M 12.771 0.8726849999999999 M 10.00395 0.8726849999999999 A 2.7670500000000002 2.7670500000000002 0 1 1 15.53805 0.8726849999999999 L 15.53805 1.724085 A 2.7670500000000002 2.7670500000000002 0 1 1 10.00395 1.724085 Z M 11.66418 1.724085 A 1.1068200000000001 1.1068200000000001 0 1 1 13.87782 1.724085 L 13.87782 2.575485 A 1.1068200000000001 1.1068200000000001 0 1 1 11.66418 2.575485 Z}"
            #
            # puts "   -> \$pathDef       $pathDef"
            #
        return $__pathDef
            #
    }        
        #
        #
    method deleteDimension {} {
            #
        set dimensionList   [$DimensionFactory delete_Member]
        return $dimensionList
            #
    }
    
        #
        #
    method exportDXF {fileName} {
            #
        return [$DXF exportFile $fileName]
            #
    }    
        #
    method exportSVG {fileName} {
            #
        return [$SVG exportFile $fileName]
            #
    }
        #
        #
    method getPointOnEllipse {r1 r2 phi} {
            #
            # ... https://www.hackerboard.de/science-and-fiction/39279-geometrie-punkt-auf-ellipse.html
            # (x / y) = (a * cos(a) / b * sin(a))
            # -> (x / y) = (r1 * cos(phi) / r2 * sin(phi))
            #
        set phi [cad4tcl::math::rad $phi]
        set x   [expr $r1 * cos($phi)]
        set y   [expr $r2 * sin($phi)]
            #
        return [list $x $y]
            #
    }
        #
    method getPointOnEllipse2 {r1 r2 phi} {
            #
            # ... get the point on ellipse cut by line with angle phi through center
            #
            # ... https://www.hackerboard.de/science-and-fiction/39279-geometrie-punkt-auf-ellipse.html
            #       (x / y) = (a * cos(a) / b * sin(a))
            #        x = r1 * cos(phi) 
            #        y = r2 * sin(phi))
            #
            # ... http://www.chemieonline.de/forum/showthread.php?t=108424
            #        tan(alpha) = (h / b) * tan(phi)
            #
        set phi     [cad4tcl::math::rad $phi]
            #
        set alpha   [expr {atan(($r1/$r2) * tan($phi))}]    
            #
        set x       [expr {$r1 * cos($alpha)}]
        set y       [expr {$r2 * sin($alpha)}]
            #
        return [list $x $y] 
            #
    }
        #
        #
    method __createDraftLabel {coordList argList} {
            #
        set itemID  [next $coordList $argList]
        foreach item [$canvasPath find withtag $itemID] {
            set strokewidth  [lindex [$canvasPath itemconfigure $item -strokewidth] 3]
                # puts "        -> \$strokewidth $strokewidth"
            $canvasPath itemconfigure $item -strokewidth [expr {$stageUnitScale * $strokewidth}]
        }
        return  $itemID
            #
    }  
    method __createDraftLabel___ {coordList argList} {
            #
        set svgNode {}    
        set anchor  w    
        set angle   0    
        set tags    {}    
            #
        foreach key [dict keys $argList] {
            switch -exact $key {
                -svgNode {
                    set svgNode [dict get $argList -svgNode]
                }    
                -anchor {
                    set anchor  [dict get $argList -anchor]
                }    
                -angle {
                    set angle   [dict get $argList -angle]
                }    
                -tags {
                    set tags    [dict get $argList -tags]
                }
            }
        }
            #
        if {$svgNode eq {}} {
            puts "\n           <E> ... key: -svgNode ... not defined"
            return
        }
            #
            #
            # puts ""
            # puts "---- createDraftLabel ----"
            # puts "        -> \$svgNode $svgNode"    
            # puts "        -> \$anchor  $anchor "    
            # puts "        -> \$angle   $angle  "    
            # puts "        -> \$tags    $tags   "    
            #
        #set myItem      [$canvasObject readSVGNode $svgNode]
        set myItem      [my create svg {0 0} [list -svgNode $svgNode]]
        set bboxItem    [$canvasObject bbox2 $myItem]
            puts "------- \$bboxItem $bboxItem"
            # exit
        set centerItem  [cad4tcl::_getBBoxInfo $bboxItem center]
        set heightItem  [cad4tcl::_getBBoxInfo $bboxItem height]
            #
        set lineRef     [$canvasObject create draftLine $coordList [list -fill red -width 0.5]]
        set bboxRef     [$canvasObject coords $lineRef]
        set centerRef   [cad4tcl::_getBBoxInfo $bboxRef center]
        set heightRef   [cad4tcl::_getBBoxInfo $bboxRef height]
            #
        set scaleItem   [expr {abs(double($heightRef / $heightItem))}]
            #"
        lassign $centerItem  x y
            # foreach {x y} $centerItem break
        $canvasObject scale $myItem  $x $y $scaleItem $scaleItem
            #
        set bboxItem    [$canvasObject bbox2 $myItem]
        lassign $bboxItem  item_x0 item_y0 item_x1 item_y1
        lassign $bboxRef   ref_x0 ref_y0 ref_x1 ref_y1
            # foreach {item_x0 item_y0 item_x1 item_y1} $bboxItem break
            # foreach {ref_x0 ref_y0 ref_x1 ref_y1} $bboxRef break
            #
        switch -exact $anchor {
            e {
                set posRef  [list $ref_x1 $ref_y0]
                set posItem [list $item_x1 $item_y1]
            }
            c {
                set posRef  [list [expr {0.5 * ($ref_x0 + $ref_x1)}]   [expr {0.5 * ($ref_y0 + $ref_y1)}]]
                set posItem [list [expr {0.5 * ($item_x0 + $item_x1)}] [expr {0.5 * ($item_y0 + $item_y1)}]]
            }
            w -
            default {
                set posRef  [list $ref_x0 $ref_y0]
                set posItem [list $item_x0 $item_y1]
            }
        }
            #
        set vctMove   [cad4tcl::math::subVector $posRef $posItem]
            # vctMove [::math::geometry::- $posRef $posItem]
            # 
            #
        $canvasPath   move $myItem [lindex $vctMove 0] [lindex $vctMove 1]
            #
        $canvasObject delete $lineRef
            #
        foreach tag $tags {
            $canvasObject addtag $tag withtag $myItem
        }
        $canvasObject addtag {__Content__} withtag $myItem
            #
        return $myItem
            #
    }        
    method __createDraftLine {coordList argList} {
            #
        set itemID  [next $coordList $argList]
        foreach item [$canvasPath find withtag $itemID] {
            set strokewidth  [lindex [$canvasPath itemconfigure $item -strokewidth] 3]
                # puts "        -> \$strokewidth $strokewidth"
            $canvasPath itemconfigure $item -strokewidth [expr {$stageUnitScale * $strokewidth}]
        }
        return  $itemID
            #
    }
    method __createVectorText {coordList argList} {
            #
        set itemID  [next $coordList $argList]
        foreach item [$canvasPath find withtag $itemID] {
            puts "    -> createVectorText: $item"
            set strokewidth  [lindex [$canvasPath itemconfigure $item -strokewidth] 3]
                # puts "        -> \$strokewidth $strokewidth"
            $canvasPath itemconfigure $item -strokewidth [expr {$stageUnitScale * $strokewidth}]  -strokelinecap round  -strokelinejoin round
        }
        return  $itemID
            #
    }
        #
        #
    method __createPath_org {pathDef argList} {
            #
        set w           $canvasPath
            #
        set matrix_Base {1 0 0 1 0 0}
            #
        set scale   [expr {$canvasScale * $stageScale}]    
            #
        if 0 {
            if {[dict exists $argList -matrixList]} {  
                set matrixList  [dict get $argList -matrixList]
                dict unset argList -matrixList
            } else {
                set matrixList  {{1 0 0 1 0 0}}
            }
                #
                # puts "------ 001 -------------------------------------------"
                # puts "   -> \$pathDef       $pathDef"
                # puts "   -> \$argList       $argList"
                # puts "   -> \$matrixList    $matrixList"
                #
            set matrixScale [cad4tcl::math::matrixScale $matrix_Base $scale]
                #
            set matrixList [linsert $matrixList 0 $matrixScale]
                # puts "   -> \$matrixList $matrixList"
                #
            set matrix  [cad4tcl::math::matrixMultiplyList $matrixList]    
                #
        }
            #
            # puts "   -> \$pathDef $pathDef "
            #
        set pathDef [my ConvertPath2Absolute $pathDef $scale]
            #
            # puts "   -> \$pathDef       $pathDef"
            # puts "------ 002 -------------------------------------------"
            # puts ""
            #
        set argList     [my UpdateItemAttributes  path  $argList  f1]
        lappend argList -fillrule evenodd  -strokelinecap round  -strokelinejoin round
            #
        set myItem      [$w create  path  $pathDef {*}$argList]
            #
        return          $myItem
            #
    }
        #
        #
}

