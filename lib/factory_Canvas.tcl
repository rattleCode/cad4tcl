 ########################################################################
 #
 #  Copyright (c) Manfred ROSENBERGER, 2017
 #
 #      package: cadCanvas 	->	classCanvasFactory.tcl
 #
 # ----------------------------------------------------------------------
 #  namespace:  cadCanvas
 # ----------------------------------------------------------------------
 #
 #  2017/11/26
 #      extracted from the rattleCAD-project (Version 3.4.05)
 #          http://rattlecad.sourceforge.net/
 #          https://sourceforge.net/projects/rattlecad/
 #
 #

oo::define cad4tcl::CanvasFactory {
        #
    variable _counter    
    variable _objectList    
        #
    constructor {} { 
            #
        puts "            -> factory CanvasFactory"
            #
        set _counter        0
        set _objectList    {}
            #
        set cad4tcl::canvasType 1
            #
    }
        #
    destructor     { 
        puts "            -> [self] ... destroy CanvasFactory"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... cad4tcl::CanvasFactory $target_method $args  ... unknown"
    }
        #
    method create {parentWidget cv_width cv_height stageFormat stageScale stageBorder args} {
            #
        incr _counter
            #
        set cvName      [format {cadCanvas_%s} $_counter]
            # set cvName cvCAD
            # 
            # puts "   -> $args"
        set arguments   [cad4tcl::_flattenNestedList $args]
            # puts "   -> $arguments"
            # set myObject    [cad4tcl::CADCanvas  new $parentWidget.$cvName $cv_width $cv_height $stageFormat $stageScale $stageBorder $arguments]
            #
            #
        if {$cad4tcl::canvasType == 1} {
            if {[catch {package require tkpath 0.3.3} eID]} {
                set myCanvasType 0
            } else {
                set myCanvasType 1
            }
        } else {
                set myCanvasType 0
        }
            #
            #
            # ... before creating the object
        update idletasks
            #
            #
            # ... for debug purpose only
            #       set myCanvasType 0    
            #
        if $myCanvasType {
                # -- $cad4tcl::canvasType == 1
            set myObject    [cad4tcl::PathCanvas    new $parentWidget.$cvName  $cv_width $cv_height $stageFormat $stageScale $stageBorder [cad4tcl::_flattenNestedList $args]]
                #
            set  Canvas(Path)   [$myObject getCanvas]
            pack $Canvas(Path)  -expand yes  -fill both
                #
                #
        } else {
                # -- $cad4tcl::canvasType == 0
            set myObject    [cad4tcl::Canvas        new $parentWidget.$cvName  $cv_width $cv_height $stageFormat $stageScale $stageBorder [cad4tcl::_flattenNestedList $args]]
                #
            set  Canvas(Path)   [$myObject getCanvas]
            pack $Canvas(Path)  -expand yes  -fill both
                #
        }
            #
            #
        cad4tcl::setNodeAttributeRoot /root/_package_/UnitScale   m   [$myObject configure UnitScale m  ]
        cad4tcl::setNodeAttributeRoot /root/_package_/UnitScale   c   [$myObject configure UnitScale c  ]
        cad4tcl::setNodeAttributeRoot /root/_package_/UnitScale   i   [$myObject configure UnitScale i  ]
        cad4tcl::setNodeAttributeRoot /root/_package_/UnitScale   p   [$myObject configure UnitScale p  ]
        cad4tcl::setNodeAttributeRoot /root/_package_/UnitScale   std [$myObject configure UnitScale std]
            #
        lappend _objectList $myObject
            #
        return $myObject
            #
    }
        #
    method createNew {parentWidget width height {stageFormat {noFormat}} {stageScale 1.0} {stageBorder 10} args} {
            #
        if 1 {
            puts "   \$parentWidget        $parentWidget"
            puts "   \$width               $width       "
            puts "   \$height              $height      "
            puts "   \$stageFormat         $stageFormat "
            puts "   \$stageScale          $stageScale  "
            puts "   \$stageBorder         $stageBorder"
        }
            #
        incr _counter
            #
        set refNameOld      [format {_cvCAD_%03d} $_counter]
        set parentWidget    $parentWidget.cv
            #
            # set myObject [cad4tcl::CADCanvas new $refNameOld $parentWidget $width $height $stageFormat $stageScale $stageBorder]
            #
        if {$cad4tcl::canvasType == 1} {
            if {[catch {package require tkpath 0.3.3} eID]} {
                set myCanvasType 0
                #set myCanvasType 1
            } else {
                set myCanvasType 1
            }
        } else {
                set myCanvasType 0
        }
            #
            #
            # ... before creating the object
        update idletasks
            #
            #
        if $myCanvasType {
                # -- $cad4tcl::canvasType == 1
            set myObject    [cad4tcl::PathCanvas    new $canvasPath  $cv_width $cv_height $stageFormat $stageScale $stageBorder [cad4tcl::_flattenNestedList $args]]
                #
            set  Canvas(Path)   [$myObject getCanvas]
            pack $Canvas(Path)  -expand yes  -fill both
                #
                #
        } else {
                # -- $cad4tcl::canvasType == 0
            set myObject    [cad4tcl::Canvas        new $canvasPath  $cv_width $cv_height $stageFormat $stageScale $stageBorder [cad4tcl::_flattenNestedList $args]]
                #
            set  Canvas(Path)   [$myObject getCanvas]
            pack $Canvas(Path)  -expand yes  -fill both
                #
        }
            #
            #
        lappend _objectList $myObject
            #
            # ... history purpose ... remove in final version
            #my createLog $_counter $myObject 
            #
        return $myObject
            #
    }
        #
    method delete {cvObj} {
            #
        puts "   cad4tcl::CanvasFactory -> delete $cvObj"
            #
        $cvObj destroy
            #
    }
        #
    method getMemberDOM {} {
            #
        set xmlDoc  [dom parse [[cad4tcl::getXMLRoot] asXML]]
        set xmlRoot [$xmlDoc documentElement]
            #
        foreach node [$xmlRoot childNodes] {
            if {[$node nodeName] eq {instance}} {
                $xmlRoot removeChild $node
                $node delete
            }
        }
            #
        foreach cvObject $_objectList {
            set objectDoc   [$cvObject updateReportDOM]
            $xmlRoot appendXML [$objectDoc asXML]
        }
            #
        return $xmlDoc
            #
    }
        #
    method getMemberList {} {
        return $_objectList
    }
        #
        #
        #
        #
    method reportMemberList {} {
        foreach cvObject $_objectList {
            puts "       CanvasFactory:  $cvObject"
        }
        return $_objectList
    }
        #
    method __remove_update_memberList {} {
            # puts "   ----- update_memberList -----"
        foreach cvObject $_objectList {
            if [info exist $cvObject] {
                puts "  -1- $cvObject"
            } else {
                puts "  -0- $cvObject"
            }
        }
        return $_objectList
    }
        #
    method __remove_get_memberXML {} {
            #
        set firstObject [lindex $_objectList 0]
        set domNode     [$firstObject getDomNode]
        set domParent   [$domNode parentNode]
            #
        return $domParent
            #
    }
        #
        #
}
