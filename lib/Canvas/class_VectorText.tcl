 ########################################################################
 #
 #  Copyright (c) Manfred ROSENBERGER, 2017
 #
 #      package: cad4tcl 	->	class_VectorText.tcl
 #
 # ----------------------------------------------------------------------
 #  http://www.magicsplat.com/articles/oo.html
 #  http://stackoverflow.com/questions/24957666/tcloo-variable-scope-with-inheritance-superclass
 # ----------------------------------------------------------------------
 #  namespace:  cad4tcl
 # ----------------------------------------------------------------------
 #
 #  2017/11/26
 #      extracted from the rattleCAD-project (Version 3.4.05)
 #          http://rattlecad.sourceforge.net/
 #          https://sourceforge.net/projects/rattlecad/
 #
 #

oo::define cad4tcl::VectorText_Canvas {
        #
    variable cvObject
    variable ItemInterface
    variable fontRoot
    variable tagId
    variable fontSize
    variable posOrigin
    variable posOrigin_Abs
        #
    variable textItem
    variable bbItem
    variable bbox
    variable bbox_2
        #
    variable anchor   
    variable angle
    variable color
    variable lineWidth
    variable tagList
        #
    variable unitScale_p
    variable unitScale  
    variable stageScale 
    variable canvasScale
        #
        #
    constructor {cvObj ItemIF fontXML tagId text pos size argDict} {
            #
            # puts "            -> cad4tcl::VectorText_Canvas"
            #
            #
        set cvObject        $cvObj
        set ItemInterface   $ItemIF 
        set fontRoot        $fontXML
        set tagId           $tagId
        set posOrigin       $pos
        set fontSize        $size
            #
        set anchor          sw
        set angle           0
        set color           {}
        set lineWidth       {}
        set tagList         {}
            #
        set unitScale_p     [cad4tcl::_getUnitRefScale p]
        set unitScale       [$ItemInterface configure  stageUnitScale]
        set stageScale      [$ItemInterface configure  stageScale]
        set canvasScale     [$ItemInterface configure  canvasScale]
            #
        my ParseParameter   $argDict
            #
        set bbox            {}
            #
        set textItem        [my Create $tagId $text $pos]
            #
        my AlignItem
            #
        if {$angle != 0} {
            my RotateItem
        }               
            #
        my Format
            #
    }
        #
    destructor { 
        puts "            -> [self] ... destroy cad4tcl::VectorText_Canvas"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... cad4tcl VectorText_Canvas $target_method $args  ... unknown"
    }
        #
    method Create {tagId text pos} {    
            #
        set myTag       [format {vctText_%s__%s} $fontSize $tagId]
            #
            # puts "-----------> Create"    
            # puts "          -> \$tagList      $tagList"    
        lappend tagList $myTag  
            # puts "          -> \$tagList      $tagList"    
            #
        set myCanvas    [$cvObject getCanvas]
            #
            #
            # puts "          -> \$fontSize      $fontSize"    
            # puts "          -> \$stageScale    $stageScale "    
            # puts "          -> \$canvasScale   $canvasScale"   
            # puts ""        
        set myScale    [expr {1.0 * $fontSize * $canvasScale * $stageScale}]
            # set myScale    $fontSize
            #
        lassign $pos    pos_x pos_y
            #
        set pos_x   [expr {$pos_x * $canvasScale * $stageScale}]
        set pos_y   [expr {$pos_y * $canvasScale * $stageScale}]
            #
        set posOrigin_Abs   [list $pos_x $pos_y]
            #
        set pos_Next   $pos_x
            #
        set unicodeText [encoding convertfrom utf-8 $text]
            # set text [encoding convertfrom  [encoding system] $text]
            #
            #set text [format {%c} 216]
            #
        foreach char [split $unicodeText ""] {
                #
            scan $char %c ascii
                # puts "   -> $char -> $ascii"
            if {$ascii < 32} {
                set offset 0
            } else {
                if {$ascii > 255} {
                    set charNode [$fontRoot find id {exception}]
                }
                set charNode [$fontRoot find id $ascii]
                    # puts [$charNode asXML]
                set offset [$charNode getAttribute offset]
                set offset [expr {$offset * $myScale}]
                    # puts "    $char -> \$pos_x -> $pos_x"
                    # puts "    \$fontSize: $fontSize <-> $myScale"
                foreach defNode [$charNode childNodes] {
                    set type    [$defNode getAttribute type]
                    set values  [$defNode getAttribute values]
                        # puts "    \$type $type -> $values"
                        # puts "    \$type $type -> $lineType"
                        # puts "---------- \$type $type -> $tagList"
                    set values  [cad4tcl::math::addVectorCoordList [list $pos_Next $pos_y] $values $myScale]
                        # puts "       -> $values"
                    set vctItem [$myCanvas create line $values -tags $tagList]
                        #
                        # $myCanvas addtag __Content__ withtag $vctItem
                        #
                }
                set pos_Next [expr $pos_Next + $offset]
            }
        }
            #
        set append 2/9
        set append 0.1530612244897959
            #
        set pos_Next    [expr {$pos_Next - ($myScale * $append)}]
            #
        set bbox        [list $pos_x $pos_y $pos_Next [expr {$pos_y + $myScale}]]
            #
            # puts "  \$bbox  $bbox"
            # puts "-------------------------------------"
            # set bbItem  [$myCanvas create rectangle $bbox]
            # $myCanvas addtag $myTag withtag $bbItem
            #
        $myCanvas scale $myTag 0 0 1 -1
            #
        return $myTag
            #
    }
        #
        #
    method ParseParameter {argDict} {
            #
            # puts "   ParseParameter $argDict"
            #
        set anchor      sw
        set angle       0
        set color       green
        set lineWidth   1.0
        set tagList     {}
            #
            # puts "        -> \$argDict      $argDict"
            #
        foreach key {-anchor -angle -outline -stroke -fill -width -tags} {
            if [dict exist $argDict $key] {
                switch -exact $key {
                    -anchor { 
                        set anchor      [dict get $argDict $key]
                    }
                    -angle {
                        set angle       [dict get $argDict $key]
                    }
                    -outline -
                    -stroke -
                    -fill { 
                        set color       [dict get $argDict $key]
                    }
                    -width { 
                        set lineWidth   [dict get $argDict $key]
                    }
                    -tags { 
                        set tagList     [dict get $argDict $key]
                    }
                    default { 
                        set value       [dict get $argDict $key]
                        # dict set argDict $key   $value
                    }
                }
            }
        }
            #
            # puts "        -> \$anchor       $anchor"
            # puts "        -> \$angle        $angle"
            # puts "        -> \$color        $color"
            # puts "        -> \$lineWidth    $lineWidth"
            # puts "        -> \$tagList      $tagList"
            # puts "        -> \$argDict      $argDict"
            #
        return $argDict
            #
    }
        #
    method AlignItem {{_anchor {}}} {
            #
            # puts "\n"
            # puts "    ... alignItem: [self]"
            # puts "          default:$anchor"
            # puts "          current: $anchor"
        if {$_anchor eq {}} {
            set _anchor $anchor
            # puts "          $_anchor"
        }
        if {$_anchor eq {}} {
            return
        }
        set vctAlign    [my GetAlignOffset $_anchor]
            # puts "          $vctAlign"
        lassign $vctAlign x y
        [$cvObject getCanvas] move $textItem $x $y
        return  
            #
    }
        #
    method RotateItem {} {
            # canvas object rotation function written by Keith Vetter
            # 	 see http://wiki.tcl.tk/8595
            # angle direction reversed -ger
            #
        set myCanvas [$cvObject getCanvas]
            #
        lassign     $posOrigin      c  d
        lassign     $posOrigin_Abs  pos_x pos_y
            #
        set radius   0.7
        set refObject [$myCanvas create oval [expr {$pos_x - $radius}] [expr {$pos_y - $radius}] [expr {$pos_x + $radius}] [expr {$pos_y + $radius}]  -fill blue]
        $myCanvas scale $refObject 0 0 1 -1
            #
        set bbox     [$myCanvas bbox $refObject] 
        lassign $bbox a b c d
            # puts "   -> $bbox" 
        set Ox [expr {0.5 * ($a + $c)}]
        set Oy [expr {0.5 * ($b + $d)}]
            # puts "   --> $Ox $Oy"            
            #
    	set angle [expr {$angle * atan(1) * 4 / -180.0}] ;# Radians
    	foreach id [$myCanvas find withtag $textItem] {     ;# Do each component separately
                #
    		set xy {}
    		foreach {x y} [$myCanvas coords $id] {
                    # rotates vector (Ox,Oy)->(x,y) by angle clockwise
                set x [expr {$x - $Ox}]             ;# Shift to origin
                set y [expr {$y - $Oy}]
                    #
                set xx [expr {$x * cos($angle) - $y * sin($angle)}] ;# Rotate
                set yy [expr {$x * sin($angle) + $y * cos($angle)}]
                    #
                set xx [expr {$xx + $Ox}]           ;# Shift back
                set yy [expr {$yy + $Oy}]
                    #
                lappend xy $xx $yy
                    #
    		}
    		$myCanvas coords $id $xy
    	}
        $myCanvas delete $refObject
    }
        
        #
    method GetBBox {} {
        return $bbox
    }    
        #
    method GetAlignOffset {{refName {}}} {
            # puts "   ---- getBBox --- $refName"
        lassign $bbox x0 y0 x2 y2
            # puts "    -> \$bbox $bbox"
        set x1  [expr {0.5 * ($x0+$x2)}]
        set y1  [expr {0.5 * ($y0+$y2)}]
        set w   [expr {$x2-$x0}]
        set h   [expr {$y2-$y0}]
        set w2  [expr {0.5 * $w}]
        set h2  [expr {0.5 * $h}]
        switch -exact $refName {
    		"n"		{ return [list [expr {-0.5 * $w}]   [expr {1.0 * $h}]] }
    		"ne"	{ return [list [expr {-1.0 * $w}]   [expr {1.0 * $h}]] }
    		"e"		{ return [list [expr {-1.0 * $w}]   [expr {0.5 * $h}]] }
    		"se"	{ return [list [expr {-1.0 * $w}]   0] }
    		"s"		{ return [list [expr {-0.5 * $w}]   0] }
    		"sw"	{ return [list  0                   0] }
    		"w"		{ return [list [expr {-1.0 * $w}]   [expr {0.5 * $h}]] }
    		"nw"	{ return [list  0                   [expr {1.0 * $h}]] }
    		"c"		{ return [list [expr {-0.5 * $w}]   [expr {0.5 * $h}]] }
            default {
                return {0 0}
            }
        }
    }    
        #
    method Format {} {
            #
            # puts " --- Format --- "
            # puts "     ---> $textItem"
            # puts "       -> \$color $color"
            # puts "       -> \$fontSize $fontSize"
            # puts "       -> \$lineWidth $lineWidth"       
            #
        $cvObject itemconfigure $textItem  -fill $color  -width $lineWidth
            #
    }
        #
    method getTag {} {
        return $textItem
    } 
        #
}