 ########################################################################
 #
 #  Copyright (c) Manfred ROSENBERGER, 2017
 #
 #      package: cadCanvas 	->	super_Canvas.tcl
 #
 # ----------------------------------------------------------------------
 #  namespace:  cadCanvas
 # ----------------------------------------------------------------------
 #
 #  2017/11/26
 #      extracted from the rattleCAD-project (Version 3.4.05)
 #          http://rattlecad.sourceforge.net/
 #          https://sourceforge.net/projects/rattlecad/
 #
 #

oo::define cad4tcl::Canvas__Super {
        #
        #
    variable packageHomeDir         ;# base dir of package
        #
    variable DimensionFactory
    variable ItemInterface
        #
    variable Canvas
    variable Stage
    variable Style
    variable UnitScale
        #
    puts "   -> \$cad4tcl::canvasType $cad4tcl::canvasType   ... Canvas__Super"
        #
        # ----------------------------------------------------
        #
    constructor {parentWidget cv_width cv_height stageFormat stageScale stageBorder} {
            #
        puts "            -> superclass Canvas__Super"
            #
            # <Canvas   path="" iborder="0" scale=""/>
            # <Stage    format="" unit="" scale="" width="" height="" title=""/>
            # <Style    linewidth="0.35" linecolour="black" fontstyle="vector" fontsize="3.5" fontdist="1" font="Arial" fontcolour="black" precision="2" DefaultPrecision="2"/>
            #
        array set Canvas {
            Path            {}
            Type            {}
            InnerBorder     {}
            Scale           {}
            Cursor          {}
        }  
        array set Stage {
            Format          {}
            Unit            {}
            UnitScale       {}
            Scale           {}
            Width           {}
            Height          {}
            Title           {}
        }  
        array set Style {
            Linewidth           {}
            Linecolour          {}
            Fontstyle           {}
            Fontsize            {}
            Fontdist            {}
            Font                {}
            Fontcolour          {}
            Precision           {}
            DefaultPrecision    {}
            DivTitleforeground  {}
            DivTitlebackground  {}
            StageBrightColor    white
            StageDarkColor      #EDEBEA
        }
        array set UnitScale {
            m       {}
            c       {}
            i       {}
            p       {}
            std     {}
        }
            #
    }
        #
    destructor { 
        puts "            -> [self] ... destroy cad4tcl::Canvas__Super"
        pack forget $Canvas(Path)
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... cad4tcl::Canvas__Super::$target_method $args  ... unknown"
    }
        #
    method CreateStage {{type sheet}} {
            #
        return                 
            #
    }
        #
    method CreateWindow {coordList argList} {
        set w [my getCanvas]
        return [eval $w create window $coordList $argList]
    }
        #
    method reportSettings {} {
            #
        puts ""
        puts "  -- reportSettings: [self] ----------"
            #
        puts "\n  -- Canvas --"
        parray Canvas
            #
        puts "\n  -- Stage --"
        parray Stage
            #
        puts "\n  -- Style --"
        parray Style
            #
        puts "\n  -- UnitScale --"
        parray UnitScale
            #
    }
        #
    method getPackageHomeDir {} {
        return  $packageHomeDir
    }   
        #
    method getCanvas {} {
        return  $Canvas(Path)
    }
        #
    method getDimensionFactory {} {
        return $DimensionFactory
    }
        #
    method getItemInterface {} {
        return $ItemInterface
    }
        #
    method getOrigin {{tagID __Stage__}} {
            #
        set bbox [$Canvas(Path) coords $tagID] 
        lassign $bbox  x1 y1 x2 y2
            # foreach {x1 y1 x2 y2} $bbox break
        set bottomLeft [list $x1 $y2]
            #
        return $bottomLeft
            #
    }        
        #
    method getType {} {
            #
        return $Canvas(Type)
            #
    }        
        #
        #
    method updateStageAppearance {args} {
            #
        puts "\n"
        puts "    ... updateStageAppearance $args"
        puts "                \$cad4tcl::darkMode: $cad4tcl::darkMode"
        puts "\n"
            #
        set w           $Canvas(Path)
            # 
        set itemStage   [my find withtag {__Stage__}]
            #
            # puts "\n  \$itemStage $itemStage \n"
            #
        if $cad4tcl::darkMode {
            $w itemconfigure    $itemStage   -fill $Style(StageDarkColor)    -outline $Style(StageDarkColor)
        } else {
            $w itemconfigure    $itemStage   -fill $Style(StageBrightColor)  -outline $Style(StageBrightColor)
        }
            #
            #   set itemShadow   [my find withtag {__StageShadow__}]
            #               $itemStage   -fill #ccc8c5  -outline #ccc8c5
            #               $itemStage   -fill gray90   -outline gray90
            #               $itemStage   -fill #efebe7  -outline #f4f1ed
            #
    }
        #
        #
    method addtag {args} {
        set w [my getCanvas]
        return [eval $w addtag $args]
    }
        #
    method bbox {args} {
        set w [my getCanvas]
            # puts "      -> \$w      $w"
            # puts "      -> \$args   $args"
            # puts "      -> [$w find all]"
            # puts "      -> [$w find withtag $args]"
        return [eval $w bbox $args]
    }
        #
    method bbox2 {args} {
            # set precision $::tcl_platform(pointerSize)
        set x0   Inf    ;# set x0  1e+8
        set y0   Inf    ;# set y0  1e+8
        set x1  -Inf    ;# set x1  -1e+8
        set y1  -Inf    ;# set y1  -1e+8
            #
        foreach tag [my find withtag $args] {
                # puts "     -> $tag"
                # puts "          [my coords $tag]"
            set coordslist  {}
                #
            foreach element [my coords $tag] {
                if [string is double $element] {
                        # ... <path d="M 213.5277291002451 945.8517030900574 L 213.69867887051194 945.8517030900574 L 213.69">
                    lappend coordslist $element
                }
            }
            foreach {_x0 _y0 _x1 _y1} $coordslist {
                if {$_x0 < $x0} {set x0 $_x0}
                if {$_y0 < $y0} {set y0 $_y0}
                if {$_x1 > $x1} {set x1 $_x1}
                if {$_y1 > $y1} {set y1 $_y1}
            }
        }
            # puts "---> bbox"
            # puts "   -> x0 $x0"
            # puts "   -> y0 $y0"
            # puts "   -> x1 $x1"
            # puts "   -> y1 $y1"
            #
        return [list $x0 $y0 $x1 $y1]
            #
    }
        #
    method bind {args} {
        set w  [my getCanvas]
        return [eval $w bind $args]
    }
        #
    method coords {args} {
        set w  [my getCanvas]
        return [eval $w coords $args]
    }
        #
    method delete {args} {
        set w [my getCanvas]
        return [eval $w delete $args]
    }
        #
    method dtag {args} {
        set w [my getCanvas]
        return [eval $w dtag $args]
    }
        #
    method find {args} {
        set w [my getCanvas]
        return [eval $w find $args]
    }
        #
    method focus {tagID} {
        set w [my getCanvas]
        return [$w focus $tagID]
    }
        #
    method gettags {tagID} {
        set w [my getCanvas]
        return [$w gettags $tagID]
    }
        #
    method itemcget {args} {
        set w [my getCanvas]
        return [eval $w itemcget $args]
    }
        #
    method lower {args} {
            #
        set w [my getCanvas]
            #
        set referenceID [lindex $args end]
        set idList      [lrange $args 0 end-1]
            #
        if {$referenceID eq "all"} {
            $w lower   $idList  all
            $w raise   $idList  {__Stage__}
        }  else {
            $w lower $idList $referenceID
        }
            #
        return {}    
            #
    }
        #
    method move {tagID xAmount yAmount} {
    }
        #
    method bareMove {tagID xAmount yAmount} {
        my Move $tagID $xAmount $yAmount
    }
        #
    method raise {args} {
            #
        set w [my getCanvas]
            #
        set referenceID [lindex $args end]
        set idList      [lrange $args 0 end-1]
            #
        if {$referenceID eq "all"} {
            $w lower   $idList  all
            $w lower   $idList  {__Stage__}
            if {[my find withtag {__StageShadow__}] ne {}} {
                    # __StageShadow__ ... does not exist in passive Canvas
                $w lower   $idList  {__StageShadow__}
            }
        } else {
            $w raise $idList $referenceID
        }
            #
        return {}    
            #        
    }        
        #
    method scale {args} {
    }
        #
    method type {tagID} {
        set w [my getCanvas]
        return [$w type $tagID]
    }
        #
    method classInfo {} {
        set w [my getCanvas]
        return [winfo class $w]
    }
        #
    
}
    #
    #
