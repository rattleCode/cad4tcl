 ########################################################################
 #
 #  Copyright (c) Manfred ROSENBERGER, 2017
 #
 #      package: cadCanvas 	->	classDimension.tcl
 #
 # ----------------------------------------------------------------------
 #  namespace:  cadCanvas
 # ----------------------------------------------------------------------
 #
 #  2017/11/26
 #      extracted from the rattleCAD-project (Version 3.4.05)
 #          http://rattlecad.sourceforge.net/
 #          https://sourceforge.net/projects/rattlecad/
 #
 #

oo::define cad4tcl::AngleDimension {
        #
    superclass cad4tcl::Dimension__Super
        #
        #
    variable ItemInterface
    variable cvObject
        #
    variable _parentCanvas              ;# the canvas, the dimension is created
    variable _canvasRegistryNode        ;# the node in the cadCanvas registry
    variable _dimensionTag              ;# name of taglist containing all canvas elements of this dimension
    variable _textTag                   ;# contains the tag of the dimensiontext
    variable _fontSize                  ;# fontsize of dimension text
    variable _lineWidth                 ;# lione width of dimension
    variable _editTag                   ;# contains the tag of the sensitive dimension area
        #
        #
    constructor {cvObj itemIF p_Coords dimDistance {textOffset 0} {colour black}} {
            #
        # puts "            -> class AngleDimension"
            #
        set cvObject       $cvObj
            #
        set ItemInterface  $itemIF
            #
        my setCanvasSettings
            #
        set _dimensionTag   _empty_    
        set _editTag        _empty_
            #
        set _dimensionTag   [my createObject  $p_Coords  $dimDistance  $textOffset  $colour]
            # puts "     -> \$_dimensionTag $_dimensionTag"    
            #
        set w $_parentCanvas
            #
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... cad4tcl::AngleDimension $target_method $args  ... unknown"
    }
        #
    method createObject {p_Coords  dimRadius {textOffset 0} {colour black}} {
            #
        set w           [$cvObject configure    Canvas  Path]
        set fontSize    [$cvObject configure    Style   Fontsize]
        set stageScale  [$cvObject configure    Stage   Scale]
            #
            # ---------------------------------------------
        set tagListName [format "dimGroup_%s" [llength [$w find withtag all]] ]
            #
            #
            # puts "      \$w             $w"
            # puts "      \$fontSize      $fontSize"
            # puts "      \$stageScale    $stageScale"
            #
        set pc  [list [lindex $p_Coords 0] [lindex $p_Coords 1]]
        set p1  [list [lindex $p_Coords 2] [lindex $p_Coords 3]]
        set p2  [list [lindex $p_Coords 4] [lindex $p_Coords 5]]
            #
        set checkLength [ expr {(1 + 2 * $fontSize) / $stageScale}]
            #
            # -------------------------------
            # ceck $w
        if {$w == {}} {
            error "cad4tcl::AngleDimension -> Error:  could not get \$w" 
        }
            #
            # -------------------------------
            # correct in case of {} parameters ... execution through cad4tcl::dimension
        if {$textOffset == {}} {set textOffset  0}
        if {$colour     == {}} {set colour      black}
            #
            # -------------------------------
            # reset item container
        set tagList     {}

            # -------------------------------
            # correct direction
        set arcRadius $dimRadius
        
            # -------------------------------
            # get direction
        set angle_p1    [ cad4tcl::math::dirAngle $pc $p1 ]
        set angle_p2    [ cad4tcl::math::dirAngle $pc $p2 ]

        # -------------------------------
            # negativ dimRadius exception
        if { $dimRadius < 0 } {
            set angle_x     $angle_p1
            set angle_p1    $angle_p2
            set angle_p2    [expr {360 + $angle_x}]
        }
        set gapAngle        [expr {$angle_p2 - $angle_p1}]
        
        # -------------------------------
            # angle in between 3 points
        set angle_p1        [my noramlizeAngle $angle_p1 ]
        set absAngle        [my noramlizeAngle $gapAngle ]     
        
                # set angle_p1    [ eval format "%0.3f" $angle_p1 ]
                # set angle_p2    [ eval format "%0.3f" $angle_p2 ]
        
        if { [eval format "%0.3f" $angle_p1] == [eval format "%0.3f" $angle_p2] } { return }
        
        set angle_p2        [expr {$angle_p1 + $absAngle}]
                # set angle_p2    [ eval format "%0.3f" $angle_p2 ]
        
                # puts "    ... angle_p1:       $angle_p1"
                # puts "    ... angle_p2:       $angle_p2"
                # puts "    ... gapAngle:       $gapAngle"

            # -------------------------------
            # dimension help lines parameters
        set angle_dim_p1    [expr {$angle_p1  +  90}]
        set angle_dim_p2    [expr {$angle_p2  -  90}]
        
        set p_hl_p1         [cad4tcl::math::rotateLine  $p1  [expr {2 / $stageScale}]  $angle_p1]
        set p_hl_p2         [cad4tcl::math::rotateLine  $p2  [expr {2 / $stageScale}]  $angle_p2]
        if { $dimRadius > 0 } {
            set p_hl_dim_p1 [cad4tcl::math::rotateLine  $pc  $arcRadius  $angle_p1]
            set p_hl_dim_p2 [cad4tcl::math::rotateLine  $pc  $arcRadius  $angle_p2]
        } else {
            set p_hl_dim_p1 [cad4tcl::math::rotateLine  $pc  $arcRadius  [expr {180 + $angle_p1}]]
            set p_hl_dim_p2 [cad4tcl::math::rotateLine  $pc  $arcRadius  [expr {180 + $angle_p2}]]
        }

            # -------------------------------
            # dimension help lines
        set check_ln_p1     [cad4tcl::math::length $pc $p1]
        set check_ln_p2     [cad4tcl::math::length $pc $p2]
        if {$check_ln_p1 < $arcRadius} {
            set myLine [my createLine_2     $p_hl_p1  $p_hl_dim_p1  $colour ]
            lappend tagList $myLine
                # $w addtag $tagListName withtag $myLine
        }
        if {$check_ln_p2 < $arcRadius } {
            set myLine [my createLine_2     $p_hl_p2  $p_hl_dim_p2  $colour ]
            lappend tagList $myLine
                # $w addtag $tagListName withtag $myLine 
        }

            # -------------------------------
            # dimension arc ends
        set asinParameter   [ expr {1.0 * $checkLength/(2*$arcRadius)}]
        set checkAngle      180
                # puts "   -> asinParameter  $asinParameter"
        if { [expr abs($asinParameter)] < 1 } {
            set checkAngle  [ expr {2 * asin($asinParameter) * 180 / $cad4tcl::math::CONST_PI}]
        }
        
        if { $absAngle > $checkAngle } {
            set myEnd [my createLineEnd_2   $p_hl_dim_p1  $angle_dim_p1  $colour ]
            lappend tagList $myEnd
                # $w addtag $tagListName withtag $myEnd
            set myEnd [my createLineEnd_2   $p_hl_dim_p2  $angle_dim_p2  $colour ]
            lappend tagList $myEnd 
                # $w addtag $tagListName withtag $myEnd 
        } else {
            set myEnd [my createLineEnd_2   $p_hl_dim_p1  $angle_dim_p1  $colour  {outside} ]
            lappend tagList $myEnd 
                # $w addtag $tagListName withtag $myEnd
            set myEnd [my createLineEnd_2   $p_hl_dim_p2  $angle_dim_p2  $colour  {outside} ]
            lappend tagList $myEnd 
                # $w addtag $tagListName withtag $myEnd 
        }
        
            # -------------------------------
            # arc extension on offset 
        if { $textOffset != 0 } {
            set extAngle    0
            set offsetAngle 0
            # --- compute extension from fontSize
            set asinParameter   [expr {1.0 * $checkLength/$arcRadius}]
            if { $asinParameter < 1 } {
                set asinParameter   [expr {1.5 * $fontSize/$arcRadius}]
                set extAngle    [expr {asin($asinParameter) * 180 / $cad4tcl::math::CONST_PI}]
            } 
                # puts "    ... gapAngle:       $gapAngle"
            
            set offsetAngle     [expr {$angle_p1 + $absAngle/2 - $textOffset}]
            set offsetAngle_1   [expr {$angle_p1 + $absAngle/2 - $textOffset - $extAngle}]
            set offsetAngle_2   [expr {$angle_p2 - $absAngle/2 - $textOffset + $extAngle}]
                # puts "    ... textOffset:     $textOffset    "
                # puts "    ... angle_p1:       $angle_p1   $offsetAngle_1 "
                # puts "    ... angle_p2:       $angle_p2   $offsetAngle_2 "
            if { $textOffset > 0 } {
                if {$offsetAngle_1 < $angle_p1} {set angle_p1 $offsetAngle_1}
            } else {
                if {$offsetAngle_2 > $angle_p2} {set angle_p2 $offsetAngle_2}
            }
        }
        
            # -------------------------------
            # dimension arc and text position angle
        set arcAngle        [expr {$angle_p2 - $angle_p1}]
                # puts "    ... arcAngle:       $arcAngle   "
                # puts "    ... angle_p1:       $angle_p1   "
                # puts "    ... angle_p2:       $angle_p2   "
        # set checkValue        [expr $angle_p2 - $angle_p1 ]
                # puts "    ... checkValue:     $checkValue   "

        if {[expr {$angle_p2 - $angle_p1}] > 360} {
                set myArc [my createArc_2   $pc  $arcRadius  0 359.999  $colour]
                lappend tagList $myArc
                $w addtag $tagListName withtag $myArc
        } else {
            if {[expr {$angle_p1 + $arcAngle}] < 360} {
                set myArc [my createArc_2   $pc  $arcRadius  $angle_p1  $arcAngle  $colour ]
                lappend tagList $myArc
                        # $w addtag $tagListName withtag $myArc 
            } else {
                set myArc [my createArc_2   $pc  $arcRadius  $angle_p1  $arcAngle  $colour ]
                lappend tagList $myArc
                        # $w addtag $tagListName withtag $myArc 
                set myArc [my createArc_2   $pc  $arcRadius  0         [expr {$angle_p1 + $arcAngle - 360}]  $colour ]
                lappend tagList $myArc
                        # $w addtag $tagListName withtag $myArc 
            }
        }
        
            # -------------------------------
            # no text offset if distance less than 2+fontSize -> $checkLength
        if { [expr {abs($arcRadius)}] < $checkLength } { set textOffset 0 }
        
            # -------------------------------
            # dimension arc and text position angle
            # puts "    ... gapAngle:       $gapAngle"
        set textPosAngle        [expr {$angle_p1 + 0.5*$absAngle - $textOffset}]
        if { $dimRadius < 0 } {
            set textPosAngle    [expr {180 + $textPosAngle}]
        }
            # -------------------------------
            # text position
        set textPosition    [cad4tcl::math::rotateLine  $pc  $arcRadius  $textPosAngle]

            #  ---------------------------------------------
            # create text
            # set myItem [my createPoint_2 $textPosition 2 red]
            # lappend tagList $myItem    
            
        set myText [my createText_2   $absAngle  {°}  $textPosition  $textPosAngle  $colour ]
            # $w itemconfigure $myText -tags [list $myText __DimensionText__]
            lappend tagList $myText
                # puts "[$w itemconfigure $myText]"
                # $w addtag $tagListName withtag $myText
            
            #  ---------------------------------------------
            #  sum up and return
                # $w addtag $tagListName withtag $myBase
                # $w addtag $tagListName withtag $myHelp
                # $w addtag $tagListName withtag $myPos
                # $w move {myArc} 30 0

            #
            #
            # -------------------------------
            # clenaup itemTag container for sure
        $w dtag  $tagListName all
            # fill itemTag
        foreach item $tagList {
            # puts "---> <D> -- addtag -->  $item"
            foreach item2 [$w find withtag $item] {
                # puts "---> <D> -- addtag2 -->  $item2  -> [$cvObject itemcget $item2 -width]"
            }
                # $w delete $item
            $w addtag $tagListName withtag $item
        }
        foreach item [$w gettags $tagListName] {
            # puts "---> <D> -- check -- $tagListName -->  $item"
        }
            # -------------------------------
            # 
        return $tagListName         
            #
    }
        #
    method get_dimensionType {} {
        return {angle}
    }
        #
}

