 ########################################################################
 #
 #  Copyright (c) Manfred ROSENBERGER, 2017
 #
 #      package: cadCanvas 	->	classDimension.tcl
 #
 # ----------------------------------------------------------------------
 #  namespace:  cadCanvas
 # ----------------------------------------------------------------------
 #
 #  2017/11/26
 #      extracted from the rattleCAD-project (Version 3.4.05)
 #          http://rattlecad.sourceforge.net/
 #          https://sourceforge.net/projects/rattlecad/
 #
 #

oo::define cad4tcl::LengthDimension {
        #
    superclass cad4tcl::Dimension__Super
        #
        #
    variable ItemInterface
    variable cvObject
        #
    variable _parentCanvas              ;# the canvas, the dimension is created
    variable _canvasRegistryNode        ;# the node in the cadCanvas registry
    variable _dimensionTag              ;# name of taglist containing all canvas elements of this dimension
    variable _textTag                   ;# contains the tag of the dimensiontext
    variable _fontSize                  ;# fontsize of dimension text
    variable _lineWidth                 ;# lione width of dimension
    variable _editTag                   ;# contains the tag of the sensitive dimension area
        #
        #
    constructor {cvObj  itemIF  p_Coords  dimOrientation  dimDistance  {textOffset 0}  {colour black}} {
            #
        # puts "            -> class AngleDimension"
            #
        set cvObject       $cvObj
            #
        set ItemInterface  $itemIF
            #
        my setCanvasSettings
            #
        set _dimensionTag   _empty_    
        set _editTag        _empty_
            #
            # set returnValues    [my createObject  $p_Coords  $dimDistance  $textOffset  $colour]    
        set _dimensionTag   [my createObject    $p_Coords  $dimOrientation  $dimDistance  $textOffset  $colour]    
            # puts "     -> \$_dimensionTag $_dimensionTag"    
            #
        set w $_parentCanvas
            #
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... cad4tcl::LengthDimension $target_method $args  ... unknown"
    }
        #
    method createObject {p_Coords  dimOrientation dimDistance {textOffset 0} {colour black}} {
            #
        set w           [$cvObject configure    Canvas  Path]
        set fontSize    [$cvObject configure    Style   Fontsize]
        set stageScale  [$cvObject configure    Stage   Scale]
            #
            # ---------------------------------------------
        set tagListName [format "dimGroup_%s" [llength [$w find withtag all]] ]
            #
            #
            # puts "      \$w             $w"
            # puts "      \$fontSize      $fontSize"
            # puts "      \$stageScale    $stageScale"
            #    
        set checkLength [expr {(1 + 2 * $fontSize) / $stageScale}]
            #
            # -------------------------------
            # ceck $w
        if { $w == {} } { 
            error "cad4tcl::LengthDimension -> Error:  could not get \$w" 
        }
        
            # -------------------------------
            # correct in case of {} parameters ... execution through cad4tcl::dimension
        if {$textOffset == {}} { set textOffset 0}
        if {$colour     == {}} { set colour     black}
            #
        set textOffset [expr {$textOffset / $stageScale}]
            #
            # -------------------------------
            # reset item container
        set tagList     {}
            #
            # -------------------------------
            # set type exceptions
        switch $dimOrientation {
            aligned {
                set p1          [list [lindex $p_Coords 0] [lindex $p_Coords 1]]
                set p2          [list [lindex $p_Coords 2] [lindex $p_Coords 3]]
                set p_start     $p1
                set p_end       $p2
            }
            horizontal {
                set p1          [list [lindex $p_Coords 0] [lindex $p_Coords 1]]
                set p2          [list [lindex $p_Coords 2] [lindex $p_Coords 3]]
                set p_start     $p1
                set p_end       [list [lindex $p2 0] [lindex $p1 1]]
            }
            vertical {
                set p1          [list [lindex $p_Coords 0] [lindex $p_Coords 1]]
                set p2          [list [lindex $p_Coords 2] [lindex $p_Coords 3]]
                set p_start     $p1
                set p_end       [list [lindex $p1 0] [lindex $p2 1] ]
            }
            perpendicular { # dimension line through p0 & p1 perpendicular through p2 , p0 as direction ref
                set p0          [list [lindex $p_Coords 0] [lindex $p_Coords 1]]
                set p1          [list [lindex $p_Coords 2] [lindex $p_Coords 3]]
                set p2          [list [lindex $p_Coords 4] [lindex $p_Coords 5]]
                set p_start     $p1
                set p_isect     [cad4tcl::math::intersectPerp  $p0 $p1 $p2]
                set p1_aln_vct  [cad4tcl::math::subVector      $p1 $p_isect]
                set p_end       [cad4tcl::math::addVector      $p2 $p1_aln_vct]
                set perpAngle   [cad4tcl::math::dirAngle_Coincidence $p_start $p_end  0.0001 $p0] 
                set dimPerp     [cad4tcl::math::length         $p_start $p_end]
                set dimDir      [cad4tcl::math::VRotate        $p1_aln_vct 90]
            }                  
        }
            #
            # -------------------------------
            # set helpline parameters
        set dimDistance     [expr {$dimDistance / $stageScale}]
        set textAngle       [cad4tcl::math::dirAngle   $p_start    $p_end]
        if {$dimOrientation == {perpendicular}} {
            set textAngle   $perpAngle
        }
        set textPosAngle    [expr {$textAngle - 90}]
        set dimLength       [cad4tcl::math::length     $p_start    $p_end]
        set p_hl_dim_p1     [cad4tcl::math::rotateLine $p_start    $dimDistance    $textPosAngle ]
        set p_hl_dim_p2     [cad4tcl::math::rotateLine $p_end      $dimDistance    $textPosAngle ]
            #
            # -------------------------------
            # define start position of helplines, in case of p_hl_dim_p? orientation
        set p_hl_p1         [cad4tcl::math::addVector  $p_start    [cad4tcl::math::unifyVector $p_start   $p_hl_dim_p1 ] [expr {2 / $stageScale}]]
        set p_hl_p2         [cad4tcl::math::addVector  $p2         [cad4tcl::math::unifyVector $p2        $p_hl_dim_p2 ] [expr {2 / $stageScale}]]
        
            # -------------------------------
            # dimension help lines - createLine
            # puts "\n--helpLines---------------"
        set myLine [my createLine_2         $p_hl_p1  $p_hl_dim_p1  $colour]
        lappend tagList $myLine
            # $w addtag $tagListName withtag $myLine
        set myLine [my createLine_2         $p_hl_p2  $p_hl_dim_p2  $colour]
        lappend tagList $myLine
            # $w addtag $tagListName withtag $myLine 
        
            # -------------------------------
            # dimension line ends
        if { $dimLength > $checkLength } {
            set myEnd [my createLineEnd_2    $p_hl_dim_p1  $textAngle  $colour]
            lappend tagList $myEnd
                # $w addtag $tagListName withtag $myEnd
            set myEnd [my createLineEnd_2    $p_hl_dim_p2  [expr {180 + $textAngle}]  $colour]
            lappend tagList $myEnd
                # $w addtag $tagListName withtag $myEnd
        } else {
            set myEnd [my createLineEnd_2    $p_hl_dim_p1  $textAngle  $colour  {outside}]
            lappend tagList $myEnd
                # $w addtag $tagListName withtag $myEnd
            set myEnd [my createLineEnd_2    $p_hl_dim_p2  [expr {180 + $textAngle}]  $colour  {outside}]
            lappend tagList $myEnd
                # $w addtag $tagListName withtag $myEnd
        }
            #
            # -------------------------------
            # line extension on offset 
        if { $textOffset != 0 } {
            set extLength       [expr {1.5 * $fontSize / $stageScale}]
            set offsetAngle     [expr {180 + $textAngle}] 
            set offsetLength_1  [expr {$extLength + $textOffset}]
            set offsetLength_2  [expr {$extLength - $textOffset}]
            set refLength       [expr {$dimLength / 2}]
            set lineCenter      [cad4tcl::math::center $p_hl_dim_p1 $p_hl_dim_p2]
                # puts "     -> refLength       $refLength"
                # puts "     -> offsetLength_1      $offsetLength_1"
                # puts "     -> offsetLength_2      $offsetLength_2"

            set offsetPoint     [cad4tcl::math::rotateLine $lineCenter     $textOffset $offsetAngle]
            set offsetPoint_1   [cad4tcl::math::rotateLine $offsetPoint    $extLength  $offsetAngle]
            set offsetPoint_2   [cad4tcl::math::rotateLine $offsetPoint    $extLength  [expr {180 + $offsetAngle}]]
            if { $textOffset > 0 } {
                if {[expr {abs($offsetLength_1)}] > $refLength } {set p_hl_dim_p1 $offsetPoint_1}
            } else {
                if {[expr {abs($offsetLength_2)}] > $refLength } {set p_hl_dim_p2 $offsetPoint_2}
            }
            # --- debug in graphic
                # set myBase        [my createPoint  $cv_Dimension  $offsetPoint  1  orange]
                #   $w addtag $tagListName withtag $myBase
                # set myBase        [my createPoint  $cv_Dimension  $p_hl_dim_p1  1  red]
                #   $w addtag $tagListName withtag $myBase
                # set myBase        [my createPoint  $cv_Dimension  $p_hl_dim_p2  1  blue]
                #   $w addtag $tagListName withtag $myBase
        }
            #       
            # -------------------------------
            # dimension line
            # puts "\n--dimLine---------------"
        set myLine          [my createLine_2        $p_hl_dim_p1  $p_hl_dim_p2  $colour ]
        lappend tagList $myLine
            # $w addtag $tagListName withtag $myLine
            # 
            # puts " -> \$p_Coords $p_Coords"
            # puts "        -> help $p_hl_dim_p1  $p_hl_dim_p2"
            # puts "        -> \$p_Coords $p_Coords"
            # set aItem [$ItemInterface create line $p_Coords [list -fill red]]
            # lappend tagList $aItem
            # puts "        -> [$cvObject gettags $aItem]"
        
            # -------------------------------
            # text position
        set textPosition    [cad4tcl::math::center     $p_hl_dim_p1   $p_hl_dim_p2]  
        set textPosition    [cad4tcl::math::rotateLine $textPosition  $textOffset [expr {180 + $textAngle}]]
        
            #  ---------------------------------------------
            # create text
            # set myItem [my createPoint_2 $textPosition $fontSize red]
            # lappend tagList $myItem    
            
        set myText          [my createText_2 $dimLength  {}  $textPosition  $textPosAngle  $colour]
            # $w itemconfigure $myText -tags [list $myText __DimensionText__]
        lappend tagList $myText
            # puts "---> <D> [$w gettags $myText]"
            
            #
            #
            # -------------------------------
            # clenaup itemTag container for sure
        $w dtag  $tagListName all
            # 
            # -------------------------------
            # fill itemTag
        foreach item $tagList {
            # puts "---> <D> -- addtag -->  $item"
            # $w delete $item
            $w addtag $tagListName withtag $item
        }
        foreach item $tagListName {
            # puts "---> <D> -- check -- $tagListName -->  $item"
        }
            # 
            # -------------------------------
            #
        return $tagListName
            #
    }
        
        #
    method get_dimensionType {} {
        return {length}
    }
        #
}


