 ########################################################################
 #
 #  Copyright (c) Manfred ROSENBERGER, 2017
 #
 #      package: cadCanvas 	->	classDimension.tcl
 #
 # ----------------------------------------------------------------------
 #  namespace:  cadCanvas
 # ----------------------------------------------------------------------
 #
 #  2017/11/26
 #      extracted from the rattleCAD-project (Version 3.4.05)
 #          http://rattlecad.sourceforge.net/
 #          https://sourceforge.net/projects/rattlecad/
 #
 #

oo::define cad4tcl::RadiusDimensionCenter {
        #
    superclass cad4tcl::Dimension__Super
        #
    variable ItemInterface
    variable cvObject
        #
    variable _parentCanvas              ;# the canvas, the dimension is created
    variable _canvasRegistryNode        ;# the node in the cadCanvas registry
    variable _dimensionTag              ;# name of taglist containing all canvas elements of this dimension
    variable _textTag                   ;# contains the tag of the dimensiontext
    variable _fontSize                  ;# fontsize of dimension text
    variable _lineWidth                 ;# lione width of dimension
    variable _editTag                   ;# contains the tag of the sensitive dimension area
        #
    constructor {cvObj  itemIF  p_Coords  dimDistance  {textOffset 0}  {colour black}} {
            #
        # puts "            -> class AngleDimension"
            #
        set cvObject       $cvObj
            #
        set ItemInterface  $itemIF
            #
        my setCanvasSettings
            #
        set _dimensionTag   _empty_    
        set _editTag        _empty_
            #
        set _dimensionTag   [my createObject  $p_Coords  $dimDistance  $textOffset  $colour]    
            # puts "     -> \$_dimensionTag $_dimensionTag"     
            #
        set w $_parentCanvas
            #
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... cad4tcl::RadiusDimension $target_method $args  ... unknown"
    }
        #
    method createObject {p_Coords  dimDistAngle {textOffset 0} {colour black}} {
            #
        set w           [$cvObject configure    Canvas  Path]
        set fontSize    [$cvObject configure    Style   Fontsize]
        set stageScale  [$cvObject configure    Stage   Scale]
            #
            # ---------------------------------------------
        set tagListName [format "dimGroup_%s" [llength [$w find withtag all]] ]
            #
            #
            # puts "      \$w             $w"
            # puts "      \$fontSize      $fontSize"
            # puts "      \$stageScale    $stageScale"
            #
        set checkLength [expr {(1 + 2 * $fontSize) / $stageScale}]
            #
        set p0    [list [lindex $p_Coords 0] [lindex $p_Coords 1]]
        set p1    [list [lindex $p_Coords 2] [lindex $p_Coords 3]]
            #
            # -------------------------------
            # ceck $w
        if { $w == {} } { 
            error "cad4tcl::RadiusDimension -> Error:  could not get \$w" 
        }
            #
            # -------------------------------
            # correct in case of {} parameters ... execution through cad4tcl::dimension
        if {$textOffset == {}} {set textOffset  0}
        if {$colour     == {}} {set colour      black}
            #
            # -------------------------------
            # reset item container
        set tagList     {}
            # set tagListName [format "dimGroup_%s" [llength [$w find withtag all]] ]
            # $w dtag  $tagListName all

            # -------------------------------
            # set helpline parameters
        set p_hl_dim_p0     $p0
        set p_hl_dim_p1     [cad4tcl::math::rotatePoint    $p0     $p1  $dimDistAngle ]
        set textAngle       [cad4tcl::math::dirAngle       $p0     $p_hl_dim_p1] 
        set textPosAngle    [expr {$textAngle - 90}]
        set dimLength       [cad4tcl::math::length         $p0     $p1]

            # -------------------------------
            # dimension line ends
        if {$dimLength > $checkLength} {
            set myEnd [my createLineEnd_2   $p_hl_dim_p1  $textAngle  $colour  {outside}]
            lappend tagList $myEnd
                # $w addtag $tagListName withtag $myEnd
        } else {
            set myEnd [my createLineEnd_2   $p_hl_dim_p1  $textAngle  $colour ]
            lappend tagList $myEnd
                # $w addtag $tagListName withtag $myEnd
        }

            # -------------------------------
            # line extension on offset 
        if { $textOffset != 0 } {
            set extLength       [expr {1.5 * $fontSize / $stageScale}]
            set offsetAngle     $textAngle 
            set refLength       [expr {$dimLength / 2}]
            set lineCenter      [cad4tcl::math::center     $p_hl_dim_p0 $p_hl_dim_p1 ]
            set offsetPoint     [cad4tcl::math::rotateLine $lineCenter     [expr {$textOffset / $stageScale}]   $offsetAngle]
            set offsetPoint_0   [cad4tcl::math::rotateLine $offsetPoint    $extLength  [expr {180 + $offsetAngle}]]
            set offsetPoint_1   [cad4tcl::math::rotateLine $offsetPoint    $extLength               $offsetAngle]
            
            set offsetLength_0  [cad4tcl::math::length $lineCenter $offsetPoint_0]
            set offsetLength_1  [cad4tcl::math::length $lineCenter $offsetPoint_1]
            
            if { $offsetLength_0 > $offsetLength_1 } {
                if { $offsetLength_0 > $refLength } { 
                        # puts "        $offsetLength_0 > $offsetLength_1"
                        # puts "        $offsetLength_0"
                        # puts "        $refLength"
                    set p_hl_dim_p0 $offsetPoint_0 
                } 
            } else {
                if { $offsetLength_1 > $refLength } { 
                        # puts "        $offsetLength_0 < $offsetLength_1"
                        # puts "        $offsetLength_1"
                        # puts "        $refLength"
                    set p_hl_dim_p1 $offsetPoint_1 
                } 
            }
                #
                # --- debug in graphic
                #
        }
        
            # -------------------------------
            # dimension line
        set myLine      [my createLine_2             $p_hl_dim_p0  $p_hl_dim_p1  $colour ]
        lappend tagList $myLine
                # $w addtag $tagListName withtag $myLine
                        
            # -------------------------------
            # helpline arc
        set angle_p1    [cad4tcl::math::dirAngle   $p0 $p1]
        set arcAngle    [cad4tcl::math::angle      $p_hl_dim_p1 $p0 $p1]
        set angle_p1_h  [expr {$angle_p1 + $arcAngle}]
        set arcRadius   $dimLength

            # gap to helpline 
        set asinParameter   [expr {2.0 / ($stageScale * $arcRadius)}]
        set gapAngle        [expr {asin($asinParameter) * 180 / $cad4tcl::math::CONST_PI}]
        
        if { $dimDistAngle > 0 } { 
            set angle_p1    [expr {$angle_p1 + $gapAngle}]
        } else {
            set angle_p1    [expr {$angle_p1 - $arcAngle}]
            set arcAngle    [expr {$arcAngle - $gapAngle}]
        }

        if { [expr {$angle_p1_h - $angle_p1}] > 360} {
                # puts "    $checkValue "
            set myArc [createArc            $p0  $arcRadius  0 359.999  $colour]
            lappend tagList $myArc
                # $w addtag $tagListName withtag $myArc 
        } else {
            if {$arcAngle > 0.5 } { # check for clean vizualisation of arc
                if {[expr {$angle_p1 + $dimDistAngle}] < 360} {
                        # puts "    $checkValue  $dimDistAngle"
                    set myArc [my createArc_2   $p0  $arcRadius  $angle_p1 $arcAngle  $colour]
                    lappend tagList $myArc
                        # $w addtag $tagListName withtag $myArc 
                } else {
                        # puts "    $checkValue  $dimDistAngle"
                    set myArc [my createArc_2   $p0  $arcRadius  $angle_p1 $arcAngle  $colour]
                    lappend tagList $myArc
                        # $w addtag $tagListName withtag $myArc 
                    set myArc [my createArc_2   $p0  $arcRadius  0         [expr {$angle_p1 + $arcAngle - 360}]  $colour ]
                    lappend tagList $myArc
                        # $w addtag $tagListName withtag $myArc 
                }
            } else { 
                #puts "   -> break" 
            }
        }

            # -------------------------------
            # text position
        set textPosition    [cad4tcl::math::center         $p_hl_dim_p0  $p_hl_dim_p1]  
        set textPosition    [cad4tcl::math::rotateLine     $textPosition  [expr {$textOffset / $stageScale}] $textAngle]
            #
            #  ---------------------------------------------
            # create text
            # set myItem [my createPoint_2 $textPosition 2 red]
            # lappend tagList $myItem    
            
        set myText [my createText_2 $dimLength  {}  $textPosition  $textPosAngle  $colour ]
            # $w itemconfigure $myText -tags [list $myText __DimensionText__]
        lappend tagList $myText
                # puts "[$w itemconfigure $myText]"
                # $w addtag $tagListName withtag $myText

            #  ---------------------------------------------
            #  sum up and return
                # $w addtag $tagListName withtag $myBase
                # $w addtag $tagListName withtag $myHelp
                # $w addtag $tagListName withtag $myPos
                # $w move {myArc} 30 0
            #
            
            #
            #
            # -------------------------------
            # clenaup itemTag container for sure
        $w dtag  $tagListName all
            #
            # -------------------------------
            # fill itemTag
        foreach item $tagList {
            # puts "---> <D> -- addtag -->  $item"
            # $w delete $item
            $w addtag $tagListName withtag $item
        }
        foreach item $tagListName {
            # puts "---> <D> -- check -- $tagListName -->  $item"
        }
            # 
            # -------------------------------
        return $tagListName 
            #
    }
        #
    method get_dimensionType {} {
        return {radius}
    }
        #
}

