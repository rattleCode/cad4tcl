 ########################################################################
 #
 #  Copyright (c) Manfred ROSENBERGER, 2017
 #
 #      package: cadCanvas 	->	classDimension.tcl
 #
 # ----------------------------------------------------------------------
 #  namespace:  cadCanvas
 # ----------------------------------------------------------------------
 #
 #  2017/11/26
 #      extracted from the rattleCAD-project (Version 3.4.05)
 #          http://rattlecad.sourceforge.net/
 #          https://sourceforge.net/projects/rattlecad/
 #
 #

oo::define cad4tcl::Dimension__Super {
        #
    variable ItemInterface
    variable ItemFactory                ;# creates items on canvas
        #
    variable cvObject
        #
    variable _parentCanvas              ;# the canvas, the dimension is created
    variable _canvasRegistryNode        ;# the node in the cadCanvas registry
    variable _canvasScale               ;# scale of the canvas itself
    variable _stageScale                ;# scale of the displayed stage of cadCanvas
    variable _stageUnit                 ;# m, ... ???
    variable _unitScale                 ;# unitspecific scale of the canvas in relation to ... ???
    variable _dimensionUID              ;# a unique dimension id
    variable _dimensionTag              ;# name of taglist containing all canvas elements of this dimension
    variable _textTag                   ;# contains the tag of the dimensiontext
    variable _sensitiveTag              ;# contains the tag of the sensitive dimension area
    variable _sensitiveSize             ;# radius of sensitive area
    variable _fontSize                  ;# fontsize of dimension text
    variable _lineWidth                 ;# lione width of dimension
    
        #
        # ----------------------------------------------------
        #
    constructor {} {
            #
        # puts "            -> superclass Dimension__Super"
            #
    }
        #
    destructor { 
            #
        # puts "            -> [self] ... destroy Dimension__Super"
            #
        $_parentCanvas     delete $_dimensionTag
            #
        if [info exists _sensitiveTag] {
            $_parentCanvas delete $_sensitiveTag
        }
            #
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... cad4tcl::Dimension__Super::$target_method $args  ... unknown"
    }
        #
    method setCanvasSettings {} {
            #
            # set _canvasRegistryNode $canvasRegistryNode
            #
        set _parentCanvas   [$cvObject getCanvas]
        set _canvasScale    [$cvObject configure    Canvas  Scale]                
        set _stageScale     [$cvObject configure    Stage   Scale]                
            #   
        set _stageUnit      [$cvObject configure    Stage   Unit]
        set _unitScale      [$cvObject configure    Stage   UnitScale]
        set _lineWidth      [$cvObject configure    Style   Linewidth]
        set _fontSize       [$cvObject configure    Style   Fontsize]
            #
    }
        #
    method fit2Canvas {{myItem {}}} {
            #
        set w       $_parentCanvas
            #
        if {$myItem == {}} {
            set myItem  $_dimensionTag
        }
            #
        set dimScale [expr {$_canvasScale / $_unitScale}]
        $w scale $myItem  0 0  $dimScale $dimScale
            #
        set moveVector  [cad4tcl::_getBottomLeft $w]
        $w move $myItem [lindex $moveVector 0] [ lindex $moveVector 1]    
            #
    }
        #
    method get_parentCanvas {} {
        return $_parentCanvas
    }
        #
    method add_UID {tag} {
        set _dimensionUID $tag
        $_parentCanvas addtag $_dimensionUID withtag $_dimensionTag
        return $_dimensionTag
    }
        #
    method get_dimensionTag {} {
        return $_dimensionTag
    }
        #
    method get_dimensionType {} {
        return {}
    }
        #
    method get_textTag {} {
        if {[info exists _textTag]} {
        return $_textTag
        } else {
            return {}
        }
    }
        #
    method get_editTag {} {
        return $_editTag
    }
        #
    method get_UID {} {
        return $_dimensionUID
    }
        #
    method createSensitiveArea {{activeColor wheat}} {
            #
        set w           [$cvObject configure    Canvas  Path]
        set canvasScale [$cvObject configure    Canvas  Scale]
        set unitScale   [$cvObject configure    Stage   UnitScale]
        set fontSize    [$cvObject configure    Style   Fontsize]
           #
        set unitScale_p [cad4tcl::_getUnitRefScale    p]
            #
        if {[my get_textTag] eq {}} {
            return {}
        }
            #
        set textCoords  [$w bbox $_textTag]
            #
            # puts "  createSensitiveArea"
            # puts "  <D> \$textCoords $textCoords"
        if {$textCoords == {}} return
            #
        set textCenter  [cad4tcl::_getBBoxInfo  $textCoords center]
            # puts "  <D> \$textCenter $textCenter"
            #
        set editSize    [expr {2 * $_fontSize * 1 * $canvasScale / (1 * $unitScale)}]
            # puts "  <D> \$editSize   $editSize"
            # puts "  <D> \$unitScale    $unitScale"
            # puts "  <D> \$canvasScale  $canvasScale"
            #
        foreach {x y} $textCenter {
            set x1 [expr {$x - $editSize}]
            set x2 [expr {$x + $editSize}]
            set y1 [expr {$y - $editSize}]
            set y2 [expr {$y + $editSize}]
        }
        set coords      [list   $x1 $y1 $x2 $y2]
            #
        set tagName [format "sensitive_%s" [llength [$w find withtag all]] ]    
            #
            # set activeColor    {light gray}
            # set activeColor    {beige}
            # set activeColor    {wheat}
            #
        set _sensitiveTag   [$w create  oval    $coords \
                                -tags           "$tagName __Sensitive__" \
                                -fill           {} \
                                -disabledfill   {} \
                                -activefill     $activeColor \
                                -outline        {}] 
            #
            # puts "  <D>     -> $tagName $_sensitiveTag  "
        $w raise $_sensitiveTag   
        $w raise $_textTag   
        $w addtag $_dimensionTag    withtag $tagName  
            #
        $w addtag {__Content__}     withtag $tagName
        $w addtag {__Dimension__}   withtag $tagName
        $w addtag {__Sensitive__}   withtag $tagName
        $w addtag {__NoDrag__}      withtag $tagName
            #
            # puts "  <D>     -> [$w gettags  $tagName]"    
            # puts "  <D> createSensitiveArea [$w coords $tagName]"    
            #
            # my fit2Canvas $_sensitiveTag   
            #
        return $tagName
            #
    }
        #
    method report_UID {} {
        puts "    ->   $_parentCanvas -> $_dimensionTag --> $_dimensionUID"
    }
        #
    method noramlizeAngle {angle} {
        while { $angle < 0} {
            set angle   [expr {$angle + 360}]
        }
        while { $angle > 360} {
            set angle   [expr {$angle - 360}]
        }
        return $angle
    }
        #
        #
    method createLine_2 {p1 p2 colour} {
            #
        set w           [$cvObject configure    Canvas  Path]
        set canvasScale [$cvObject configure    Canvas  Scale]
        set stageScale  [$cvObject configure    Stage   Scale]
        set unitScale   [$cvObject configure    Stage   UnitScale]
        set lineWidth   [$cvObject configure    Style   Linewidth]
            #
            # ---------------------------------------------
        set myTagName   [format "dimLine_%s" [llength [$w find withtag all]] ]
            #
            #
        set coords      [list   [lindex $p1  0]  [lindex $p1  1] \
                                [lindex $p2  0]  [lindex $p2  1]]
            #
        set p1 [lrange $coords 0 1]
        set p2 [lrange $coords 2 3]
            #
        set myItem  [$ItemInterface create  line \
                            $coords  \
                            [list \
                                -outline    $colour \
                                -width      $lineWidth \
                                -tags       [list __Dimension__ $myTagName]]]
            #
        return $myItem
            #
    }
        #  
    method createArc_2 {p r start extent colour} {
            #
        set w           [$cvObject configure    Canvas  Path]
        set stageScale  [$cvObject configure    Stage   Scale]
        set lineWidth   [$cvObject configure    Style   Linewidth]
            #
            # ---------------------------------------------
        set myTagName   [format "dimArc_%s" [llength [$w find withtag all]] ]
            #
            #
        lassign $p  x y
            # foreach {x y} $p break
        set coords  [list   [expr {$x - $r}] [expr {$y - $r}] \
                            [expr {$x + $r}] [expr {$y + $r}]]
            #
        set myItem  [$cvObject create arc \
                            $p \
                            [list \
                                -radius  $r \
                                -start   $start  \
                                -extent  $extent  \
                                -style   {arc}  \
                                -outline $colour  \
                                -width   $lineWidth \
                                -tags   [list __Dimension__ $myTagName]]]
            #
        return $myTagName
            #
    }
        #  
    method createLineEnd_2 {p end_angle colour {style inside} } {
            #
        set w           [$cvObject configure    Canvas  Path]
        set stageScale  [$cvObject configure    Stage   Scale]
        set lineWidth   [$cvObject configure    Style   Linewidth]
        set stageUnit   [$cvObject configure    Stage   Unit]
        set fontSize    [$cvObject configure    Style   Fontsize]
        set lineDist    [$cvObject configure    Style   Fontdist]
            #
            # ---------------------------------------------
        set myTagName   [format "dimEnd_%s" [llength [$w find withtag all]] ]
            #
            #
        if { $style != {inside} } { 
                #
            set end_angle   [expr {$end_angle + 180}] 
            set p_outl      [cad4tcl::math::rotateLine  $p  [expr ($fontSize + 2) / $stageScale]  $end_angle]
            set coords      [list   [lindex $p      0]  [lindex $p      1] \
                                    [lindex $p_outl 0]  [lindex $p_outl 1]]
            set myItem  [$ItemInterface create  line  \
                            $coords  \
                            [list \
                                -outline    $colour \
                                -width      $lineWidth \
                                -tags       [list __Dimension__ $myTagName]]]
                #
        }
            #
            # -------------------------------
            # create arrow-end
        set asl     [expr {$fontSize / ($stageScale * cos([expr 15 * (4 * atan(1)) / 180]))}]
            #       [expr $fontSize / [expr $stageScale * cos([expr 15 * (4 * atan(1)) / 180])]]
        set p0      [cad4tcl::math::rotateLine     $p  [expr {1/$stageScale}]    [expr {$end_angle + 90  }]]
        set p1      [cad4tcl::math::rotateLine     $p  $asl                      [expr {$end_angle +  7.5}]]
        set p2      [cad4tcl::math::rotateLine     $p  $asl                      [expr {$end_angle -  7.5}]]
        set p3      [cad4tcl::math::rotateLine     $p  [expr {1/$stageScale}]    [expr {$end_angle - 90  }]]
            #
        set coords  [list   [lindex $p0  0]  [lindex $p0  1] \
                            [lindex $p   0]  [lindex $p   1] \
                            [lindex $p1  0]  [lindex $p1  1] \
                            [lindex $p2  0]  [lindex $p2  1] \
                            [lindex $p   0]  [lindex $p   1] \
                            [lindex $p3  0]  [lindex $p3  1]]
            #
        set myItem  [$ItemInterface create  line  \
                            $coords  \
                            [list \
                                -outline    $colour \
                                -width      $lineWidth \
                                -tags       [list __Dimension__ $myTagName]]]
            # $cvObject   addtag  $tagNameEnd withtag $myItem
            #
        return $myTagName
            #
    }
        #
    method createText_2 {dimValue format p dimAngle colour } {
            #
        set w           [$cvObject configure    Canvas      Path]
        set canvasScale [$cvObject configure    Canvas      Scale]
        set stageScale  [$cvObject configure    Stage       Scale]
        set stageUnit   [$cvObject configure    Stage       Unit]
        set fontSize    [$cvObject configure    Style       Fontsize]
        set fontColour  [$cvObject configure    Style       Fontcolour]
        set fontDist    [$cvObject configure    Style       Fontdist]
        set font        [$cvObject configure    Style       Font]
        set fontStyle   [$cvObject configure    Style       Fontstyle]
        set dimPrec     [$cvObject configure    Style       Precision]
        set lineWidth   [$cvObject configure    Style       Linewidth]
            #
        set unitScale   [$cvObject configure    Stage       UnitScale]
        set unitScale_p [$cvObject configure    UnitScale   p]
            #
            # ---------------------------------------------
        set myTagName   [format "dimText_%s" [llength [$w find withtag all]] ]
            #
            # puts "createText_2: [info object class [self]]"
            #
            #
            # format text
            #
        if {$fontColour != $colour} {set fontColour $colour}
        if {$dimValue < 0 } {set dimValue [expr {-1 * $dimValue}]}
        set formatString    "\%.${dimPrec}f"
        set dimValue        [format "$formatString" $dimValue]
            #
            # remove trailing zeros
            #
        set leading         [lindex [split $dimValue .] 0]
        set trailing        [lindex [split $dimValue .] 1]            
        if {[expr {$dimValue == $leading}]} {
            set text        [format "%s%s"  $leading $format]
        } else {
            set trailing [string trimright $trailing {0}]
            if {$trailing == {}} {
                set text    [format "%s%s"    $leading $format]
            } else {
                set text    [format "%s,%s%s" $leading $trailing $format]
            }
        }
            #
            # geometric definitions
            #
        set checkAngle $dimAngle
        if {$dimAngle <   0} { set checkAngle [expr {$dimAngle + 360}] }
        if {$dimAngle > 360} { set checkAngle [expr {$dimAngle - 360}] }
            #
        set fontDist    [expr {$fontDist / $stageScale}]
            #
        if {$checkAngle > 180} {
            set textOrient  +90
            set textPos     [cad4tcl::math::rotateLine   $p $fontDist [expr {180 + $dimAngle}]]
        } elseif { $checkAngle == 0} {
            set textOrient  +90
            set textPos     [cad4tcl::math::rotateLine   $p $fontDist [expr {180 + $dimAngle}]]
        } else {
            set textOrient  -90
            set textPos     [cad4tcl::math::rotateLine   $p $fontDist $dimAngle]
        }
            #
        lassign $p  x y
            # foreach {x y} $p break
            #
            # puts ""
            # puts "createText"
            # puts "      \$dimPrec       $dimPrec"
            # puts "      \$font          $font"
            # puts "      \$fontColour    $fontColour"
            # puts "      \$fontDist      $fontDist"
            # puts "      \$fontSize      $fontSize"
            # puts "      \$fontStyle     $fontStyle"
            # puts "      \$stageScale    $stageScale"
            # puts "      \$stageUnit     $stageUnit "
            #
            # -------------------------------
            # depend on style
        if {$fontStyle == "vector"} {
                #
            set mySize      [expr {$fontSize / $stageScale}]
                # set myWidth     [expr 0.1 * $mySize]
                # set myWidth     [expr 0.1 * $mySize / ($unitScale_p * $unitScale_p * $unitScale)]
                # set myWidth     [expr 0.1 * $mySize * ($unitScale / $unitScale_p)]
                # set myWidth     [expr 0.1 * $fontSize / ($stageScale * $canvasScale)]
            set myWidth     [expr {$lineWidth / $stageScale}]
                #
                # set myItem  [$ItemInterface create vectorText ]
            set argList [list \
                            -text       $text \
                            -anchor     s \
                            -angle      [expr $dimAngle + $textOrient] \
                            -outline    $fontColour \
                            -width      $lineWidth \
                            -size       $mySize \
                            -tags       [list __Dimension__ $myTagName]
                        ]
            set myItem  [$ItemInterface create vectorText \
                            $textPos \
                            $argList 
                        ]
                #
                # $cvObject configure $myItem -width $lineWidth    
                #
        } else {
                #
            set myScale     [expr {1.0 * ($unitScale / $unitScale_p)}]
                #
            set fontSize    [expr {round($fontSize * $myScale)}]
            set font        [format "%s%s"    $font $fontSize]
                # puts "      \$fontSize      $fontSize"
            set myItem      [$w create text  $x $y \
                                -text   $text \
                                -anchor s \
                                -fill   $fontColour \
                                -font   $font \
                                -tags   [list __Dimension__ $myTagName]]
                #
        }
            #
            # puts "   -> [self] -> createText_2 <-- $myItem"
            #
        set _textTag $myTagName
            #
        return $myTagName
            #
    }
        #  
    method createText_3 {preFix dimValue postFix p dimAngle colour } {
            #
        set w           [$cvObject configure    Canvas      Path]
        set canvasScale [$cvObject configure    Canvas      Scale]
        set stageScale  [$cvObject configure    Stage       Scale]
        set stageUnit   [$cvObject configure    Stage       Unit]
        set fontSize    [$cvObject configure    Style       Fontsize]
        set fontColour  [$cvObject configure    Style       Fontcolour]
        set fontDist    [$cvObject configure    Style       Fontdist]
        set font        [$cvObject configure    Style       Font]
        set fontStyle   [$cvObject configure    Style       Fontstyle]
        set dimPrec     [$cvObject configure    Style       Precision]
        set lineWidth   [$cvObject configure    Style       Linewidth]
            #
        set unitScale   [$cvObject configure    Stage       UnitScale]
        set unitScale_p [$cvObject configure    UnitScale   p]
            #
            # ---------------------------------------------
        set myTagName   [format "dimText_%s" [llength [$w find withtag all]] ]
            #
            #
            # format text
            #
        if {$fontColour != $colour} {set fontColour $colour}
        if {$dimValue < 0 } {set dimValue [expr {-1 * $dimValue}]}
        set formatString    "\%.${dimPrec}f"
        set dimValue        [format "%s$formatString" $preFix $dimValue]
            #
            # remove trailing zeros
            #
        set leading         [lindex [split $dimValue .] 0]
        set trailing        [lindex [split $dimValue .] 1]            
        if {[expr {$dimValue == $leading}]} {
            set text        [format "%s%s"  $leading $postFix]
        } else {
            set trailing [string trimright $trailing {0}]
            if {$trailing == {}} {
                set text    [format "%s%s"    $leading $postFix]
            } else {
                set text    [format "%s,%s%s" $leading $trailing $postFix]
            }
        }
            #
            # geometric definitions
            #
        set checkAngle $dimAngle
        if {$dimAngle <   0} { set checkAngle [expr {$dimAngle + 360}] }
        if {$dimAngle > 360} { set checkAngle [expr {$dimAngle - 360}] }
            #
        set fontDist    [expr {$fontDist / $stageScale}]
            #
        if {$checkAngle > 180} {
            set textOrient  +90
            set textPos     [cad4tcl::math::rotateLine   $p $fontDist [expr {180 + $dimAngle}]]
        } elseif { $checkAngle == 0} {
            set textOrient  +90
            set textPos     [cad4tcl::math::rotateLine   $p $fontDist [expr {180 + $dimAngle}]]
        } else {
            set textOrient  -90
            set textPos     [cad4tcl::math::rotateLine   $p $fontDist $dimAngle]
        }
            #
        lassign $p  x y
            # foreach {x y} $p break
            #
            # puts ""
            # puts "createText"
            # puts "      \$dimPrec       $dimPrec"
            # puts "      \$font          $font"
            # puts "      \$fontColour    $fontColour"
            # puts "      \$fontDist      $fontDist"
            # puts "      \$fontSize      $fontSize"
            # puts "      \$fontStyle     $fontStyle"
            # puts "      \$stageScale    $stageScale"
            # puts "      \$stageUnit     $stageUnit "
            #
            # -------------------------------
            # depend on style
        if {$fontStyle == "vector"} {
                #
            set mySize      [expr {$fontSize / $stageScale}]
                # set myWidth     [expr 0.1 * $mySize]
                # set myWidth     [expr 0.1 * $mySize / ($unitScale_p * $unitScale_p * $unitScale)]
                # set myWidth     [expr 0.1 * $mySize * ($unitScale / $unitScale_p)]
                # set myWidth     [expr 0.1 * $fontSize / ($stageScale * $canvasScale)]
            set myWidth     [expr {$lineWidth / $stageScale}]
                #
                # set myItem  [$ItemInterface create vectorText ]
            set argList [list \
                            -text       $text \
                            -anchor     s \
                            -angle      [expr $dimAngle + $textOrient] \
                            -outline    $fontColour \
                            -width      $lineWidth \
                            -size       $mySize \
                            -tags       [list __Dimension__ $myTagName]
                        ]
            set myItem  [$ItemInterface create vectorText \
                            $textPos \
                            $argList 
                        ]
                #
                # $cvObject configure $myItem -width $lineWidth    
                #
        } else {
                #
            set myScale     [expr {1.0 * ($unitScale / $unitScale_p)}]
                #
            set fontSize    [expr {round($fontSize * $myScale)}]
            set font        [format "%s %s"   $font $fontSize]
                # puts "      \$fontSize      $fontSize"
            set myItem      [$w create text  $x $y \
                                -text   $text \
                                -anchor s \
                                -fill   $fontColour \
                                -font   $font \
                                -tags   [list __Dimension__ $myTagName]]
                #
        }
            #
            # puts "   -> [self] -> createText_2 <-- $myItem"
            #
        set _textTag $myTagName
            #
        return $myTagName
            #
    }
        #  
    method createPoint_2 {p radius colour} {
            #
        set w           [$cvObject configure    Canvas  Path]
            #
            # ---------------------------------------------
        set myTagName   [format "dimPoint_%s" [llength [$w find withtag all]] ]
            #
        set myItem      [$ItemInterface create circle \
                            $p \
                            [list \
                                -r      $radius \
                                -fill   $colour \
                                -width  0 \
                                -tags   [list __Dimension__ $myTagName]]]
            #
        return $myTagName
            #
    }
        #
}
