 ########################################################################
 #
 # cad4tcl___test_00.tcl
 # by Manfred ROSENBERGER
 #
 #   (c) Manfred ROSENBERGER 2018/07/04
 #
 #

set TEST_ROOT_Dir    [file normalize [file dirname [lindex $argv0]]]
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
    #
set TEST_Sample_Dir  [file join $TEST_ROOT_Dir _sample]
    #
set TEST_Export_Dir  [file join $TEST_ROOT_Dir _export]
    #
    #
puts "    -> \$TEST_ROOT_Dir ...... $TEST_ROOT_Dir"
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
puts "    -> \$TEST_Sample_Dir .... $TEST_Sample_Dir"
puts "    -> \$TEST_Export_Dir .... $TEST_Export_Dir"
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"
lappend auto_path [file join $TEST_Library_Dir __ext_Libraries]
    #
foreach dir $tcl_library {
    puts "   -> tcl_library $dir"
}    
    #
foreach dir $auto_path {
    puts "   -> auto_path   $dir"
}    
    #
    #
package require Tk
package require cad4tcl 0.10
    #
    #
    # cad4tcl::canvasType
    # ... 1 use tkpath if available
    # ... 0 use tk::canvas, dont search for tkpath
    #
set cad4tcl::canvasType 1
    #
    #
frame .cf
pack  .cf  -fill both
    #
    #
set cvObject    [cad4tcl::new  .cf  800  600  A4  1.0  30]
set cv_scale    [$cvObject configure Canvas Scale]
set st_scale    [$cvObject configure Stage Scale]
    #
puts "   -> \$cvObject $cvObject"
puts "   -> \$cv_scale $cv_scale"
puts "   -> \$st_scale $st_scale"
    #
    #
frame .bf
pack  .bf  -fill x
    #
    #
button .bf.btFit     -text "fit"  -width 10  -command [list fit_Board]
pack   .bf.btFit     -side left
proc fit_Board {} {
    variable  cvObject
    variable  cv_scale 
    puts "-> fit_Board:     $cvObject "
    puts "   -> cv_scale:   $cv_scale"
    set cv_scale [$cvObject fit]
}
    #
    #
button .bf.btStScale-  -text " - "          -width  5  -command [list scale_Stage minus]
pack   .bf.btStScale-  -side left
button .bf.btStScale   -text "scale stage"  -width 10  -command [list scale_Stage 1]
pack   .bf.btStScale   -side left
button .bf.btStScale+  -text " + "          -width  5  -command [list scale_Stage plus]
pack   .bf.btStScale+  -side left
proc scale_Stage {direction} {
    variable  cvObject
    variable  st_scale
    set st_Scale [$cvObject configure Stage Scale]
    switch -exact -- $direction {
        minus {
            if {$st_Scale > 0.5} {
                set st_Scale    [expr {$st_Scale - 0.25}]
            }
        }
        plus {
            if {$st_Scale < 5} {
                set st_Scale    [expr {$st_Scale + 0.25}]
            }
        }
        default {
            set st_Scale        1
        }
    }
    set st_Scale [$cvObject configure Stage Scale $st_Scale]
    puts "   -> st_Scale:       $st_Scale"
        #
    delete_Content
        #
    create_Content
        #
    # set cv_scale [$cvObject center [expr {$scale * $cv_scale}]]
}
    #
    #
    #
button .bf.btCvScale-  -text " - "          -width  5  -command [list scale_Canvas minus]
pack   .bf.btCvScale-  -side left
button .bf.btCvScale   -text "scale canvas" -width 10  -command [list scale_Canvas reset]
pack   .bf.btCvScale   -side left
button .bf.btCvScale+  -text " + "          -width  5  -command [list scale_Canvas plus]
pack   .bf.btCvScale+  -side left
proc scale_Canvas {direction} {
    variable  cvObject
    variable  cv_Scale
    variable  st_Scale
    set st_Scale [$cvObject configure Stage Scale]
    set cv_Scale [$cvObject configure Canvas Scale]
    puts "  -> scale_Canvas:   $cvObject"
    puts "   -> cv_Scale:       $cv_Scale"
    switch -exact -- $direction {
        minus {
            if {$cv_Scale > 0.2} {
                set cv_Scale    [expr {$cv_Scale - 0.1}]
            }
        }
        plus {
            if {$cv_Scale < 5} {
                set cv_Scale    [expr {$cv_Scale + 0.1}]
            }
        }
        default {
            set cv_Scale        1
        }
    }
    set cv_scale [$cvObject center $cv_Scale]
        # set cv_Scale [$cvObject configure Canvas Scale $cv_Scale]
    puts "   -> cv_Scale:       $cv_Scale"   
        
    # set st_Scale [$cvObject center [expr {$st_Scale}]]
        #
    # delete_Content
        #
    # create_Content
        #
}
    #
    #
button .bf.btClear   -text "clear Content"  -width 15  -command delete_Content
pack   .bf.btClear   -side left
proc delete_Content {} {
    variable cvObject
    $cvObject deleteContent
}
    #
    #
button .bf.btCreate  -text "create Content"  -width 15  -command create_Content
pack   .bf.btCreate  -side left
proc create_Content {} {
    variable  cvObject
    variable  cv_scale
    puts "  -> create_Content:  $cvObject"
    puts "   -> cv_scale:       $cv_scale"
        #
        # $cvObject deleteContent
        #
    set systemTime  [clock seconds]
    set timeString  [clock format $systemTime -format {%Y.%d.%m %H:%M:%S}]
    set description [package versions cad4tcl]
        #
    $cvObject create draftFrame \
                        {} \
                        -label "[file tail $::argv0]" \
                        -title "Drafting Frame" \
                        -date  $timeString \
                        -descr $description
        #
    $cvObject create  draftRaster   {} -defRaster {5 lightgray 10 lightgray}
        #
        #
    if 1 {
            #
        $cvObject create line \
                        [list 30 20 100 20] \
                        -tags {__test__} \
                        -outline darkblue \
                        -width 5
                        
        $cvObject create line \
                        [list 30 30 100 30] \
                        -tags {__test__} \
                        -outline blue \
                        -width 2.5
                        
            #
            # -- centerLine        
            #
        $cvObject create centerLine \
                        [list 30 180 150 180] \
                        -tags {__test__} \
                        -outline darkred
                        
        $cvObject create centerLine \
                        [list 30 160 150 160] \
                        -tags {__test__} \
                        -outline red \
                        -width 1
        $cvObject create centerLine \
                        [list 30 150 150 150] \
                        -tags {__test__} \
                        -outline red \
                        -strokedasharray {12 1 1 1} \
                        -width 1
                     
        $cvObject create centerLine \
                        [list  30 130 150 130] \
                        -tags {__test__} \
                        -outline darkred \
                        -width 5
        $cvObject create centerLine \
                        [list  30 120 150 120] \
                        -tags {__test__} \
                        -outline darkred \
                        -width 2
        $cvObject create centerLine \
                        [list  30 110 150 110] \
                        -tags {__test__} \
                        -outline darkred \
                        -width 1
        $cvObject create centerLine \
                        [list  30 100 150 100] \
                        -tags {__test__} \
                        -outline darkred \
                        -width 0.5
        $cvObject create centerLine \
                        [list  30  90 150  90] \
                        -tags {__test__} \
                        -outline darkred \
                        -width 0.25   

        $cvObject create centerLine \
                        [list  30  70 150  70] \
                        -tags {__test__} \
                        -outline darkred \
                        -strokedasharray {24 2 2 2} \
                        -width 2   
        $cvObject create centerLine \
                        [list  30  60 150  60] \
                        -tags {__test__} \
                        -outline darkred \
                        -strokedasharray {24 2 2 2} \
                        -width 1   
        $cvObject create centerLine \
                        [list  30  50 150  50] \
                        -tags {__test__} \
                        -outline darkred \
                        -strokedasharray {24 2 2 2} \
                        -width 0.5   
            #
            #
            # -- hiddenLine        
            #
        $cvObject create hiddenLine \
                        [list 160 180 260 180] \
                        -tags {__test__} \
                        -outline darkred
                        
        $cvObject create hiddenLine \
                        [list 160 160 260 160] \
                        -tags {__test__} \
                        -outline darkred \
                        -width 1
        $cvObject create hiddenLine \
                        [list 160 150 260 150] \
                        -tags {__test__} \
                        -outline darkred \
                        -strokedasharray {5 1} \
                        -width 1
                        
        $cvObject create hiddenLine \
                        [list 160 130 260 130] \
                        -tags {__test__} \
                        -outline darkred \
                        -width 5
        $cvObject create hiddenLine \
                        [list 160 120 260 120] \
                        -tags {__test__} \
                        -outline darkred \
                        -width 2
        $cvObject create hiddenLine \
                        [list 160 110 260 110] \
                        -tags {__test__} \
                        -outline darkred \
                        -width 1
        $cvObject create hiddenLine \
                        [list 160 100 260 100] \
                        -tags {__test__} \
                        -outline darkred \
                        -width 0.5
        $cvObject create hiddenLine \
                        [list 160  90 260  90] \
                        -tags {__test__} \
                        -outline darkred \
                        -width 0.25            

        $cvObject create hiddenLine \
                        [list 160  70 260  70] \
                        -tags {__test__} \
                        -outline darkred \
                        -strokedasharray {10 2} \
                        -width 2   
        $cvObject create hiddenLine \
                        [list 160  60 260  60] \
                        -tags {__test__} \
                        -outline darkred \
                        -strokedasharray {10 2} \
                        -width 1   
        $cvObject create hiddenLine \
                        [list 160  50 260  50] \
                        -tags {__test__} \
                        -outline darkred \
                        -strokedasharray {10 2} \
                        -width 0.5              
            #
            #
    }                
    # return
                    
    if 0 {
            set w [$cvObject getCanvas]
            puts "    -> \$w $w"
                #
                # --- createCenterLine - polyline 19.767000000000003 -59.301 98.83500000000001 -59.301 -tags __test__ -strokedasharray {24.0 2.0 2.0 2.0} -stroke red -strokewidth 1.2457784342688332
            set coordList {180 180 460 180}
            set argList   {-tags __test__ -strokedasharray {24.0 2.0 2.0 2.0} -stroke red -strokewidth 1.2457784342688332}
            eval $w create polyline    $coordList  $argList
                #
                # --- createCenterLine - polyline 19.767000000000003 -65.89 98.83500000000001 -65.89 -tags __test__ -strokedasharray {48.0 4.0 4.0 4.0} -stroke blue -strokewidth 0.6228892171344166
            set coordList {180 150 460 150}
            set argList   {-tags __test__ -strokedasharray {48.0 4.0 4.0 4.0} -stroke blue -strokewidth 0.6228892171344166}
            eval $w create polyline    $coordList  $argList
                #
                # --- createCenterLine - polyline 19.767000000000003 -65.89 98.83500000000001 -65.89 -tags __test__ -strokedasharray {48.0 4.0 4.0 4.0} -stroke blue -strokewidth 0.6228892171344166
            set y       120
            set width   0.9
            while {$y <= 400} {
                    #
                set coordList [list 500 $y 650 $y]
                set argList   [list -tags __test__ -strokedasharray {48.0 4.0 4.0 4.0} -stroke lightblue -strokewidth $width]
                eval $w create polyline    $coordList  $argList
                    #
                    # .cf.cadCanvas_1 create text  13.178 -31.35828125  -tags vectorText_01 -font font1 -anchor se -text 0123456789
                set coordList [list 550 [expr {$y + 7}]]
                set argList   [list -tags vectorText_01 -font font1 -anchor se -text " [format {%.3f} $width]"]
                eval $w create text    $coordList  $argList
                    #
                incr y      20
                set width   [expr {$width + 0.05}]
                    #
            }
                #
            set y       120
            set width   0.9
            while {$y <= 400} {
                    #
                set coordList [list 700 $y 850 $y]
                set argList   [list -tags __test__ -strokedasharray {24.0 2.0 2.0 2.0} -stroke darkblue -strokewidth $width]
                eval $w create polyline    $coordList  $argList
                    #
                    # .cf.cadCanvas_1 create text  13.178 -31.35828125  -tags vectorText_01 -font font1 -anchor se -text 0123456789
                set coordList [list 750 [expr {$y + 7}]]
                set argList   [list -tags vectorText_01 -font font1 -anchor se -text " [format {%.3f} $width]"]
                eval $w create text    $coordList  $argList
                    #
                incr y      20
                set width   [expr {$width + 0.05}]
                    #
            }
                #
    }
    
    
    if 0 {
            #
        $cvObject create centerLine \
                        [list 30 190 150 190] \
                        -tags {__test__} \
                        -outline red \
                        -strokedasharray {12 1 1 1} \
                        -width 0.5
                        
        puts "\n -- 001 --\n"
        $cvObject create centerLine \
                        [list 30 170 150 170] \
                        -tags {__test__} \
                        -outline blue \
                        -strokedasharray {12 1 1 1} \
                        -width 0.45
        $cvObject create centerLine \
                        [list 30 160 150 160] \
                        -tags {__test__} \
                        -outline blue \
                        -strokedasharray {12 1 1 1} \
                        -width 0.40
        puts "\n -- 002 -- \n"
        $cvObject create centerLine \
                        [list 30 150 150 150] \
                        -tags {__test__} \
                        -outline blue \
                        -strokedasharray {12 1 1 1} \
                        -width 0.39
        $cvObject create centerLine \
                        [list 30 140 150 140] \
                        -tags {__test__} \
                        -outline blue \
                        -strokedasharray {12 1 1 1} \
                        -width 0.38
        $cvObject create centerLine \
                        [list 30 130 150 130] \
                        -tags {__test__} \
                        -outline blue \
                        -strokedasharray {12 1 1 1} \
                        -width 0.37
        $cvObject create centerLine \
                        [list 30 120 150 120] \
                        -tags {__test__} \
                        -outline blue \
                        -strokedasharray {12 1 1 1} \
                        -width 0.36
        $cvObject create centerLine \
                        [list 30 110 150 110] \
                        -tags {__test__} \
                        -outline blue \
                        -strokedasharray {12 1 1 1} \
                        -width 0.35
            #
        # $cvObject create text \
                            {50 100} \
                            -text "-width 0.25" \
                            -tags {vectorText_01} 
            #
    }
    
    if 0 {
            #
        $cvObject create centerLine \
                        [list 30 110 150 110] \
                        -tags {__test__} \
                        -outline darkred \
                        -strokedasharray {14 2 2 2} \
                        -width 0.5
        $cvObject create centerLine \
                        [list 30 120 150 120] \
                        -tags {__test__} \
                        -outline darkred \
                        -strokedasharray {14 2 2 2} \
                        -width 1.0
        $cvObject create centerLine \
                        [list 30 130 150 130] \
                        -tags {__test__} \
                        -outline darkred \
                        -strokedasharray {14 2 2 2} \
                        -width 2.0
        $cvObject create centerLine \
                        [list 30 140 150 140] \
                        -tags {__test__} \
                        -outline darkred \
                        -strokedasharray {14 2 2 2} \
                        -width 5.0
        $cvObject create centerLine \
                        [list 30 150 150 150] \
                        -tags {__test__} \
                        -outline orange \
                        -width 1
            #
    }


        
        #
    return
        #
    if 0 {
        set y0   30
        set y1  190
        set x    25
        while {$x <= 270} {
                #
            $cvObject create line \
                            [list $x $y0 $x $y1] \
                            -tags {__DraftFrame__} \
                            -outline lightgray \
                            -width 0.001
                #
            incr x 5
                # puts "   -> $x"
                #
        }
            #
        set x0   20
        set x1  275
        set y    35
        while {$y <= 185} {
                #
            $cvObject create line \
                            [list $x0 $y $x1 $y] \
                            -tags {__DraftFrame__} \
                            -outline lightgray \
                            -width 0.001
                #
            incr y 5
                # puts "   -> $x"
                #
        }
    }
        #
        #
    if 0 {
            #
        $cvObject create circle \
                            {30 175} \
                            -tags {circle_01} \
                            -radius 10 \
                            -fill lightgray \
                            -outline red
        $cvObject create oval \
                            {60 185  100 165} \
                            -tags {oval_01} \
                            -fill lightgray \
                            -outline red
            #   
        $cvObject create rectangle \
                            {20 125  100 155} \
                            -tags {rectangle_01} \
                            -fill lightgray \
                            -outline red
            #   
        $cvObject create polygon \
                            { 20 90   50 90   90 100    90 110   50 110   35 105   20 105} \
                            -tags {polygon_01} \
                            -fill lightgray \
                            -outline red
        $cvObject create polyline \
                            {110 80  140 80  180  90   180 100  130 100  125  95  110  95} \
                            -tags {line_01} \
                            -fill lightgray \
                            -outline blue
        $cvObject create line \
                            {200 70  230 70  270  80   270  90  220  90  215  85  200  85} \
                            -tags {line_02} \
                            -fill lightgray \
                            -outline orange
            #   
        $cvObject create centerLine \
                            {120 130  170 130  260 100} \
                            -tags {centerLine_01} \
                            -outline blue
            #   
            #   
        $cvObject create vectorText \
                            {20 50} \
                            -text "vectorText  abcdefghijklmnopqrstuvexyz 0123456789 °" \
                            -tags {vectorText_01}  
        $cvObject create vectorText \
                            {20 40} \
                            -text "vectorText  ABCDEFGHIJKLMNOPQRSTUVEXYZ 0123456789 °" \
                            -tags {vectorText_02}
            #
        $cvObject create circle \
                            {150 150} \
                            -tags {dim_01} \
                            -radius 1 \
                            -fill lightgray \
                            -outline red
        $cvObject create circle \
                            {130 170} \
                            -tags {dim_01} \
                            -radius 0.75 \
                            -fill lightgray \
                            -outline red
        $cvObject create circle \
                            {165 165} \
                            -tags {dim_01} \
                            -radius 0.75 \
                            -fill lightgray \
                            -outline red
        $cvObject create line \
                            {130 170  150 150  165 165} \
                            -tags {dim_01} \
                            -fill lightgray \
                            -outline gray20 \
                            -width 0.75
        $cvObject create dimensionAngle \
                            {{150 150} {165 165} {130 170}} \
                            40 \
                            0 \
                            darkred
            #
            #
        $cvObject create circle \
                            {225 140} \
                            -tags {dim_02} \
                            -radius 0.75 \
                            -fill lightgray \
                            -outline red
        $cvObject create circle \
                            {200 155} \
                            -tags {dim_02} \
                            -radius 0.75 \
                            -fill lightgray \
                            -outline red
        $cvObject create line \
                            {225 140  200 155} \
                            -tags {dim_02} \
                            -fill lightgray \
                            -outline gray20 \
                            -width 0.75
        $cvObject create dimensionLength \
                            {{225 140} {200 155}} \
                            horizontal \
                            35 \
                            0 \
                            darkblue
        $cvObject create dimensionLength \
                            {{200 155} {225 140}} \
                            vertical \
                            10 \
                             0 \
                            darkblue
            #
        $cvObject create circle \
                            {275 130} \
                            -tags {dim_03} \
                            -radius 0.75 \
                            -fill lightgray  \
                            -outline red
        $cvObject create circle \
                            {250 145} \
                            -tags {dim_03} \
                            -radius 0.75 \
                            -fill lightgray  \
                            -outline red
        $cvObject create line \
                            {275 130  250 145} \
                            -tags {dim_03} \
                            -fill lightgray  \
                            -outline gray20  \
                            -width 0.75
        $cvObject create dimensionLength \
                            {{275 130} {250 145}} \
                            aligned \
                            20 \
                            0 \
                            darkorange
        $cvObject create dimensionRadius \
                            {{275 130} {250 145}} \
                            45 \
                            0 \
                            darkgreen
            #
    }
        #
}
    #
    #
button .bf.btImpSVG  -text "import SVG"  -width 10  -command [list importSVG {145 105}]
pack   .bf.btImpSVG  -side left
proc importSVG {{pos {}} {angle 0}} {
    variable cvObject
    variable cv_scale
    variable TEST_Sample_Dir
    puts "  -> create_Content:  $cvObject"
    puts "   -> cv_scale:       $cv_scale"
    set fileName [tk_getOpenFile -initialdir $TEST_Sample_Dir]    
    if {$pos eq {}} {
        set pos {0 0}
    }
    if {$fileName eq {}} {
        return
    } else {
        puts "  ... $fileName"
    }
    set dragObj [$cvObject create svg $pos -svgFile $fileName -angle $angle]
    puts "   -> \$dragObj $dragObj"     
    $cvObject registerDragObject  $dragObj  [list [namespace current]::reportDragObject]
}
proc reportDragObject {args} {
    # puts " -> reportDragObject:  $args"
}    

    #
    #
button .bf.btExpSVG  -text "export SVG"  -width 10  -command [list exportSVG]
pack   .bf.btExpSVG  -side left
proc exportSVG {} {
    variable cvObject
    variable TEST_Export_Dir
    set exportFile [file join $TEST_Export_Dir _test_00.svg]
    $cvObject export SVG $exportFile
    tk_messageBox -message "cad4tcl: content exported to: \n $exportFile"
}     
    #
    #
    #
button .bf.btSwitch  -text "switch Type"  -width 10  -command [list switch_Type]
pack   .bf.btSwitch  -side left
proc switch_Type {} {
    variable  cvObject
    variable  cv_scale 
    variable TEST_Sample_Dir
    puts "-> switch_Type:     $cvObject "
    puts "   -> cv_scale:   $cv_scale"
    if {[$cvObject getType] eq {PathCanvas}} {
        set cad4tcl::canvasType 0
    } else {
        set cad4tcl::canvasType 1
    }
    cad4tcl::delete $cvObject
    set cvObject    [cad4tcl::new  .cf  800  600  A4  1.0  30]
    set cv_scale    [$cvObject configure Canvas Scale]
    set cv_type     [$cvObject getType]
    puts "-> switch_Type:     $cv_type"
    wm title .      "[file rootname [file tail $::argv0]] - $cv_type"
    $cvObject create vectorText \
                    {20 50} \
                    -text "canvasType: $cv_type" \
                    -tags {vectorText_03}
                    
                    puts "\$TEST_Sample_Dir $TEST_Sample_Dir"
    switch -exact $cv_type {
        Canvas {
            set fileName [file join $TEST_Sample_Dir tkcanvas Tcl_02_tkcanvas.svg]
            set fileName [file join $TEST_Sample_Dir tkcanvas Ghostscript_Tiger_200x200_tkcanvas.svg]
        }
        PathCanvas -
        default {
            set fileName [file join $TEST_Sample_Dir tkpath Tcl_02_tkpath.svg]
            set fileName [file join $TEST_Sample_Dir tkpath Ghostscript_Tiger_200x200_tkpath.svg]
        }
    }
    $cvObject create svg \
                    {200 80} \
                    -svgFile $fileName \
                    -angle 0
}
