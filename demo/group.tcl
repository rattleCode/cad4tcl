 ########################################################################
 #
 # cad4tcl__SVG_Viewer.tcl
 # by Manfred ROSENBERGER
 #
 #   (c) Manfred ROSENBERGER 2018/07/04
 #
 #

set TEST_ROOT_Dir    [file normalize [file dirname [lindex $argv0]]]
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
    #
set TEST_Sample_Dir  [file join $TEST_ROOT_Dir _sample]
    #
set TEST_Export_Dir  [file join $TEST_ROOT_Dir _export]
    #
    #
puts "    -> \$TEST_ROOT_Dir ...... $TEST_ROOT_Dir"
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
puts "    -> \$TEST_Sample_Dir .... $TEST_Sample_Dir"
puts "    -> \$TEST_Export_Dir .... $TEST_Export_Dir"
    #
    #
lappend auto_path [file join $TEST_ROOT_Dir __lib]
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"
lappend auto_path [file join $TEST_Library_Dir __ext_Libraries]
    #
foreach dir $tcl_library {
    puts "   -> tcl_library $dir"
}    
    #
foreach dir $auto_path {
    puts "   -> auto_path   $dir"
}    
    #
    
package require tkpath 0.3.0


set t .c_group
destroy $t
toplevel $t
set w $t.c
pack [tkp::canvas $w -width 400 -height 400 -bg white]

unset -nocomplain s f stroke fill

array set stroke [list 1 "#c8c8c8" 2 "#a19de2" 3 "#9ac790" 4 "#e2a19d"]
array set fill   [list 1 "#e6e6e6" 2 "#d6d6ff" 3 "#cae2c5" 4 "#ffd6d6"]

$w create prect 10 10 390 390 -rx 20 -strokewidth 4 -stroke gray70 -tags g0

foreach i {1 2 3 4} {
    set s($i) [$w style create -strokewidth 3 -stroke $stroke($i)]
    set f($i) [$w style create -strokewidth 3 -stroke $stroke($i) -fill $fill($i)]

    $w create group -tags g$i
    $w create prect 10 10 180 180 -rx 10 -parent g$i -style $s($i)

    set id [$w create path "M 0 0 l 30 40 h -60 z" -parent g$i -style $f($i)]
    $w move $id 60 40
    
    set id [$w create path "M -20 0 h 40 l -40 80 h 40 z" -parent g$i -style $f($i)]
    $w move $id 140 40

    set id [$w create ellipse 0 0 -rx 30 -ry 20 -parent g$i -style $f($i)]
    $w move $id 60 140
}

$w move g1 10 10
$w move g2 200 10
$w move g3 10 200
$w move g4 200 200

proc inspectContent {w id {intent {}}} {
    set itemType    [$w type $id]
    set itemTags    [$w gettags $id]
    puts "$intent$id <- $itemType <- $itemTags"
    set intent "    $intent"
    foreach childItem [$w children $id] {
        inspectContent $w $childItem $intent
    }
}
inspectContent $w 0
unset -nocomplain s f stroke fill


