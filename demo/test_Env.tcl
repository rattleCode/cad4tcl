 ########################################################################
 #
 # test_SVG.tcl
 # by Manfred ROSENBERGER
 #
 #   (c) Manfred ROSENBERGER 2018/05/24
 #
 # 



set WINDOW_Title      "cad4tcl, an extension for canvas"
    #
    #
set BASE_Dir  [file normalize [file dirname [file normalize $::argv0]]]
    #
set TEST_ROOT_Dir    [file normalize [file dirname [lindex $argv0]]]
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
    #
set TEST_Sample_Dir  [file join $TEST_ROOT_Dir _sample]
    #
set TEST_Export_Dir  [file join $TEST_ROOT_Dir _export]
    #
puts "   \$TEST_ROOT_Dir ... $TEST_ROOT_Dir"
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
puts "    -> \$TEST_Sample_Dir .... $TEST_Sample_Dir"
puts "    -> \$TEST_Export_Dir .... $TEST_Export_Dir"
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"
lappend auto_path [file join $TEST_Library_Dir __ext_Libraries]
    #
    #
    # source [file join $BASE_Dir classSVG.tcl]
package require svgDOM 0.09
    #
    #
    
puts "\n ==================================================\n"    
    
    
return
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
    #
lappend auto_path "$TEST_Library_Dir/vectormath"
lappend auto_path "$TEST_Library_Dir/appUtil"
lappend auto_path "$TEST_Library_Dir/__ext_Libraries"
    #
lappend auto_path "$TEST_Library_Dir/lib-vectormath"
lappend auto_path "$TEST_Library_Dir/lib-tkpath0.3.3"
lappend auto_path "$TEST_Library_Dir/lib-appUtil"
    #
    #
package require   cad4tcl 0.11
package require   appUtil
    #
