 ########################################################################
 #
 # cad4tcl__Test_00.tcl
 # by Manfred ROSENBERGER
 #
 #   (c) Manfred ROSENBERGER 2017/11/26
 #
 #

set TEST_ROOT_Dir [file normalize [file dirname [lindex $argv0]]]
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
    #
puts "    -> \$TEST_ROOT_Dir ...... $TEST_ROOT_Dir"
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"
lappend auto_path [file join $TEST_Library_Dir __ext_Libraries]
    #
foreach dir $tcl_library {
    puts "   -> tcl_library $dir"
}    
    #
foreach dir $auto_path {
    puts "   -> auto_path   $dir"
}    
    #
    #
package require cad4tcl 0.06
    #
    #
set cad4tcl::canvasType 1
    #
proc createBase {} {
    #wm forget .
    package require cad4tcl_simplifySVG
    cad4tcl::app::simplifySVG::view::build .
}
proc createToplevel {} {
    package require cad4tcl_simplifySVG
    cad4tcl::app::simplifySVG::buildToplevelGUI
}
    #
pack [button .b1 -text "create"          -command [list createBase]]
pack [button .b2 -text "create toplevel" -command [list createToplevel]]
    #
    #
return

