 ########################################################################
 #
 # cad4tcl__SVG_Viewer.tcl
 # by Manfred ROSENBERGER
 #
 #   (c) Manfred ROSENBERGER 2018/07/04
 #
 #

set TEST_ROOT_Dir    [file normalize [file dirname [lindex $argv0]]]
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
    #
set TEST_Sample_Dir  [file join $TEST_ROOT_Dir _sample]
    #
set TEST_Export_Dir  [file join $TEST_ROOT_Dir _export]
    #
    #
puts "    -> \$TEST_ROOT_Dir ...... $TEST_ROOT_Dir"
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
puts "    -> \$TEST_Sample_Dir .... $TEST_Sample_Dir"
puts "    -> \$TEST_Export_Dir .... $TEST_Export_Dir"
    #
    #
lappend auto_path [file join $TEST_ROOT_Dir __lib]
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"
lappend auto_path [file join $TEST_Library_Dir __ext_Libraries]
    #
foreach dir $tcl_library {
    puts "   -> tcl_library $dir"
}    
    #
foreach dir $auto_path {
    puts "   -> auto_path   $dir"
}    
    #
    #
package require Tk
package require cad4tcl 0.21
package require pdf4tcl
puts "    -> Tk:      [package present Tk]"
puts "    -> cad4tcl: [package present cad4tcl]"
puts "    -> pdf4tcl: [package present pdf4tcl]"
    #
source [file join $TEST_ROOT_Dir __lib lib_rdial.tcl]
    #
namespace eval bikeComponent {
    variable packageHomeDir [file dirname [file normalize [info script]]]
    lappend auto_path [file join $packageHomeDir lib]
}
    #
namespace eval bikeComponent::view {

    variable    cvObject_Canvas {}
    variable    cvObject_Path   {}
    variable    menueFrame      {}
    
    variable    compList_System {}
    variable    compList_Custom {}
    variable    tree_System     {}
    variable    tree_Custom     {}
    variable    tree_User       {}
    variable    tree_CompSelection  {}
    variable    rdials_list     {}

    variable    compSVGFile     {}
    variable    compSVGNode     {}
    variable    compSVGKey      {}

    variable    configValue
      array set configValue     {}
      
    variable    colorSet
      array set colorSet        [list components snow2]
      
    variable    packageHomeDir
    variable    packageImageDir
    
    variable    iconArray  
      array set iconArray       {}
    
    variable    arrayRepoFilter  
      array set arrayRepoFilter {}
    variable    arrayCompFilter  
      array set arrayCompFilter {}
      
    variable    listRepoFilter  {}
    variable    listCompFilter  {}
      
}
    #
    #
proc bikeComponent::view::init {} {
        # ->
    variable packageHomeDir     $bikeComponent::packageHomeDir
    variable packageImageDir    [file normalize [file join $packageHomeDir __lib]]
        #
    variable iconArray
    variable configValue
    array set configValue       [list compOffset_X  0.0  compOffset_y 0.0  compAngle  0.0  test  0.0]
        #
        #
    ttk::style configure lightblue.TFrame       -background lightblue
    ttk::style configure    yellow.TFrame       -background yellow
    ttk::style configure    yellow.TLabelframe  -background yellow
        #
        # set resultString    [lsearch -inline -exact [ttk::style element names] {IconButton}]
    if {[catch {ttk::style layout IconButton} eID]} {
        puts "     ... have to create new ttk::style: $eID"
        ttk::style configure IconButton -padding {0 0}
        ttk::style layout IconButton {
            Button.button -children {
                Button.focus -children {
                    Button.padding -children {
                        Button.label -sticky nsew
                    }
                }
            }
        }
    }
        #
        #
    set iconArray(resize)       [image create photo -file [file join $packageImageDir resize.gif]     ]
    set iconArray(scale_plus)   [image create photo -file [file join $packageImageDir scale_plus.gif] ]
    set iconArray(scale_minus)  [image create photo -file [file join $packageImageDir scale_minus.gif]]
    set iconArray(update)       [image create photo -file [file join $packageImageDir update.gif]     ]
        #
        #  
}
# ------------------------------------------------------------------------
    #
proc bikeComponent::view::build {w} {
        #
    variable packageImageDir
        #
    package require cad4tcl
        #
    if {$w == {.} || $w == {}} {
        set rootFrame   [ttk::frame .f   -style lightblue.TFrame]
    } else {
        set rootFrame   [ttk::frame $w.f -style lightblue.TFrame]
    }
    pack $rootFrame  -expand yes  -fill both
    
    createView $rootFrame       
        
    return $rootFrame        
        
}

# ------------------------------------------------------------------------
   #  create report widget
proc bikeComponent::view::createView {w} {
    
    variable cvObject_Canvas
    variable cvObject_Path
    variable menueFrame
    
    variable    compList_System
    variable    compList_Custom
    variable    tree_System
    variable    tree_Custom
    variable    tree_User
    variable    tree_CompSelection
    
        # -- layout -- 
    pack [ ttk::frame $w.f -style yellow.TFrame] -fill both -expand yes
    set menueFrame  [ ttk::frame $w.f.f_menue    -relief flat]
    set viewFrame   [ ttk::frame $w.f.f_view ]
    set canvasFrame [ ttk::frame $w.f.f_canvas ]
    set pathFrame   [ ttk::frame $w.f.f_path ]
    pack    $menueFrame \
            $viewFrame\
        -fill both -side left
    pack configure $viewFrame  -expand yes
    pack configure $menueFrame -expand no
        #
    pack    $canvasFrame \
            $pathFrame \
        -fill both -side top
        #
        #
        # ---- Button Bar
    set buttonBar       [create_ButtonBar  $menueFrame]
    pack $buttonBar  -expand yes -fill both
        #
        #
        # ---- File Area
    set fileFrame       [ttk::labelframe $menueFrame.f_fileopen -text "File"]
    pack $fileFrame  -side top  -fill x  -expand yes 
        #
    grid columnconfigure $fileFrame 0 -minsize 15
    grid columnconfigure $fileFrame 1 -weight 1
        #
    button $fileFrame.btImpSVG  -text "import SVG"      -width 10  -command [list bikeComponent::view::importSVG {145 105}]
    pack   $fileFrame.btImpSVG  -side top -fill x
        #
    button $fileFrame.btReport  -text "reportContent"   -width 10  -command [list bikeComponent::view::inspectContent]
    pack   $fileFrame.btReport  -side top -fill x
        #
    button $fileFrame.btExpSVG  -text "export SVG"      -width 10  -command [list bikeComponent::view::exportSVG]
    pack   $fileFrame.btExpSVG  -side top -fill x
        #
    button $fileFrame.btExpPDF  -text "export PDF"      -width 10  -command [list bikeComponent::view::exportPDF]
    pack   $fileFrame.btExpPDF  -side top -fill x
        # 
        #
        # ---- Control Area
        #
    set controlFrame     [ttk::labelframe $menueFrame.f_control -text "Control"]
    pack $controlFrame   -side top  -fill x  -expand yes 
        #
    grid columnconfigure $controlFrame 0 -minsize 15
    grid columnconfigure $controlFrame 1 -weight 1
        #
    button $controlFrame.btFit     -text "fit"  -width 10  -command [list bikeComponent::view::refitCanvas]
    pack   $controlFrame.btFit     -side top -fill x
        #
    button $controlFrame.btScale-  -text "scale -"  -width 10  -command [list bikeComponent::view::scaleCanvas 0.5]
    pack   $controlFrame.btScale-  -side top -fill x
    button $controlFrame.btScale+  -text "scale +"  -width 10  -command [list bikeComponent::view::scaleCanvas 2.0]
    pack   $controlFrame.btScale+  -side top -fill x
        #
    button $controlFrame.btClear   -text "delete Content"  -width 15  -command bikeComponent::view::cleanCanvas
    pack   $controlFrame.btClear   -side top -fill x
        #
    pack $controlFrame -expand yes -fill x    
        #
        #
        # ---- Position Area
    set positionFrame   [ttk::labelframe $menueFrame.f_position -text "Position"]
    pack $positionFrame   -side top  -fill x  -padx 3 -pady 1
        #
    create_configLine  $positionFrame.off_x    "Offset x"    [namespace current]::configValue(compOffset_X)  -180 0.0 180     1      [list [namespace current]::configLine_Update]
    create_configLine  $positionFrame.off_y    "Offset y"    [namespace current]::configValue(compOffset_y)  -180 0.0 180     1      [list [namespace current]::configLine_Update]
    create_configLine  $positionFrame.angle    "Angle"       [namespace current]::configValue(compAngle)     -180 0.0 180     1      [list [namespace current]::configLine_Update]
    pack    $positionFrame.off_x \
            $positionFrame.off_y \
            $positionFrame.angle \
            -side top
        #
        #
        # -- finish layout --
    pack configure $buttonBar       -fill x -expand no
    pack configure $fileFrame       -fill x -expand no  -side top
    pack configure $controlFrame    -fill x -expand no  -side top
    pack configure $positionFrame   -fill x -expand no  -side top
        #
        #
    set cad4tcl::canvasType 1
    set cvObject_Path   [cad4tcl::new  $canvasFrame  850  600  A3  1.0  40  -bd 2  -bg lightgray  -relief sunken]
        #
        # -- canvas --
    set cad4tcl::canvasType 0
    set cvObject_Canvas [cad4tcl::new  $canvasFrame  850  600  A3  1.0  40  -bd 2  -bg lightgray  -relief sunken]
        #
        
}

# ------------------------------------------------------------------------
    #  build ButtonBar
proc bikeComponent::view::create_ButtonBar {w} {
        #
    variable iconArray
        #
    set myFrame     [ttk::frame $w.f_bbar]
    pack $myFrame   -fill both -expand yes
        #
    ttk::button    $myFrame.render       -image  $iconArray(update)        -style "IconButton"  -command { bikeComponent::view::updateView }  
    ttk::button    $myFrame.scale_plus   -image  $iconArray(scale_plus)    -style "IconButton"  -command { bikeComponent::view::scaleCanvas  [expr 3.0/2] }  
    ttk::button    $myFrame.scale_minus  -image  $iconArray(scale_minus)   -style "IconButton"  -command { bikeComponent::view::scaleCanvas  [expr 2.0/3] }  
    ttk::button    $myFrame.resize       -image  $iconArray(resize)        -style "IconButton"  -command { bikeComponent::view::refitCanvas }  
        #
    grid    $myFrame.render       -in $myFrame  -row 0  -column 0  -sticky w
    grid    $myFrame.scale_minus  -in $myFrame  -row 0  -column 2  -sticky e
    grid    $myFrame.scale_plus   -in $myFrame  -row 0  -column 3  -sticky e
    grid    $myFrame.resize       -in $myFrame  -row 0  -column 4  -sticky e
        #
    grid columnconfigure $myFrame 1  -weight 2
        #
        # myGUI::gui::setTooltip  $toolBar.render       "update Canvas..."       
        # myGUI::gui::setTooltip  $toolBar.scale_plus   "scale plus"             
        # myGUI::gui::setTooltip  $toolBar.scale_minus  "scale minus"            
        # myGUI::gui::setTooltip  $toolBar.resize       "resize"                 
        #
    return $myFrame
        #
}

# ------------------------------------------------------------------------
    #  create config_line
    #
proc bikeComponent::view::create_configLine {w lb_text entryVar start current end resolution command} {
        #
    variable    rdials_list
    set         $entryVar $current
        puts "   ... \$entryVar [list [format "$%s" $entryVar]]"
        #
    ttk::frame   $w
    pack    $w
    ttk::label   $w.lb   -text $lb_text           -width 10  -anchor w
    ttk::entry   $w.cfg  -textvariable $entryVar  -width  5  -justify right
    ttk::frame   $w.f    -relief sunken 
        #
    rdial::create   $w.f.scl \
                    -value      $current \
                    -height     13 \
                    -width      84 \
                    -orient     horizontal \
                    -callback   [list $command $entryVar]
    lappend rdials_list $w.f.scl

    bind $w.cfg <Leave>         [list $command ]
    bind $w.cfg <Return>        [list $command ]
        
    pack      $w.lb  $w.cfg  $w.f  $w.f.scl    -side left  -fill x -padx 2
        #
}

# ------------------------------------------------------------------------
    #  reset Positioning
    #
proc bikeComponent::view::reset_Positioning {cvObj} {
        #
    variable rdials_list
    variable configValue
        #
    set  configValue(compAngle)     0.0
    set  configValue(compOffset_X)  0.0
    set  configValue(compOffset_y)  0.0
    foreach rd $rdials_list {
        rdial::configure $rd -value 0
    }
        #
    [namespace current]::updateCanvas $cvObj
        #
}
# ------------------------------------------------------------------------
   #  refit Canvas to provided widget
   #
proc bikeComponent::view::refitCanvas {} {
        #
    variable cvObject_Canvas
    variable cvObject_Path
        #
    puts "   ... bikeComponent::view::refitCanvas: $cvObject_Canvas / $cvObject_Path"
    set  scaleCanvas [$cvObject_Canvas    fit]
    set  scalePath   [$cvObject_Path      fit]
        #
    set my_cvScale   $scaleCanvas
        #
    updateView
        #
}
# ------------------------------------------------------------------------
    #  update Canvas
    #
proc bikeComponent::view::updateCanvas {cvObj {drag_Event {}}} {
        #
    variable compSVGNode
    variable configValue
    variable colorSet
        #
    set cvCanvas    [$cvObj getCanvas] 
        #
        #
        puts "\n            ... updateCanvas: $compSVGNode"
        # puts "            ... $currentTab"
        # puts "            ... $varName"
        # puts "            ... $cvCanvas"
        #
    $cvObj        deleteContent
        #
    create_Centerline $cvObj
        #
    if {$compSVGNode != {}} {
            #
        set stageCenter     [$cvObj configure Stage Center]
        lassign $stageCenter c_x c_y
            #
        set compPosition    [list [expr {$c_x + $configValue(compOffset_X)}] [expr {$c_y + $configValue(compOffset_y)}]]
            #
            # set myComponent     [$cvObj create svg $compPosition [list -svgNode $compSVGNode  -angle $configValue(compAngle)  -tags __Decoration__]]
            #
        set myComponent     [$cvObj create svg $compPosition [list -svgNode $compSVGNode  -angle $configValue(compAngle)  -tags __Decoration__]]
            #
        foreach cv_Item [$cvObj find withtag __Decoration__] {
            set cv_Type     [$cvCanvas type $cv_Item]
                # puts "         -> $cv_Item   <- $cv_Type"
            switch -exact $cv_Type {
                group    {
                        # $cvObj itemconfigure  $cv_Item -style [$cvCanvas style create  -fill $colorSet(components)] ;
                        # -strokewidth 3  
                    foreach childItem [$cvCanvas children $cv_Item] {
                        set child_Type     [$cvCanvas type $childItem]
                        $cvObj itemconfigure  $childItem -fill $colorSet(components)
                    }
                }
                oval     -
                path     -
                rect     -
                prect    -
                circle   -
                ellipse  -
                polygon  -
                ppolygon  { 
                    $cvObj itemconfigure  $cv_Item -fill $colorSet(components)
                }
                default  {}
            }
                # puts "  -> $cvItem [$cvCanvas gettags $cv_Item]"
        }
    }
        #
        #
    switch -exact [winfo class $cvCanvas] {
        PathCanvas {
            inspectContent_w $cvCanvas 0
        }
        default {}
    }
        #
}
# ------------------------------------------------------------------------
    #  create Centerline
    #
proc bikeComponent::view::create_Centerline {cvObj} {
        #
    set stageCenter     [$cvObj configure Stage Center]
    set stageWidth      [$cvObj configure Stage Width]
    set stageHeight     [$cvObj configure Stage Height]
        #
    puts "   -> create_Centerline:"
    puts "           -> \$stageCenter: $stageCenter"
    puts "           -> \$stageWidth:  $stageWidth"
    puts "           -> \$stageHeight: $stageHeight"
        #
    set cl_xc   [expr {0.5 * $stageWidth}]
    set cl_yc   [expr {0.5 * $stageHeight}]
        #
    set cl_x0   [expr {10}]
    set cl_x1   [expr {$stageWidth  - 10}]
        #
    set cl_y0   [expr {10}]
    set cl_y1   [expr {$stageHeight - 10}]
        #
    puts "    -> [info object class $cvObj]"
        #
    if {[info object class $cvObj] eq "::cad4tcl::Canvas"} {
        catch {
            $cvObj  create centerLine [list $cl_x0 $cl_yc $cl_x1 $cl_yc ]  -fill red  -tags __CenterLine__
            $cvObj  create centerLine [list $cl_xc $cl_y0 $cl_xc $cl_y1 ]  -fill red  -tags __CenterLine__
        }
    } else {
        $cvObj      create centerLine [list $cl_x0 $cl_yc $cl_x1 $cl_yc ]  -fill red  -tags __CenterLine__
        $cvObj      create centerLine [list $cl_xc $cl_y0 $cl_xc $cl_y1 ]  -fill red  -tags __CenterLine__
    }
        #
}
# ------------------------------------------------------------------------
    #  create config commands
    #
proc bikeComponent::view::configLine_Update  {{entryVar ""} {value {0}} args} {
        #
    variable    cvObject_Canvas    
    variable    cvObject_Path
        #
    puts "     -> configLine_Update: \$args $args"
        #
    if {$entryVar ne ""} {
        set $entryVar $value
    }
        #
    parray [namespace current]::configValue
        #
    updateCanvas $cvObject_Canvas    
    updateCanvas $cvObject_Path    
        #
}
# ------------------------------------------------------------------------
    #  create button commands
    #
proc bikeComponent::view::importSVG {{pos {}} {angle 0}} {
        #
    variable cvObject_Canvas
    variable cvObject_Path
        #
    variable my_cvScale
    variable TEST_Sample_Dir
    variable compSVGFile
    variable compSVGNode
    puts "  -> create_Content:  $cvObject_Canvas / $cvObject_Path"
        # puts "   -> my_cvScale:       $::my_cvScale"
    set fileName [tk_getOpenFile -initialdir $::TEST_Sample_Dir]    
    if {$pos eq {}} {
        set pos {0 0}
    }
    if {$fileName eq {}} {
        return
    } else {
        puts "  ... $fileName"
    }
        #
    set compSVGFile $fileName
        #
    if {$compSVGFile eq {}} {
        puts "\n     <E> ... please select a SVG File"
        return
    } else {
        set fp          [open $compSVGFile]
            #
        fconfigure      $fp -encoding utf-8
        set xml         [read $fp]
        close           $fp
            #
        set doc         [dom parse  $xml]
        set compSVGNode [$doc documentElement]
    }
        #
    if [catch {updateCanvas $cvObject_Canvas} eID] {
        puts "\n    -> $cvObject_Canvas -> $eID\n"
    }
    if [catch {updateCanvas $cvObject_Path} eID] {
        puts "\n    -> $cvObject_Path -> $eID\n"
    }
        #
}
    #
proc bikeComponent::view::updateView {} {
        #
        # -- base geometry
        #
        # ---- a copy of myGUI::cvCustom::update_BaseGeometry
        #
    variable    cvObject_Canvas    
    variable    cvObject_Path    
        #
        #
        # set bottomCanvasBorder    30
        # set updatePosition        recenter
        #
    $cvObject_Canvas    deleteContent
    updateCanvas        $cvObject_Canvas   
    reset_Positioning   $cvObject_Canvas 
        #
        #
    $cvObject_Path      deleteContent
    updateCanvas        $cvObject_Path   
    reset_Positioning   $cvObject_Path 
        #
    return
        #
}
proc bikeComponent::view::scaleCanvas {{scale 1}} {
        #
    variable cvObject_Canvas
    variable cvObject_Path
        #
    puts "\n  -> scaleCanvas:   $cvObject_Canvas"
        #
    set curScale    [$cvObject_Canvas    configure   Canvas  Scale ]
    set scaleCanvas [format "%.4f" [ expr $scale * $curScale * 1.0 ] ]
        #
    $cvObject_Canvas center $scaleCanvas    
        #
    set curScale    [$cvObject_Path      configure   Canvas  Scale ]
    set scalePath   [format "%.4f" [ expr $scale * $curScale * 1.0 ] ]
        #
    $cvObject_Path   center $scalePath    
        #
        # puts "   $curScale"
        # tk_messageBox -message "curScale: $curScale  /  newScale  $newScale"
    return    
        #
}
proc bikeComponent::view::cleanCanvas {} {
    variable cvObject_Canvas
    variable cvObject_Path
    $cvObject_Canvas deleteContent
    $cvObject_Path   deleteContent
}
proc bikeComponent::view::reportDragObject {args} {
    # puts " -> reportDragObject:  $args"
}    
proc bikeComponent::view::exportSVG {} {
    variable cvObject_Canvas
    variable cvObject_Path
    variable TEST_Export_Dir
        #
    set exportFile [file join $::TEST_Export_Dir _test_00_canvas.svg]
    $cvObject_Canvas export SVG $exportFile
    tk_messageBox -message "cad4tcl: content exported to: \n $exportFile"
        #
    set exportFile [file join $::TEST_Export_Dir _test_00_path.svg]
    $cvObject_Path   export SVG $exportFile
    tk_messageBox -message "cad4tcl: content exported to: \n $exportFile"
}        
proc bikeComponent::view::exportPDF {} {
    variable cvObject_Canvas
    variable cvObject_Path
    variable TEST_Export_Dir
        #
    set exportFile [file join $::TEST_Export_Dir _test_00_canvas.pdf]
    set w          [$cvObject_Canvas getCanvas]
        #
        # Example printing a canvas with pdf4info
    pdf4tcl::new mypdf -paper a3 -margin  5mm     ; # make a new pdf-object
    mypdf startPage                               ; # make a new page
        # make and fill a canvas .canv 
        # ...
    mypdf canvas $w                               ; # then put the canvas on your pdf-page
    mypdf write -file $exportFile                 ; # write to file
    mypdf destroy                                 ; # clean up
    tk_messageBox -message "cad4tcl: content exported to: \n $exportFile"
        #
        #
        #
    set exportFile [file join $::TEST_Export_Dir _test_00_path.pdf]
    set w          [$cvObject_Path getCanvas]
        #
    inspectContent_w $w 0
        #
        # Example printing a canvas with pdf4info
    pdf4tcl::new mypdf -paper a3 -margin  5mm     ; # make a new pdf-object
    mypdf startPage                               ; # make a new page
        # make and fill a canvas .canv 
        # ...
    mypdf canvas $w                               ; # then put the canvas on your pdf-page
    mypdf write -file $exportFile                 ; # write to file
    mypdf destroy                                 ; # clean up
    tk_messageBox -message "cad4tcl: content exported to: \n $exportFile"
} 
    #
    #
proc bikeComponent::view::inspectContent {} {
        #
    variable cvObject_Path
    set w [$cvObject_Path getCanvas]
        #
    set id 0
    puts ">$id<"
        #
    set itemType    [$w type $id]
    set itemTags    [$w gettags $id]
    puts "<- $id -> $itemType -> $itemTags"
        #
    [namespace current]::inspectContent_w $w $id "        "
        #
}
proc bikeComponent::view::inspectContent_w {w id {intent {}}} {
        #
    puts ""
    puts "  -- inspectContent -- [winfo class $w] -- $id --"
    puts ""
        #
        #
    [namespace current]::__inspectContent $w $id "        "

        #
    puts ""
    puts "  -- inspectContent -- [winfo class $w] --"
    puts ""
        #
}
proc bikeComponent::view::__inspectContent {w id {intent {}}} {
        #
    set itemType    [$w type $id]
    set itemTags    [$w gettags $id]
    puts "$intent$id <- $itemType <- $itemTags"
    set intent "    $intent"
        #
    if {[catch {set childItems [$w children $id]} eID]} {
        #puts "            <E> $eID"
        set childItems {}
    }
        # puts "                         \$childItems -> $childItems"
        
    if {$itemType != {group}} {
        set itemCoords  [$w coords $id]
        puts "                   -> $itemCoords"
    }
    set itemMatrix  [$w itemconfigure -matrix]
    puts "                   -> $itemMatrix"
        #
    foreach item $childItems {
        [namespace current]::__inspectContent $w $item $intent
    }
}

    #
    #
bikeComponent::view::init
bikeComponent::view::build .


