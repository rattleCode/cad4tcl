 ########################################################################
 #
 # cad4tcl__Test_00.tcl
 # by Manfred ROSENBERGER
 #
 #   (c) Manfred ROSENBERGER 2017/11/26
 #
 #

set TEST_ROOT_Dir [file normalize [file dirname [lindex $argv0]]]
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
    #
puts "    -> \$TEST_ROOT_Dir ...... $TEST_ROOT_Dir"
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"
lappend auto_path [file join $TEST_Library_Dir __ext_Libraries]
    #
foreach dir $tcl_library {
    puts "   -> tcl_library $dir"
}    
    #
foreach dir $auto_path {
    puts "   -> auto_path   $dir"
}    
    #
    #
    # ------------------
    #
package require cad4tcl 0.06
    #
set nsModel    cad4tcl::app::simplifySVG::model
    #
    #
    # ------------------
    #
set testFile        [file join $TEST_ROOT_Dir _sample shimano_RD-9100.svg]
    #
puts "   ... \$testFile $testFile"
    #
    #
    # ------------------
    #
cad4tcl::app::simplifySVG::model::openSVGFile $testFile    
    #
set svgTextDeep     [${nsModel}::getSVGText]
set svgDoc          [${nsModel}::getSVGDoc]
set svgRootSource   [${nsModel}::getSVGRoot]
    #
puts "[$svgRootSource asXML]"
    #
    #
    # ------------------
    #
set svgObject       [::svgDOM::SVG new svgNode $svgRootSource]
    #
set pathType        fraction
puts "\n       \$pathType $pathType\n"
$svgObject pathType $pathType
    #
set svgRootTarget   [$svgObject getSVG]
$svgObject destroy
    #
puts "[$svgRootTarget asXML]"
    #
    #
    # ------------------
    #
proc cad4tcl::app::simplifySVG::view::simplifyStructure {} {
    
    variable targetText
    
    puts "--- simplifyStructure - 0 --"
    puts "$targetText"
    
    # set svg [$targetText get 1.0 end]
    # dom parse  $svg doc
        #
    set doc $targetText
        #
    $doc documentElement root
        
    puts "\n"
    puts "--- simplifyStructure - 1 --"
    puts ""
    puts "[$root nodeName]"
        # return
        # puts "[$doc asXML]"
    
    
    flattenSingleGroup $root    
        #
        
    puts "\n"
    puts "--- simplifyStructure - 2 --"
    puts ""
    $doc documentElement root
    puts "[$root asXML]"


    return    
        #
    
}

proc cad4tcl::app::simplifySVG::view::flattenSingleGroup {parentNode} {
        #
    puts "        -> flattenSingleGroup [info level]"
    puts "            -> $parentNode"
    puts "                -> children: [$parentNode childNodes]"
            #
    foreach node [$parentNode childNodes] {
        switch -exact -- [$node nodeName] {
            g -
            svg {
                if {[info level] > 2} {
                    # puts "[$node asXML]"
                }
                set childNodes  [$node childNodes]
                if {[llength $childNodes] == 1} {
                    set childNode $childNodes
                    puts "\n                   -> upstore [$childNode toXPath] -> [$childNode nodeName] -> [$childNode getAttribute id]"
                    puts "                         -> \$parentNode  [$parentNode nodeName]"
                    puts "                         -> \$node        [$node nodeName] -> [$node childNodes]"
                    puts "                         -> \$childNode   [$childNode nodeName]"
                    $node removeChild $childNode
                    $parentNode replaceChild $childNode $node
                    #$node delete
                    #set domDoc  [$parentNode ownerDocument]
                    # $domDoc 
                }
                foreach childNode [[$parentNode childNodesLive] childNodes] {
                    flattenSingleGroup $childNode
                }
            }
            default {
                # puts "                   -> .. keep $node -> [$node asXML]"
                # puts "                   -> .. keep [$node toXPath] -> [$node nodeName] -> [$node getAttribute id]"
            }
        }
    }
        #
    return
        #
}
    #
    #
    # ------------------
    #
# variable cad4tcl::app::simplifySVG::view::targetText $svgRootTarget
    #
puts "\n"
puts "--- 1 ---"
puts ""
$svgRootTarget documentElement root
    #
puts "[$root nodeName]"
puts "[$root asXML]"
    #
    
puts "\n"
puts "--- 2 ---"
puts ""
cad4tcl::app::simplifySVG::view::flattenSingleGroup $root
    #
    
puts "\n"
puts "--- 3 ---"
puts ""
$svgRootTarget documentElement root
puts "[$root nodeName]"
puts "[$root asXML]"
    #
    
puts "\n"
puts "--- 4 ---"
puts ""
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
puts ""    
    #
return

