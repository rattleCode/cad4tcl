 ########################################################################
 #
 # test_cad4tcl.tcl
 # by Manfred ROSENBERGER
 #
 #   (c) Manfred ROSENBERGER 2010/02/06
 #
 # 



set WINDOW_Title      "cad4tcl, a facade for tk::canvas and tkpath"
    #
    #
    # - https://wiki.tcl-lang.org/page/main+script
if {$::argv0 eq [info script]} {
        #
    set BASE_Dir        [file normalize [file dirname [file normalize $::argv0]]]
    set TEST_ROOT_Dir   [file normalize [file dirname [lindex $argv0]]]
        #
} else {
        #
    puts " ----> $BASE_Dir"
    set TEST_ROOT_Dir   [file normalize $BASE_Dir]
    puts " ----> $TEST_ROOT_Dir"
        #
}
    #
catch {puts " <D> calling procedure: [lindex [info level 0] 0]"}
    #
variable TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
variable TEST_Sample_Dir  [file join $TEST_ROOT_Dir _sample]
variable TEST_Export_Dir  [file join $TEST_ROOT_Dir _export]
    #
puts "   \$TEST_ROOT_Dir ... $TEST_ROOT_Dir"
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
puts "    -> \$TEST_Sample_Dir .... $TEST_Sample_Dir"
puts "    -> \$TEST_Export_Dir .... $TEST_Export_Dir"
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"
lappend auto_path [file join $TEST_Library_Dir __ext_Libraries]
    #
    #
foreach dir $::tcl_library {
    puts "   -> tcl_library $dir"
}    
    #
foreach dir $::auto_path {
    puts "   -> auto_path   $dir"
}    
    #
    #
package require     cad4tcl 0.22
    #
    #
if 0 {
    package require Tk

    frame .f0 
    set f_canvas  [labelframe .f0.f_canvas   -text "board"  ]
    set f_config  [frame      .f0.f_config   ]

    pack  .f0      -expand yes -fill both
    pack  $f_canvas  $f_config    -side left -expand yes -fill both
    pack  configure  $f_config    -fill y
        # sketchboard::createStage    $f_canvas   1000 810  250 250 m  0.5 -bd 2  -bg white  -relief sunken
        # set cvObject    [cad4tcl::new  $cv_path  $cv_width  $cv_height  A3  0.5  40]
    set cvObject    [cad4tcl::new  $f_canvas   1000 810  A3  0.5  40]
    set w           [$cvObject getCanvas]    
    set cv_scale    [$cvObject configure Canvas Scale]
}


