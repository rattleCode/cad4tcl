

set d "M84,100
m-6.5 0.0a6.5,6.5 0,1,1 13.0 0.0
l0 2.0
a6.5 6.5 0 1 1 -13.0 0.0
z
m3.9 0.0
a2.6,2.6 0 1 1 5.2,0.0
l 0,2.0
a2.6,2.6 0,1,1 -5.2,0.0
z"

puts "$d"
puts "---"
# set e [regsub -all $d [a-zA-Z] { [a-zA-Z] }]
# puts "$e"

puts "\n--A--\n"
set e [regexp -all -inline {\S+} $d ]
puts "$e"

puts "\n--B--\n"
set e [regsub -all @"," @"_$&" $d]
puts "$e"

puts "\n--C--\n"

set e [regsub -all {[a-zA-Z]|.^$\\]} $d { & }]
set e [string map {, { }} $e]
puts "$e"


# @"[abc]", @"\$&"